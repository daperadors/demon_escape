using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/ItemsList")]
public class ItemsList : ScriptableObject
{
    public List<ItemStats> _itemsList;
}
