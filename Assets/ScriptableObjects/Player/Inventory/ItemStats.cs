using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/ItemStats")]
public class ItemStats : ScriptableObject
{
    public string _nameItem;
    public bool _enable = false;
    public Sprite image;
    public bool _voiceToExecute;


    private void OnEnable()
    {
        _enable = false;
    }
}
