using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/AtackBar")]
public class AtackBarCreator : ScriptableObject
{
    public Vector2 _referencePosition;
    public float _maxFill;
    public float _minFill;
}
