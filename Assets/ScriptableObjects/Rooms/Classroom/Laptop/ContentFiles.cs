using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/ContentFiles")]
public class ContentFiles : ScriptableObject
{
    public string type;
    public string headerTitle;
    public string title;
    public string content;
    public Sprite image;
}
