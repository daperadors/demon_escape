using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/LaptopFormAnswer")]
public class LaptopFormAnswer : ScriptableObject
{
    public string name;
    public string answer;
    public Sprite image;
    public Vector3 buttonPostion;
}
