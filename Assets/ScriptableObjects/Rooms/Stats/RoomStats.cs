using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/RoomStats")]
public class RoomStats : ScriptableObject
{
    public int id;
    public string nameRoom;
    public string newTagDoor;
}
