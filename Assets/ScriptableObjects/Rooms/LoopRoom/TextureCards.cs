using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TextureCardsClass
{
    public int number;
    public int letterValue;
    public Material material;

}

[CreateAssetMenu(menuName = "Scriptable Objects/TextureCards")]
public class TextureCards : ScriptableObject
{
    [SerializeField] public TextureCardsClass[] materials;
}
