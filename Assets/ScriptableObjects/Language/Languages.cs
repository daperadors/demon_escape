using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Languages")]
public class Languages : ScriptableObject
{
    public LanguagesConfig[] translations;
}
[System.Serializable]
public class LanguagesConfig
{
    public string language;
    public LanguageStats[] languageList;
}

[System.Serializable]
public class LanguageStats
{
    public string id;
    public string text;
}