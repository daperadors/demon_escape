using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/StringScriptable")]
public class StringScriptable : ScriptableObject
{
    public string stringValue;
}