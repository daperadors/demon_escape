using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Spirit")]

public class Spirits : ScriptableObject
{
    public string spiritName;
    public bool longHair;
    public bool sobrenatural;
    public bool control;
    public bool greedy;
    public bool protectorBrujas;
    public bool fertility;
    public bool sage;
    public bool violent;
    public bool planetControl;
    public bool obsessive;

}
