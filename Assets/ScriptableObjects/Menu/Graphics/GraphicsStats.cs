using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/GraphicsStats")]
public class GraphicsStats : ScriptableObject
{
    public string type;
    public bool status;
    public string[] statusText;
    public int statusGraphSelected;
}