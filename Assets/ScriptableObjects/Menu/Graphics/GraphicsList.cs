using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/GraphicsList")]
public class GraphicsList : ScriptableObject
{
    public GraphicsStats[] grafics;
}