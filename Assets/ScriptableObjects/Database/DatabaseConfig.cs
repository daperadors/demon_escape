using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/DatabaseConfig")]
public class DatabaseConfig : ScriptableObject
{  
    public DatabaseTables[] tables;
}
[System.Serializable]
public class DatabaseTables
{
    public string tableName;
    public string[] columns;
}