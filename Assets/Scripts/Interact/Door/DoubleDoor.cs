using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleDoor : MonoBehaviour
{
    [SerializeField] private GameObject m_Door;
    [SerializeField] private GameObject m_otherDoor;

    private void Start()
    {
        InteractWithObjects.doubleDoor += openBoth;
    }
    private void openBoth()
    {
        if (m_Door.tag == "Door" || m_otherDoor.tag == "Door")
        {
            m_otherDoor.tag = "Door";
            m_Door.tag = "Door";
        }
    }
}
