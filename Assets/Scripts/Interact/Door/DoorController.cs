using System.Collections;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField] private GameObject m_Effect;
    private enum States { OPENED, CLOSED }
    private States m_CurrentState;

    [SerializeField] private GameObject[] _hands;
    private Animator m_Animator;

    public delegate void FingerPrintFade(GameObject hand, bool boolean);
    public static FingerPrintFade onFingerPrintFade;

    public bool _fadeIn = false;
    public bool _handChecked = false;
    private Coroutine _fadeCoroutine;


    private void Start()
    {
        EnemyController.onDoorCheckCollision += EnableDoorFingerPrints;
        onFingerPrintFade += DoFingerPrintFade;

        EnableDoorFingerPrints(false);
        m_Animator = GetComponent<Animator>();
        ChangeState(States.CLOSED);
        if (m_Effect != null) m_Effect.SetActive(false);
    }
    private void ChangeState(States newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState(m_CurrentState);
        InitState(newState);
    }

    private void InitState(States initState)
    {
        m_CurrentState = initState;

        switch (m_CurrentState)
        {
            case States.OPENED:
                AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.OPENDOOR);
                if (m_Effect != null) m_Effect.SetActive(true);
                break;
            case States.CLOSED:
                AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.CLOSEDOOR);
                if (m_Effect != null) m_Effect.SetActive(false);
                break;
            default:
                break;
        }
    }

    private void ExitState(States exitState)
    {
        switch (m_CurrentState)
        {
            case States.OPENED:
                m_Animator.SetBool("open", true);
                m_Animator.SetBool("close", false);
                break;
            case States.CLOSED:
                m_Animator.SetBool("close", true);
                m_Animator.SetBool("open", false);
                break;
            default:
                break;
        }
    }

    public void Interact()
    {
        switch (m_CurrentState)
        {
            case States.OPENED:
                ChangeState(States.CLOSED);
                break;
            case States.CLOSED:
                ChangeState(States.OPENED);
                break;
            default:
                break;
        }
    }
    public void CloseDoor()
    {
        ChangeState(States.CLOSED);
    }

    public void EnableDoorFingerPrints(bool enable)
    {
        if (_hands != null)
        {
            for (int x = 0, c = _hands.Length; x < c; x++)
            {
                _hands[x].SetActive(enable);
            }

            StartCoroutine(DesactiveFingerPrints());
        }
    }

    private IEnumerator DesactiveFingerPrints()
    {
        yield return new WaitForSeconds(30f);

        for (int x = 0, c = _hands.Length; x < c; x++)
        {
            _hands[x].SetActive(false);
        }
    }

    private void DoFingerPrintFade(GameObject hand, bool boolean)
    {
        if (boolean)
        {
            if (_fadeCoroutine == null && _handChecked == false)
            {
                _handChecked = true;
                _fadeCoroutine = StartCoroutine(FadeIn(hand));
            }
        }
    }

    private IEnumerator FadeIn(GameObject hand)
    {
        Color color;

        for (float x = 0.05f; x < 1; x += 0.05f)
        {
            color = hand.gameObject.GetComponent<MeshRenderer>().material.color;
            color.a = x;
            hand.gameObject.GetComponent<MeshRenderer>().material.color = color;
            yield return new WaitForSeconds(.05f);
        }

        _fadeCoroutine = null;
    }
}


