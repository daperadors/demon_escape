using System;
using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class InteractWithObjects : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_textInteract;
    [SerializeField] private ItemChange m_Items;
    [SerializeField] private float m_MaxDistance = .5f;
    [SerializeField] private LayerMask m_LayerInteract;
    [SerializeField] private LayerMask m_LayerItems;

    public delegate void UseItem(bool active);
    public delegate void PutKeypad();
    public delegate void HideCupboard(float n1, float n2);
    public delegate void DoubleDoorDelegator();
    public delegate void CanInteract();

    public static UseItem onUseItem;
    public static PutKeypad putKeypad;
    public static HideCupboard hideCupboard;
    public static DoubleDoorDelegator doubleDoor;
    public static CanInteract canInteract;

    private NewPlayerController m_Player;
    private InputActionAsset m_Input;
    private bool _canInteractObjects = true;

    [Header("Interactable Items")]
    [SerializeField] private SpiritBox _spiritBox;
    [SerializeField] private GameObject _flashLightItem;
    [SerializeField] private GameObject _violetFlashLight;
    private void Awake()
    {
        canInteract += PlayerCanInteract;
    }
    private void Start()
    {
        m_Player = GetComponentInParent<NewPlayerController>();
        m_textInteract.enabled = false;
        m_Input = NewPlayerController.InputM;
    }
    void OnEnable()
    {
        NewPlayerController.onAtackInteraction += DisableInteraction;
    }
    void OnDisable()
    {
        NewPlayerController.onAtackInteraction -= DisableInteraction;
        if (m_textInteract != null) m_textInteract.enabled = false;
    }

    void Update()
    {

        if (!_canInteractObjects || m_Player.Hide) return;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, m_MaxDistance, m_LayerInteract))
        {
            int layer = hit.collider.gameObject.layer;
            GameObject gameObject = hit.collider.gameObject;
            if (layer == LayerMask.NameToLayer("Drawer") || layer == LayerMask.NameToLayer("Furniture")) return;
            if (gameObject.CompareTag("DoorBlocked"))
            {
                if (m_Input.FindAction("Interact").triggered)
                {
                    if (m_Items._itemActual != null && m_Items._itemActual.tag == "Key")
                    {
                        m_Items.DropInventoryItem();
                        gameObject.tag = "Door";
                        gameObject.GetComponent<DoorController>().Interact();
                        NewPlayerController.InsideEscapeRoom = true;
                        if (!m_Player.PlayerInsideHouse) doubleDoor?.Invoke();
                    }
                    else if (m_Items._itemActual != null && m_Items._itemActual.tag == "FinalKey")
                    {
                        if (m_Player.EnemyInDistance(125f))
                        {
                            VoiceManager.onPlayVoice.Invoke(Voices.ENEMYNEAR);
                            return;
                        }
                        DoubleDoor door = null;
                        if (gameObject.TryGetComponent<DoubleDoor>(out door))
                        {
                            m_Items.DropInventoryItem();
                            gameObject.tag = "Door";
                            doubleDoor.Invoke();
                        }
                    }
                    else
                    {
                        VoiceManager.onPlayVoice.Invoke(Voices.DOORBLOCKED);
                        return;
                    }
                }
            }
            if (gameObject.CompareTag("KeypadSupporter"))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if (m_Items._itemActual && m_Items._itemActual.tag == "Keypad")
                    {
                        m_Items.DropInventoryItem();
                        putKeypad.Invoke();
                    }
                }
                return;
            }

            else
            {
                m_textInteract.enabled = true;
                //if (VoiceManager._enableVoice == false)
                //{
                //    //m_textInteract.transform.DOScale(1.1f, .5f);

                //}
                if (layer == LayerMask.NameToLayer("Item"))
                {
                    if (m_Input.FindAction("Interact").triggered)
                    {
                        AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.GRABITEM);
                        m_Items.InventoryAddObject(gameObject.tag, gameObject);
                        ObjectInteractionVoice.onInitializeVoiceItem.Invoke(gameObject.tag);

                    }
                }

                else
                {
                    if (m_Input.FindAction("Interact").triggered)
                    {
                        if (gameObject.CompareTag("Door"))
                        {
                            gameObject.GetComponent<DoorController>().Interact();
                        }
                        else if (gameObject.CompareTag("Pickup"))
                        {
                            PHController.startDrive?.Invoke();
                            GameManager.Instance.DisableOrEnableOtherObjects(true);
                        }
                        else if (gameObject.CompareTag("FlashlightItem"))
                        {
                            _flashLightItem.SetActive(true);
                            gameObject.SetActive(false);
                            SQLController.Instance.SaveItemInventoryStatus(_flashLightItem.GetComponent<ObjectsId>().id, 1);
                        }
                        else if (gameObject.CompareTag("Switch"))
                        {
                            AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.SWITCH);
                            gameObject.GetComponent<SwitchController>().Interact();

                        }
                        else if (gameObject.CompareTag("Laptop"))
                        {
                            gameObject.GetComponent<LaptopController>().Interact();

                        }
                        else if (gameObject.CompareTag("Ouija"))
                        {
                            Debug.Log("dadsd");
                            gameObject.GetComponentInParent<OuijaView>().Interact();
                        }

                        else if (gameObject.CompareTag("TableObstacle"))
                        {
                            if(m_Items._itemActual != null && m_Items._itemActual.tag.Equals("CrowBar"))
                            {
                                m_Items._itemActual.GetComponent<Animator>().Play("Bang");
                                gameObject.GetComponent<ObstacleController>().Interact();
                            }
                            else if(m_Items._itemActual == null || !m_Items._itemActual.tag.Equals("CrowBar")) {
                                VoiceManager.onPlayVoice(Voices.BREAK_OBSTACLE);
                            }
                        }

                        else if (gameObject.CompareTag("Cupboard"))
                        {
                            if (!m_Player.Hide)
                            {
                                Debug.Log("entra cupboard");
                                m_Player.Hide = true;
                                gameObject.GetComponentInParent<TriggersInteract>().StartMinigame();
                                AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.CUPBOARD);
                                gameObject.GetComponentInParent<Animator>().Play("Hide");
                                if (NewPlayerController.playerHide != null) NewPlayerController.playerHide.Invoke();
                                m_Player.cupboard = gameObject;
                                StartCoroutine(StartMinigameAfterAnimationCupboard());
                            }

                        }

                        else if (gameObject.CompareTag("Tutorial"))
                        {
                            gameObject.GetComponentInParent<Animator>().Play("Flip");
                            StartCoroutine(GrassTimeOfset());
                            gameObject.layer = LayerMask.NameToLayer("Default");
                        }
                    }
                }
            }

        }

        else
        {
            if (m_textInteract.enabled != false) m_textInteract.enabled = false;
        }

        if (_violetFlashLight != null && _violetFlashLight.gameObject.activeInHierarchy)
        {
            if (Physics.Raycast(transform.position, transform.forward, out hit, m_MaxDistance))
            {
                if (hit.collider.gameObject.CompareTag("HandPrint"))
                {
                    DoorController.onFingerPrintFade(hit.collider.gameObject, true);
                    NewPlayerController.onCanSeeFingerPrint?.Invoke(true);
                }
                //else
                //{
                //    hit.collider.gameObject.GetComponentInParent<DoorController>()._handChecked = false;
                //    NewPlayerController.onCanSeeFingerPrint?.Invoke(false);
                //}
            }
        }

        InteractItem();

    }
    private void PlayerCanInteract()
    {
        _canInteractObjects = true;
    }
    private void DisableInteraction()
    {
        _canInteractObjects = !_canInteractObjects;
    }

    private void InteractItem()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (m_Items._itemActual != null)
            {
                if (m_Items.ItemIsActive(m_Items._itemActual) && m_Items._itemActual.tag.Equals("SpiritBox"))
                {
                    _spiritBox._isActive = !_spiritBox._isActive;
                }
            }
        }

        

        if (m_Input.FindAction("DropItem").triggered)
        {
            m_Items.DropItem();
        }
    }
    IEnumerator StartMinigameAfterAnimationCupboard()
    {
        yield return new WaitForSeconds(1.4f);
        hideCupboard.Invoke(-15, 20);
    }
 
    IEnumerator GrassTimeOfset()
    {
        yield return new WaitForSeconds(1.1f);
        AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.GRASS);
    }
}
