using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterHouseTrigger : MonoBehaviour
{
    [SerializeField] private bool m_TriggerEnter;
    public bool TriggerEnter { get { return m_TriggerEnter; } }

    private void Start()
    {
        if(TriggerEnter) NewPlayerController.insideHouse += PlayerInsideHouse;
    }
    private void PlayerInsideHouse()
    {
        gameObject.SetActive(false);
    }
}
