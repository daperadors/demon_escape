using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchController : MonoBehaviour
{
    [SerializeField] private GameObject[] m_Lights;
    private enum States { OPENED, CLOSED }
    private States m_CurrentState;
    //SOUND 

    private void Start()
    {
    }
    private void ChangeState(States newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState(m_CurrentState);
        InitState(newState);
    }

    private void InitState(States initState)
    {
        m_CurrentState = initState;

        switch (m_CurrentState)
        {
            case States.OPENED:
                break;
            case States.CLOSED:
                break;
            default:
                break;
        }
    }


    private void UpdateState(States updateState)
    {

        switch (updateState)
        {
            case States.OPENED:
                break;
            case States.CLOSED:
                break;
            default:
                break;
        }
    }

    private void ExitState(States exitState)
    {
        switch (m_CurrentState)
        {
            case States.OPENED:
                break;
            case States.CLOSED:
                break;
            default:
                break;
        }
    }

    public void Interact()
    {
        switch (m_CurrentState)
        {
            case States.OPENED:
                OnOrOfLights(false);
                ChangeState(States.CLOSED);
                break;
            case States.CLOSED:
                OnOrOfLights(true);
                ChangeState(States.OPENED);
                break;
            default:
                break;
        }
    }

    private void OnOrOfLights(bool status)
    {
        foreach(GameObject light in m_Lights)
        {
            light.GetComponent<Light>().enabled = status;
        }
    }
}