using System.Linq;
using UnityEngine;

public class AudioLibrary : MonoBehaviour
{

    public static AudioLibrary _audioLibrary;

    [SerializeField] private AudiosObjects[] _audiosObjects;

    [Header("Audio Sources")]
    [SerializeField] private AudioSource _enemyAudio;
    [SerializeField] private AudioSource _playerAudioMovement;
    [SerializeField] private AudioSource _ambientAudio;
    [SerializeField] private PlayerSounds[] m_PlayerSounds;
    [SerializeField] private AudioSource _audioObject;
    [SerializeField] private EnemySounds[] m_EnemySounds;

    public static AudioLibrary Instance;

    public delegate void ObjectSound(AudioName audioName);
    public delegate void EnemySound(EnemyStatesSounds audioName, bool loop);
    public delegate void StopPlayAllSounds(bool status);

    public static ObjectSound objectSound;
    public static EnemySound enemySound;
    public static StopPlayAllSounds stopPlayAllSounds;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        enemySound += PlayEnemySound;
        objectSound += PlayObjectSound;
        NewPlayerController.setNewAudio += SetNewAudioPlayerMovement;
        NewPlayerController.stopAudio += StopMovementSounds;
        EnemyController.stopAudioEnemy += StopEnemySounds;
        MenuController.continueGame += ContinueGame;
        AudioLibrary.stopPlayAllSounds += StopPlayAllPESounds;

    }

    private void Start()
    {
        SQLController.Instance.SaveStatusRoom("InsideHouse", 0);
    }

    private void PlayEnemySound(EnemyStatesSounds state, bool loop = false)
    {
        EnemySounds enemySounds = m_EnemySounds.Where(s => s.state == state).FirstOrDefault();
        _enemyAudio.clip = enemySounds.audio;
        _enemyAudio.loop = loop;

        if (enemySounds != null)
        {   

            if (state == EnemyStatesSounds.RUNING) _enemyAudio.pitch = 1.5f;

            else _enemyAudio.pitch = 1;

            if (!_enemyAudio.loop)
            {
                if(!_enemyAudio.isPlaying)
                    _enemyAudio.Play();
            }
            else
            {
                _enemyAudio.Play();
            }
        }
    }

    public void PlayObjectSound(AudioName audioName)
    {
        AudiosObjects audioObj = _audiosObjects.Where(x => x._audioName == audioName).FirstOrDefault();

        _audioObject.clip = audioObj._audioObjectName;
        //if(!_audioObject.isPlaying)
        _audioObject.Play();
    }

    private void SetNewAudioPlayerMovement(PlayerStatesSounds state, bool outside)
    {
        PlayerSounds playerSounds = m_PlayerSounds.Where(s => s.state == state && s.outside == outside).FirstOrDefault();
        if (playerSounds != null)
        {
            if(state == PlayerStatesSounds.SHIFT)
            {
                if (!_playerAudioMovement.isPlaying)
                {
                    _playerAudioMovement.clip = playerSounds.audio;
                    _playerAudioMovement.loop = true;
                    _playerAudioMovement.pitch = 0.5f;
                    _playerAudioMovement.Play();
                }
            }
            else
            {
                _playerAudioMovement.clip = playerSounds.audio;
                _playerAudioMovement.loop = true;
                _playerAudioMovement.pitch = (playerSounds.state == PlayerStatesSounds.RUNNING ? .9f : playerSounds.state == PlayerStatesSounds.WALKING ? 0.7f : 0.5f);
                _playerAudioMovement.Play();
            }
        }
    }
    private void StopPlayAllPESounds(bool status)
    {
        if (status)
        {
            StopEnemySounds();
            StopMovementSounds();
        }
        else
        {
            PlayEnemySounds();
            PlayMovementSounds();
        }
    }
    private void StopMovementSounds()
    {
        _playerAudioMovement.Stop();
    }    
    private void StopEnemySounds()
    {
        _enemyAudio.Stop();
    }   
    private void PlayMovementSounds()
    {
        if(_playerAudioMovement.loop) _playerAudioMovement.Stop();
    }    
    private void PlayEnemySounds()
    {
        if(_enemyAudio.loop) _enemyAudio.Stop();
    }
    private void ContinueGame()
    {
        if (SQLController.Instance.GetStatusRoom("InsideHouse")) NewPlayerController.insideHouse?.Invoke();
    }
    [System.Serializable]
    public class AudiosObjects
    {
        public AudioName _audioName;
        public AudioClip _audioObjectName;

    }
    public enum AudioName
    {
        BUG,
        DRAWER,
        CUPBOARD,
        SWITCH,
        PIANO,
        OPENDOOR,
        CLOSEDOOR,
        BLOCKEDDOOR,
        DROP,
        PASSWORD,
        WRONGPASSWORD,
        OPENSAFE,
        GRABITEM,
        SYRINGE,
        GEAR,
        WINDOWS,
        CLICK,
        ERROR,
        PAPER,
        GRASS
    }

}
[System.Serializable]
public class EnemySounds
{
    public EnemyStatesSounds state;
    public AudioClip audio;
}
public enum EnemyStatesSounds
{
    BUG,
    WALKING,
    RUNING,
    IDLE,
    STUNNED,
    ATTACK

}
[System.Serializable]
public class PlayerSounds
{
    public PlayerStatesSounds state;
    public AudioClip audio;
    public bool outside;
}
public enum PlayerStatesSounds
{
    WALKING,
    RUNNING,
    SHIFT
}