using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour
{
    [SerializeField] private GameObject triggerArea;
    [SerializeField] private Transform posIni;
    [SerializeField] private GameObject finalPuzzlePice;

    public delegate void EndMinigame();
    public static EndMinigame endMinigame;
    private void Start()
    {
        finalPuzzlePice.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "FinalPoint")
        {
            triggerArea.SetActive(false);
            endMinigame.Invoke();
            NewPlayerController.minigameFinished?.Invoke();
            finalPuzzlePice.SetActive(true);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Maze")
        {
            this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            this.gameObject.transform.position = posIni.position;
        }
    }
}
