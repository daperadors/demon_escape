using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static TriggersInteract;

public class MazeMinigameController : MonoBehaviour
{
    [SerializeField] Animator m_Animator;
    GameObject maze;
    float rotationX;
    float rotationZ;

    private void Start()
    {
        maze = this.gameObject;
        CollisionController.endMinigame += EndMinigame;
    }
    private void OnEnable()
    {
        m_Animator.SetBool("Up", true);
    }
    private void OnDisable()
    {
        m_Animator.SetBool("Up", false);
    }
    private void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            if(rotationX <= 30) rotationX += 30*Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            if (rotationX >= -30) rotationX -= 30 * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            if (rotationZ <= 30) rotationZ += 30 * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.W))
        {
            if (rotationZ >= -30) rotationZ -= 30 * Time.deltaTime;
        }
        maze.transform.rotation = Quaternion.Euler(rotationX, 0, rotationZ);
    }
    private void EndMinigame()
    {
        m_Animator.SetBool("Up", false);
        transform.rotation = Quaternion.identity;
    }

}
