using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TriggersInteract : MonoBehaviour
{
    [SerializeField] DoorController m_Door;
    [SerializeField] GameManager m_Manager;
    [SerializeField] RoomStats m_StatsRoom;
    [SerializeField] Camera m_MinigameCamera;
    [SerializeField] GameObject m_Light;
    [SerializeField] Transform m_EntranceWaypoint;
    [SerializeField] bool m_CameraDisabled = false;

    public delegate void SetNewCamera(Camera camera, GameObject light, float n1, float n2, string minigameName, bool cameraDisabled);
    public static SetNewCamera newMinigameCamera;
    public void StartRoom()
    {
        NewPlayerController.playerEnterRoom?.Invoke();
        NewPlayerController.InsideEscapeRoom = false;
        GameManager.Instance.StartEscapeRoom(m_StatsRoom.nameRoom, true, m_Door, m_StatsRoom.newTagDoor, gameObject, m_EntranceWaypoint);
    }    
    public void StartMinigame()
    {
        GameManager.Instance.ActiveOrDisableMinigame(m_StatsRoom.nameRoom, true);
        newMinigameCamera.Invoke(m_MinigameCamera, m_Light, 166, 193, m_StatsRoom.nameRoom, m_CameraDisabled);
    }
    public void DisableOrEnableDoor(bool status)
    {
        if (status) m_Door.tag = m_StatsRoom.newTagDoor;
        else m_Door.tag = "Door";
    }
}
