using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeCameraController : MonoBehaviour
{
    [SerializeField] Camera m_Camera;

    [HideInInspector] public float m_Yaw;
    [HideInInspector] public float m_Pitch;
    private float lookSpeed = 0.5f;
    float rotationX = 0;
    float rotationY = 0;
    // Start is called before the first frame update
    void Start()
    {
        m_Camera.enabled= false;
        lookSpeed = 0.3f;
        //transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (m_Camera.enabled)
        {
            m_Yaw = -Input.GetAxis("Mouse Y");
            m_Pitch = Input.GetAxis("Mouse X");

            rotationX += m_Yaw * lookSpeed;
            rotationY += m_Pitch * lookSpeed;
            rotationX = Mathf.Clamp(rotationX, -5, 20);
            rotationY = Mathf.Clamp(rotationY, -20, 20);
            m_Camera.transform.localRotation = Quaternion.Euler(rotationX, rotationY, 0);
        }
        else
        {
            return;
        }
    }
}
