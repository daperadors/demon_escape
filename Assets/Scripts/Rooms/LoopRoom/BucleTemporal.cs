using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucleTemporal : MonoBehaviour
{
    public int countdownTime;
    private int startCountdown;

    [SerializeField] private GameObject[] m_furnituresMovables;
    [SerializeField] private GameObject[] m_cardsCodes;
    [SerializeField] private Transform[] m_waypointsCards;
    [SerializeField] private TextureCards m_Textures;
    [SerializeField] private SafeController m_SafeController;
    [SerializeField] private LayerMask m_LayerButtons;



    private ArrayList usedWaypoints = new ArrayList();
    private Vector3[] positions = new Vector3[35];
    private Quaternion[] rotations = new Quaternion[35];
    private Vector3[] codePositions = new Vector3[4];
    private Quaternion[] codeRotations = new Quaternion[4];
    private Coroutine m_ReCreateRoom;


    public RoomPosition Data;

    public const string nameFileData = "LoopRoom";
    public const string pathData = "DataSavedGame/Rooms";

    private void Awake()
    {
        MenuController.continueGame += ContinueGame;
    }
    private void Start()
    {
        startCountdown = countdownTime;
        SetRandomCodesIntoCards();
        SetCardsInRandomPosition();
        SavePosition();
        SQLController.Instance.SaveStatusRoom("LoopRoom", 0);
    }
    private void OnEnable()
    {
        m_ReCreateRoom = StartCoroutine(CountdownRoutine());
    }
    private void OnDisable()
    {
        StopCoroutine(m_ReCreateRoom);
    }
    public void SavePosition()
    {

        int a = 0;
        foreach (GameObject t in m_furnituresMovables)
        {
            positions[a] = t.transform.localPosition;
            rotations[a] = t.transform.localRotation;
            a++;
        }
        a = 0;
        foreach (GameObject t in m_cardsCodes)
        {
            codePositions[a] = t.transform.localPosition;
            codeRotations[a] = t.transform.localRotation;
            a++;
        }

        Data._RoomPosition = positions;
        Data._RoomRotation = rotations;
        Data._CodePosition = codePositions;
        Data._CodeRotation = codeRotations;
        SaveData();
    }

    private void SaveData()
    {
        SaveLoadSystemData.SaveData(Data, pathData, nameFileData);
    }

    IEnumerator CountdownRoutine()
    {
        while (countdownTime > 0)
        {
            yield return new WaitForSeconds(1f);
            countdownTime--;
        }

        var dataFound = SaveLoadSystemData.LoadData<RoomPosition>(pathData, nameFileData);
        if (dataFound != null)
        {
            int a = 0;
            foreach (Quaternion r in dataFound._RoomRotation)
            {
                m_furnituresMovables[a].transform.localRotation = r;
                a++;
            }
            a = 0;
            foreach (Vector3 p in dataFound._RoomPosition)
            {
                m_furnituresMovables[a].transform.localPosition = p;
                a++;
            }
            a = 0;
            foreach (Quaternion r in dataFound._CodeRotation)
            {
                m_cardsCodes[a].transform.localRotation = r;
                a++;
            }
            a = 0;
            foreach (Vector3 p in dataFound._CodePosition)
            {
                m_cardsCodes[a].transform.localPosition = p;
                a++;
            }

        }
        else
        {
            Data = new RoomPosition();
            SaveData();
        }
        countdownTime = startCountdown;
        StartCoroutine(CountdownRoutine());
    }
    public void SetCardsInRandomPosition()
    {
        //int x = 0;
        foreach (GameObject t in m_cardsCodes)
        {
            int numeroAleatorio = Random.Range(0, m_waypointsCards.Length - 1);
            if (!usedWaypoints.Contains(numeroAleatorio))
            {
                //gameObjects[x].transform.position = waypoints[numeroAleatorio].position;
                t.transform.position = m_waypointsCards[numeroAleatorio].position;
                usedWaypoints.Add(numeroAleatorio);
                //x++;
            }
            else
            {
                while (usedWaypoints.Contains(numeroAleatorio))
                {
                    numeroAleatorio = Random.Range(0, m_waypointsCards.Length - 1);
                }
                //gameObjects[x].transform.position = waypoints[numeroAleatorio].position;
                t.transform.position = m_waypointsCards[numeroAleatorio].position;
                usedWaypoints.Add(numeroAleatorio);
            }

        }
    }
    private void SetRandomCodesIntoCards()
    {
        List<TextureCardsClass> material1 = new List<TextureCardsClass>();
        List<TextureCardsClass> material2 = new List<TextureCardsClass>();
        List<TextureCardsClass> material3 = new List<TextureCardsClass>();
        List<TextureCardsClass> material4 = new List<TextureCardsClass>();
        foreach (TextureCardsClass card in m_Textures.materials)
        {
            switch (card.number)
            {
                case 1:
                    material1.Add(card);
                    break;
                case 2:
                    material2.Add(card);
                    break;
                case 3:
                    material3.Add(card);
                    break;
                case 4:
                    material4.Add(card);
                    break;
                default:
                    break;
            }
        }
        TextureCardsClass material_1 = material1[Random.Range(0, material1.Count - 1)];
        TextureCardsClass material_2 = material2[Random.Range(0, material2.Count - 1)];
        TextureCardsClass material_3 = material3[Random.Range(0, material3.Count - 1)];
        TextureCardsClass material_4 = material4[Random.Range(0, material4.Count - 1)];

        m_cardsCodes[0].GetComponent<Renderer>().material = material_1.material;
        m_cardsCodes[1].GetComponent<Renderer>().material = material_2.material;
        m_cardsCodes[2].GetComponent<Renderer>().material = material_3.material;
        m_cardsCodes[3].GetComponent<Renderer>().material = material_4.material;

        string code = material_1.letterValue + "" + material_2.letterValue + "" + material_3.letterValue + "" + material_4.letterValue;
        m_SafeController.SetNewCode(code);
        //print(code);
    }
    private void ContinueGame()
    {
        if (SQLController.Instance.GetStatusRoom("LoopRoom")) GameManager.Instance.StartOrDisableRoom(gameObject, false);
    }
}
