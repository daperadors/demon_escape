using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoomPosition
{
    public Vector3[] _RoomPosition;
    public Quaternion[] _RoomRotation;
    public Vector3[] _CodePosition;
    public Quaternion[] _CodeRotation;
}
