using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SafeController : MonoBehaviour
{
    [SerializeField] private Camera m_CameraSafe;
    [SerializeField] private Camera m_CameraMain;
    [SerializeField] private DoorController m_Door;
    [SerializeField] private DoorController m_OtherDoor;
    [SerializeField] private SphereCollider m_Collider;
    [SerializeField] private LayerMask m_LayerButtons;
    [SerializeField] private string m_Room;
    private Animator m_Animator;
    private string m_Code = "1111";
    private string m_EnterCode = "";
    private void Awake()
    {
        MenuController.continueGame += ContinueGame;
    }
    private void Start()
    {
        m_Animator =  GetComponent<Animator>();
        if (m_CameraSafe == null)
        {
            gameObject.SetActive(false);
            m_CameraSafe = m_CameraMain;
            m_CameraMain = null;
        }
    }
    private void Update()
    {
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            Ray rayOrigin = m_CameraSafe.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(rayOrigin, out hit, 5f, m_LayerButtons))
            {
                AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.PASSWORD);
                NewNumber(hit.collider.gameObject.GetComponent<ButtonsSafeBox>().Number);
            }
        }
    }
    public void SetNewCode(string code)
    {
        m_Code = code;
    }

    public void NewNumber(int number)
    {
        m_EnterCode += number.ToString();
        print(m_EnterCode);
        if (m_EnterCode.Length == 4)
            CheckCode();
    }
    private void CheckCode()
    {
        Debug.Log(m_EnterCode + " "+ m_Code);

        if (m_EnterCode == m_Code)
        {
            Debug.Log("Correct code.");
            if(m_CameraMain != null)
            {
                m_CameraMain.enabled = true;
                m_CameraSafe.enabled = false;
                m_Collider.enabled = false;
                AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.OPENSAFE);
                m_Animator.Play("OpenSafe");
                SQLController.Instance.SetRoomCompleted(m_Room);
                NewPlayerController.startRoom.Invoke(m_Room, false);
                NewPlayerController.minigameFinished.Invoke();
            }
            else
            {
                m_Door.gameObject.tag = "Door";
                m_Door.Interact();
                m_OtherDoor.CloseDoor();
                SQLController.Instance.SetRoomCompleted(m_Room);
                NewPlayerController.startRoom.Invoke(m_Room, false);
                NewPlayerController.InsideEscapeRoom = true;
            }
        }
        else
        {
            AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.WRONGPASSWORD);
        }
        m_EnterCode = "";

    }
    private void ContinueGame()
    {
        if (m_CameraMain != null && SQLController.Instance.GetStatusRoom("LoopRoom"))
        {
            m_Collider.enabled = false;
        }
    }
}
