using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalCamera : MonoBehaviour
{
    [SerializeField] private Transform m_PlayerCameraTransform;
    [SerializeField] private Transform m_PortalTransform;
    [SerializeField] private Transform m_OtherPortalTransform;
    private Camera m_Camera;
    private void Start()
    {
        m_Camera = GetComponent<Camera>();
    }
    void LateUpdate()
    {
        Vector3 offsetFromPortal = m_PlayerCameraTransform.position - m_OtherPortalTransform.position;
        transform.position = m_PortalTransform.position + offsetFromPortal;

        float angularDifferenceBetweenPortalRotations = Quaternion.Angle(m_PortalTransform.rotation, m_OtherPortalTransform.rotation);
        Quaternion portalRotationDifference = Quaternion.AngleAxis(angularDifferenceBetweenPortalRotations, Vector3.up);
        Vector3 newCameraDirecion = portalRotationDifference * m_PlayerCameraTransform.forward;
        transform.rotation = Quaternion.LookRotation(newCameraDirecion, Vector3.up);

        m_Camera.fieldOfView = m_PlayerCameraTransform.gameObject.GetComponent<Camera>().fieldOfView;



        //Quaternion direction = Quaternion.Inverse(transform.rotation) * m_PlayerCameraTransform.rotation;
        //m_OtherPortalTransform.localEulerAngles = new Vector3(direction.eulerAngles.x, direction.eulerAngles.y * 180, direction.eulerAngles.z);

        //Vector3 distance = transform.InverseTransformPoint(m_PlayerCameraTransform.position);
        //m_OtherPortalTransform.localPosition = -new Vector3(distance.x, -distance.y, distance.z);
    }
}
