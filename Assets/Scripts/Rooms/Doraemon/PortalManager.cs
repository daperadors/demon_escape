using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalManager : MonoBehaviour
{
    [SerializeField] private Camera[] m_Cameras;
    [SerializeField] private Material[] m_Materials;  
    void Start()
    {
        int index = 0;
        foreach(Camera camera in m_Cameras)
        {
            if (camera.targetTexture != null)
            {
                ReleaseTargetTexture(camera);
            }
            CameraSetter(camera, m_Materials[index]);
            index++;
        }
    }
    private void ReleaseTargetTexture(Camera camera)
    {
        camera.targetTexture.Release();
    }
    private void CameraSetter(Camera camera, Material material)
    {
        camera.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        material.mainTexture = camera.targetTexture;
    }
}
