using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTeleporter : MonoBehaviour
{
    [SerializeField] private Transform m_PlayerTransform;
    [SerializeField] private Transform m_LinkedCameraPortal;
    [SerializeField] private Transform m_LinkedPortalTransform;
    [SerializeField] private BoxCollider m_OtherPortalCollider;
    [SerializeField] private GameObject m_Flashlight;
    [SerializeField] private bool m_EnableFlashlight;

    private bool m_PlayerIsEnteringPortal = false;
    private void Update()
    {
        if (m_PlayerIsEnteringPortal)
        {
            m_OtherPortalCollider.enabled = false;
            Vector3 portalToPlayer = m_PlayerTransform.position - transform.position;
            float dotProduct = Vector3.Dot(transform.up, portalToPlayer);
            if (dotProduct < 0f)
            {
                float rotationDiff = -Quaternion.Angle(transform.rotation, m_LinkedPortalTransform.rotation);
                m_PlayerTransform.Rotate(Vector3.up, rotationDiff);

                Vector3 direction = m_LinkedPortalTransform.position + portalToPlayer;
                m_PlayerTransform.position = new Vector3(m_LinkedCameraPortal.position.x, direction.y, m_LinkedCameraPortal.position.z);

                m_PlayerIsEnteringPortal = false;
                m_Flashlight.SetActive(m_EnableFlashlight);
                StartCoroutine(WaitForEnablePortal());
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            m_PlayerIsEnteringPortal = true;
        }
    }    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            m_PlayerIsEnteringPortal = false;
        }
    }
    IEnumerator WaitForEnablePortal()
    {
        yield return new WaitForSeconds(1f);
        m_OtherPortalCollider.enabled = true;
    }
}
