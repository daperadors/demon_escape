using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalRoomController : MonoBehaviour
{
    [SerializeField] GameObject m_DoraemonRoom;
    private void Awake()
    {
        MenuController.continueGame += ContinueGame;
    }
    void Start()
    {
        SQLController.Instance.SaveStatusRoom("DoraemonRoom", 0);
    }
    void ContinueGame()
    {
        if (SQLController.Instance.GetStatusRoom("DoraemonRoom"))
        {
            GameManager.Instance.StartOrDisableRoom(gameObject, false);
            m_DoraemonRoom.tag = "DoorBlocked";
        }
    }
}
