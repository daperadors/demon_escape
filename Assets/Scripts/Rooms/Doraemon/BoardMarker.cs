using System.Linq;
using UnityEngine;

public class BoardMarker : MonoBehaviour
{
    [SerializeField] private Camera m_CameraPlayer;
    [SerializeField] private bool isErease = false;
    [SerializeField] private int m_PenSize = 5;

    private Renderer m_Renderer;
    private BoardController m_Board;
    private Color[] m_Colors;
    private Color[] m_Erease;
    private RaycastHit m_RaycastHit;
    private Vector2 m_TouchPos;
    private Vector2 m_LastTouchPos;
    private Quaternion m_LastTouchRot;
    private bool m_TouchLastFrame;
    void Start()
    {
        m_Renderer = GetComponent<Renderer>();
        m_Colors = Enumerable.Repeat(m_Renderer.material.color, m_PenSize * m_PenSize).ToArray();
        m_Erease = Enumerable.Repeat(Color.white, m_PenSize * m_PenSize).ToArray();
    }

    void Update()
    {
        Draw();
    }
    private void Draw()
    {
        if (Input.GetMouseButton(0))
        {
            Ray rayOrigin = m_CameraPlayer.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(rayOrigin, out m_RaycastHit, 5f))
            {
                if (m_RaycastHit.transform.CompareTag("WriteableBoard"))
                {
                    if (m_Board == null) m_Board = m_RaycastHit.transform.GetComponent<BoardController>();

                    m_TouchPos = new Vector2(m_RaycastHit.textureCoord.x, m_RaycastHit.textureCoord.y);

                    var x = (int)(m_TouchPos.x * m_Board.TextureSize.x - (m_PenSize / 2));
                    var y = (int)(m_TouchPos.y * m_Board.TextureSize.y - (m_PenSize / 2));
                    if (y < 0 || y > m_Board.TextureSize.y || x < 0 || x > m_Board.TextureSize.x) return;

                    if (m_TouchLastFrame)
                    {
                        m_Board.Texture.SetPixels(x, y, m_PenSize, m_PenSize, isErease ? m_Erease : m_Colors);
                        for (float f = 0.01f; f < 1.00f; f += 0.01f)
                        {
                            var lerpX = (int)Mathf.Lerp(m_LastTouchPos.x, x, f);
                            var lerpY = (int)Mathf.Lerp(m_LastTouchPos.y, y, f);
                            m_Board.Texture.SetPixels(lerpX, lerpY, m_PenSize, m_PenSize, isErease ? m_Erease : m_Colors);
                        }

                        m_Board.Texture.Apply();
                    }
                    m_LastTouchPos = new Vector2(x, y);
                    m_LastTouchRot = transform.rotation;
                    m_TouchLastFrame = true;
                    return;
                }
            }
        }
        m_Board = null;
        m_TouchLastFrame = false;
    }
}
