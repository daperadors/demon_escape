using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class LaptopController : MonoBehaviour
{
    [SerializeField] private Animator m_LaptopAnimator;
    [SerializeField] private GameObject m_LaptopUI;
    [SerializeField] private TextMeshProUGUI[] m_DateAndHour;
    [SerializeField] private GameObject m_Mouse;
    [SerializeField] private NewPlayerController m_Player;
    [SerializeField] private GameObject[] m_OtherUI;
    [SerializeField] private TextMeshProUGUI m_HeaderTitle;
    [SerializeField] private TMP_InputField m_InputAnswer;
    [SerializeField] private List<ContentFiles> m_ContentFiles;
    [SerializeField] private GameObject m_Window;
    [SerializeField] private GameObject m_WindowIcon;
    [SerializeField] private GameObject m_FileWindow;
    [SerializeField] private GameObject m_ExplorerWindow;
    [SerializeField] private GameObject m_Folder;
    [SerializeField] private GameObject m_Form;
    [SerializeField] private GameObject m_FormSearchHide;
    private LaptopFormAnswer m_Riddle;
    private InputActionAsset m_Input;
    private bool m_LaptopActive = false;
    public delegate void RightAnswer();
    public static RightAnswer rightAnswer;
    void Start()
    {
        ActiveOrDisableCanvas(false);
        m_Window.SetActive(false);
        m_Input = NewPlayerController.InputM;
        CloseAllWithoutWindow();
        ClassroomController.newRiddle += SetNewRiddle;
    }
    private void Update()
    {
        if (!m_LaptopActive) return;

        if (m_Input.FindAction("Escape").triggered) ExitLaptop();
        MouseControl();

    }
    public void Interact()
    {
        m_LaptopActive = true;
        ActiveOrDisableCanvas(m_LaptopActive);
        AudioLibrary.stopPlayAllSounds?.Invoke(false);
        SetHourAndDate();
        //m_LaptopAnimator.Play("OpenLaptop");
    }
    private void ActiveOrDisableCanvas(bool status)
    {
        for (int i = 0; i < 0; i++) m_OtherUI[i].SetActive(!status);
        m_Player.enabled = !status;
        m_LaptopUI.SetActive(status);
        if (status)
        {
            AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.WINDOWS);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
    private void MouseControl()
    {
        m_Mouse.transform.position = Input.mousePosition;
    }
    private void SetHourAndDate()
    {
        DateTime dateTime = DateTime.UtcNow.Date;
        string date = dateTime.ToString("dd/MM/yyyy");
        m_DateAndHour[0].text = date.Substring(0, date.Length - 4) + "2009";
        m_DateAndHour[1].text = DateTime.Now.ToString("HH:mm");
    }
    public void OpenFile(string file)
    {
        AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.CLICK);
        GetFileInfo(file);
    }
    private void GetFileInfo(string filename)
    {
        ContentFiles file = m_ContentFiles.Where(f => f.type == filename).FirstOrDefault();
        CloseAllWithoutWindow();
        if (!m_Window.activeInHierarchy) m_LaptopAnimator.Play("Folder");
        m_Window.transform.parent.gameObject.SetActive(true);
        m_Window.SetActive(true);
        m_FileWindow.SetActive(true);
        m_HeaderTitle.text = filename;
        if (file != null)
        {
            m_HeaderTitle.text = file.headerTitle;
            if (file.type == "Document")
            {
                m_FileWindow.transform.Find("Title").gameObject.GetComponent<TextMeshProUGUI>().text = file.title;
                m_FileWindow.transform.Find("Content - Text").gameObject.GetComponent<TextMeshProUGUI>().text = file.content;
                if (file.image != null)
                    m_FileWindow.transform.Find("Image").gameObject.GetComponent<Image>().sprite = file.image;
            }
        }
        else OpenContentLeftHeader("");
    }
    public void CloseWindow()
    {
        CloseAllWithoutWindow();
        m_Window.SetActive(false);
        m_Window.transform.parent.gameObject.SetActive(false);
        m_ExplorerWindow.SetActive(false);
        m_ExplorerWindow.transform.parent.gameObject.SetActive(false);
    }
    private void CloseAllWithoutWindow()
    {
        m_Folder.SetActive(false);
        m_FileWindow.SetActive(false);
        m_ExplorerWindow.SetActive(false);
        m_ExplorerWindow.transform.parent.gameObject.SetActive(false);
        m_WindowIcon.SetActive(false);
        m_Form.SetActive(false);
        m_FormSearchHide.SetActive(false);
        m_Folder.transform.Find("No content").gameObject.SetActive(false);
        m_Folder.transform.Find("Files").gameObject.SetActive(false);
    }
    public void OpenContentLeftHeader(string filename)
    {
        AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.CLICK);
        CloseAllWithoutWindow();
        if (!m_Window.activeInHierarchy) m_LaptopAnimator.Play("Folder");
        m_Window.SetActive(true);
        m_Folder.SetActive(true);
        m_HeaderTitle.text = filename;

        if (filename == "Documents") m_Folder.transform.Find("Files").gameObject.SetActive(true);
        else if (filename == "Form") OpenFormWindow();
        else if (filename == "Explorer")
        {
            OpenExplorer();
            return;
        }
        else m_Folder.transform.Find("No content").gameObject.SetActive(true);
    }
    private void OpenFormWindow()
    {
        m_Window.SetActive(true);
        if (m_Riddle.answer != "SearchHide")
        {
            m_Form.SetActive(true);
            m_Form.transform.Find("Image").GetComponent<Image>().sprite = m_Riddle.image;
        }
        else
        {
            m_FormSearchHide.SetActive(true);
            m_FormSearchHide.transform.Find("Image").GetComponent<Image>().sprite = m_Riddle.image;
            m_FormSearchHide.transform.Find("Button").transform.localPosition = m_Riddle.buttonPostion;
        }
    }
    public void CheckAnswer()
    {
        CompareAnswer();
    }
    private void CompareAnswer()
    {
        if (m_Riddle.answer.ToLower().Trim().Equals(m_InputAnswer.text.ToString().Trim().ToLower()))
        {
            print("Good!");
            ExitLaptop();
            rightAnswer.Invoke();
        }
        else
        {
            AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.ERROR);
        }
    }
    private void OpenExplorer()
    {
        m_Window.SetActive(false);
        m_ExplorerWindow.transform.parent.gameObject.SetActive(true);
        m_ExplorerWindow.SetActive(true);
        m_LaptopAnimator.Play("Explorer");
    }
    private void ExitLaptop()
    {
        m_LaptopActive = false;
        ActiveOrDisableCanvas(m_LaptopActive);
    }
    public void OpenWindowIcon()
    {
        if (m_WindowIcon.activeInHierarchy)
        {
            m_LaptopAnimator.Play("WindowIconHide");
            StartCoroutine(WaitForHide(m_WindowIcon, .5f));
        }
        else
        {
            m_WindowIcon.SetActive(true);
            m_LaptopAnimator.Play("WindowIconUnHide");
        }
    }
    public void SetNewRiddle(LaptopFormAnswer riddle)
    {
        m_Riddle = riddle;
    }
    public void ClickAnswerButton()
    {
        print("Good!");
        ExitLaptop();
        rightAnswer.Invoke();
    }
    IEnumerator WaitForHide(GameObject objectToHide, float time)
    {
        yield return new WaitForSeconds(time);
        objectToHide.SetActive(false);
    }
}
