using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardController : MonoBehaviour
{
    [SerializeField] private Vector2 m_TextureSize = new Vector2(2048, 2048);
    private Texture2D m_Texture;

    public Texture2D Texture { get{ return m_Texture; } set { m_Texture = value; } }
    public Vector2 TextureSize { get{ return m_TextureSize; } set { m_TextureSize = value; } }
    void Start()
    {
        var renderer = GetComponent<Renderer>();
        m_Texture = new Texture2D((int)m_TextureSize.x, (int)m_TextureSize.y);
        renderer.material.mainTexture = m_Texture;
    }
}
