using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class ListenerView : MonoBehaviour
{
    KeywordRecognizer _keywordRecognizer;
    private Dictionary<string, Action> _actions;
    [SerializeField] private OuijaView _ouijaView;
    private string _word;
    private List<string> _questions;
    private List<string> _spiritsNames;
    private bool _isActive = false;

    private void Start()
    {
        _isActive = false;
        _questions = new List<string>();
        _spiritsNames = new List<string>() { "abyzou", "asmodeo", "astarot", "baal", "hecate", "lamia", "mammon", "tiamat"};
        OuijaView.onEnableOuija += EnableOuijaBoard;
        CreateActions("");

        TranslationController.newLanguage += CreateActions;
    }

    private void OnDisable()
    {
        if (_keywordRecognizer != null)
        {
            _keywordRecognizer.Stop();
            _keywordRecognizer.Dispose();
            _actions.Clear();
        }
    }
    private void OnDestroy()
    {
        if (_keywordRecognizer != null)
        {
            _keywordRecognizer.Stop();
            _keywordRecognizer.Dispose();
            _actions.Clear();
        }
    }
    private void CreateActions(string any)
    {
        if (_keywordRecognizer != null)
        {
            _keywordRecognizer.Stop();
            _keywordRecognizer.Dispose();
            _actions.Clear();
            _keywordRecognizer.OnPhraseRecognized -= InteractionOuija;
        }

        _actions = new Dictionary<string, Action>
        {
            //_actions.Add("pelo largo", LargeHairQuestion);
            { TranslationController.Instance.GetText("long_hair"), () => _ouijaView.ComproveLongHair() },
            { TranslationController.Instance.GetText("supernatural"), () => _ouijaView.ComproveSobrenatural() },
            { TranslationController.Instance.GetText("control_person"), () => _ouijaView.ComproveHumanControl() },
            { TranslationController.Instance.GetText("greed"), () => _ouijaView.ComproveGreedy() },
            { TranslationController.Instance.GetText("protector"), () => _ouijaView.ComproveProtection() },
            { TranslationController.Instance.GetText("fertility"), () => _ouijaView.ComproveFertility() },
            { TranslationController.Instance.GetText("wise"), () => _ouijaView.ComproveSage() },
            { TranslationController.Instance.GetText("violence"), () => _ouijaView.ComproveViolence() },
            { TranslationController.Instance.GetText("planet_control"), () => _ouijaView.ComprovePlanetControl() },
            { TranslationController.Instance.GetText("obsessive"), () => _ouijaView.ComproveObsession() },
            { TranslationController.Instance.GetText("witch_protector"), () => _ouijaView.ComproveWitchControl() },

            //SPIRITS NAME

            { "abyzou", () => _ouijaView.ComproveOuijaResult(_word) },
            { "asmodeo", () => _ouijaView.ComproveOuijaResult(_word) },
            { "astarot", () => _ouijaView.ComproveOuijaResult(_word) },
            { "bal", () => _ouijaView.ComproveOuijaResult(_word) },
            { "hecate", () => _ouijaView.ComproveOuijaResult(_word) },
            { "lamia", () => _ouijaView.ComproveOuijaResult(_word) },
            { "mamon", () => _ouijaView.ComproveOuijaResult(_word) },
            { "tiamat", () => _ouijaView.ComproveOuijaResult(_word) },
        };

        _keywordRecognizer = new KeywordRecognizer(_actions.Keys.ToArray());
        _keywordRecognizer.OnPhraseRecognized += InteractionOuija;
        _keywordRecognizer.Start();

    }
    private void InteractionOuija(PhraseRecognizedEventArgs word)
    {
        _word = word.text;
        Debug.Log(_ouijaView._canQuestion + " " + _isActive);
        Debug.Log("Input voice = "+ word.text);
        if(_ouijaView._canQuestion == true && _isActive == true && _ouijaView._inTrigger)
        {
            if (!_questions.Contains(word.text))
            {
                if(!_spiritsNames.Contains(_word))
                {
                    _ouijaView._questionQuantity++;
                    _questions.Add(word.text);
                }
                _actions[word.text]?.Invoke();
            }
        }
    }

    private void EnableOuijaBoard(bool status)
    {
        _isActive = status;
        Debug.Log(_isActive);
    }
}
