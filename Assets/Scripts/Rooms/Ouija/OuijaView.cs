using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static OuijaView;

public class OuijaView : MonoBehaviour
{
    [SerializeField] private Spirits[] _spirits;
    [SerializeField] private GameObject[] _candelsLights;
    [SerializeField] private GameObject _finalPuzzlePice;
    private bool _antimationFinished;
    private Spirits _selectedSpirit;
    public int _questionQuantity;
    public bool _canQuestion;
    private bool _enableOuija = false;
    public bool _inTrigger = false;

    [SerializeField] private GameObject _ouijaTable;
    private Animator _tableAnimator;

    public delegate void EnableOuija(bool status);
    public static EnableOuija onEnableOuija;

    public delegate void PieceAnimation(bool state);
    public static PieceAnimation onPieceAnimation;

    private void Start()
    {
        InitializeOuija();
        _finalPuzzlePice.SetActive(false);
        PieceView.onDisableInteraction += DisableInteraction;
    }

    private void InitializeOuija()
    {
        EnableCandels(true);
        _enableOuija = false;
        _tableAnimator = _ouijaTable.GetComponent<Animator>();
        _questionQuantity = 0;
        _selectedSpirit = _spirits[Random.Range(0, _spirits.Length)];
        Debug.Log(_selectedSpirit.spiritName);
    }

    public void ComproveLongHair()
    {
        if (_selectedSpirit.longHair)
        {
            SetPieceAnimation(true);
        }
        else
        {
            SetPieceAnimation(false);
        }
    }

    public void ComproveSobrenatural()
    {
        if (_selectedSpirit.sobrenatural)
        {
            SetPieceAnimation(true);
        }
        else
        {
            SetPieceAnimation(false);
        }
    }

    public void ComproveHumanControl()
    {
        if (_selectedSpirit.control) 
        {
            SetPieceAnimation(true);
        }else
        {
            SetPieceAnimation(false);
        }
    }    
    public void ComproveWitchControl()
    {
        if (_selectedSpirit.protectorBrujas) 
        {
            SetPieceAnimation(true);
        }else
        {
            SetPieceAnimation(false);
        }
    }

    public void ComproveGreedy()
    {
        if (_selectedSpirit.greedy)
        {
            SetPieceAnimation(true);
        }
        else
        {
            SetPieceAnimation(false);
        }
    }

    public void ComproveProtection()
    {
        if (_selectedSpirit.protectorBrujas)
        {
            SetPieceAnimation(true);
        }
        else
        {
            SetPieceAnimation(false);
        }
    }

    public void ComproveFertility()
    {
        if (_selectedSpirit.fertility)
        {
            SetPieceAnimation(true);
        }
        else
        {
            SetPieceAnimation(false);
        }
    }

    public void ComproveSage()
    {
        if (_selectedSpirit.sage)
        {
            SetPieceAnimation(true);
        }
        else
        {
            SetPieceAnimation(false);
        }
    }

    public void ComproveViolence()
    {
        if (_selectedSpirit.violent)
        {
            SetPieceAnimation(true);
        }
        else
        {
            SetPieceAnimation(false);
        }
    }

    public void ComprovePlanetControl()
    {
        if (_selectedSpirit.planetControl)
        {
            SetPieceAnimation(true);
        }
        else
        {
            SetPieceAnimation(false);
        }
    }

    public void ComproveObsession()
    {
        if (_selectedSpirit.obsessive)
        {
            SetPieceAnimation(true);
        }
        else
        {
            SetPieceAnimation(false);
        }
    }

    public void ComproveOuijaResult(string spiritName)
    {
        Debug.Log(_questionQuantity);
        if (_selectedSpirit.spiritName.ToLower().Equals(spiritName.ToLower()) && _questionQuantity >= 5)
        {
            GetComponent<BoxCollider>().enabled = false;
            EnableCandels(true);
            _tableAnimator.Play("Movement");
            _finalPuzzlePice.SetActive(true);
            PlayerExtras.setNewLookAt?.Invoke(_finalPuzzlePice.transform, 3f);
            gameObject.transform.GetChild(0).gameObject.layer = LayerMask.NameToLayer("Default");
        }
        else 
        {
            if (_questionQuantity < 5)
            {
                Debug.Log("menos de cinco");
                VoiceManager.onPlayVoice(Voices.OUIJA_LESS_MAX_QUESTIONS);
            }
        }
    }

    private void SetPieceAnimation(bool boolean)
    {
        onPieceAnimation?.Invoke(boolean);
    }

    private void EnableCandels(bool enable)
    {
        for(int x = 0, c = _candelsLights.Length; x < c; x++)
        {
            _candelsLights[x].SetActive(enable);
        }
    }

    private IEnumerator HauntedRoom()
    {
        int it = 5;

        while(it > 0)
        {
            EnableCandels(false);
            yield return new WaitForSeconds(.1f);
            EnableCandels(true);
            yield return new WaitForSeconds(.1f);
            it--;
        }
        EnableCandels(false);
    }

    public void Interact()
    {
        Debug.Log("Interact");
      
        if (!_enableOuija)
        {
            gameObject.transform.GetChild(0).gameObject.layer = LayerMask.NameToLayer("Default");
            onEnableOuija?.Invoke(true);
            EnableCandels(false);
            _enableOuija = true;
            _canQuestion = true;
        }
    }

    private void DisableInteraction()
    {
        StartCoroutine(InteractionCoroutine());
    }

    private IEnumerator InteractionCoroutine()
    {
        _canQuestion = false;
        yield return new WaitForSeconds(5);
        _canQuestion = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _inTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            EnableCandels(true);
            _canQuestion = false;
            _enableOuija = false;
            _inTrigger = false;
            onEnableOuija.Invoke(false);
            gameObject.transform.GetChild(0).gameObject.layer = LayerMask.NameToLayer("Interactable");
        }
    }
}