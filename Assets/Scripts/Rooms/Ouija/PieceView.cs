using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceView : MonoBehaviour
{

    private Animator _pieceAnimator;
    [SerializeField] private OuijaView _ouijaView;

    public delegate void DisableInteraction();
    public static DisableInteraction onDisableInteraction;

    private void Start()
    {
        _pieceAnimator = GetComponentInParent<Animator>();
        OuijaView.onPieceAnimation += AnimationAction;
    }

    private void OnDisable()
    {
        OuijaView.onPieceAnimation -= AnimationAction;
    }

    private void AnimationAction(bool boolean)
    {
        if (boolean == true)
        {
            _pieceAnimator.Play("YesMove");
            onDisableInteraction?.Invoke();
        }
        else
        {
            _pieceAnimator.Play("NoMove");
            onDisableInteraction?.Invoke();
        }
    }
}
