
using System.Collections;
using UnityEngine;

public class PalindromController : MonoBehaviour
{
    [SerializeField] private GameObject[] m_paints;
    [SerializeField] private Transform[] m_paintsWaypoints;
    [SerializeField] private ColoursPasswrdController m_ColoursPasswrd;

    private ArrayList usedWaypoints = new ArrayList();

    private string m_password;

    private void Awake()
    {
        m_password = "";
        SetColoursInRandomPosition();
        CompletePassword();
        MenuController.continueGame += ContinueGame;
    }
    void Start()
    {
        m_ColoursPasswrd.SetNewCode(m_password);
        SQLController.Instance.SaveStatusRoom("PalindromRoom", 0);
    }

    public void SetColoursInRandomPosition()
    {

        foreach (GameObject t in m_paints)
        {
            int numeroAleatorio = Random.Range(0, m_paintsWaypoints.Length);
            if (!usedWaypoints.Contains(numeroAleatorio))
            {
                t.transform.position = m_paintsWaypoints[numeroAleatorio].position;
                m_password += (numeroAleatorio + 1);
                usedWaypoints.Add(numeroAleatorio);
            }
            else
            {
                while (usedWaypoints.Contains(numeroAleatorio))
                {
                    numeroAleatorio = Random.Range(0, m_paintsWaypoints.Length);
                }
                t.transform.position = m_paintsWaypoints[numeroAleatorio].position;
                m_password += (numeroAleatorio + 1);
                usedWaypoints.Add(numeroAleatorio);
            }

        }
    }
    public void CompletePassword()
    {
        string invertPsswd = "";
        char[] invpssw = m_password.ToCharArray();

        System.Array.Reverse(invpssw);

        invertPsswd = new string(invpssw);
        m_password += invertPsswd;
    }
    void ContinueGame()
    {
        if (SQLController.Instance.GetStatusRoom("PalindromRoom")) GameManager.Instance.StartOrDisableRoom(gameObject, false);
    }
}
