using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using static InteractWithObjects;

public class ColoursPasswrdController : MonoBehaviour
{
    [SerializeField] private Camera m_Camera;
    [SerializeField] private LayerMask m_ColourSwitches;
    [SerializeField] private Light[] m_colorLights;
    [SerializeField] private Transform m_CupboardTransform;
    private Animator m_Animator;
    private string m_Code = "000000000000";
    private string m_EnterCode = "";
    private int helper = 5;

    [SerializeField] private List<GameObject> _switches;

    private void Start()
    {
        m_Animator = GetComponentInChildren<Animator>();
    }
    private void OnEnable()
    {
        lightDisabled();
    }
    private void Update()
    {
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, 4f, m_ColourSwitches))
            {
                AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.SWITCH);
                NewNumber(hit.collider.gameObject.GetComponent<ColourSwitch>().Number);
                if(m_EnterCode.Length>6)
                    helper--;
            }
        }
    }
    public void SetNewCode(string code)
    {
        m_Code = code;
        Debug.Log(m_Code);
    }

    public void NewNumber(int number)
    {
        m_EnterCode += number.ToString();
        print(m_EnterCode);
        char[] separate_code = m_EnterCode.ToCharArray();
        char[] separate_psswd = m_Code.ToCharArray();
        
        for (int x = 0; x < m_EnterCode.Length; x++)
        {
            if (separate_code[x] == separate_psswd[x])
            {
                AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.PIANO);
                if (m_EnterCode.Length <= 6)
                    m_colorLights[x].enabled = true;
                else
                {
                    m_colorLights[helper].enabled = true;
                }
            }
            else
            {
                lightDisabled();
                CheckCode();
                helper = 5;
            }
        }
        if (m_EnterCode.Length == 6)
        {
            StartCoroutine(HalfPuzzle(6));
        }
        if (m_EnterCode.Length == 12)
            CheckCode();
    }
    private void CheckCode()
    {
        Debug.Log(m_EnterCode + " " + m_Code);

        if (m_EnterCode == m_Code)
        {
            Debug.Log("Correct code.");
            SQLController.Instance.SetRoomCompleted("PalindromRoom");
            NewPlayerController.startRoom.Invoke("PalindromRoom", false);
            //m_Animator.Play("CabinetOpen");
            StartCoroutine(WaitForViewTransform());
            StartCoroutine(HalfPuzzle(5));
        }
        m_EnterCode = "";
    }

    public void lightDisabled()
    {
        foreach(Light l in m_colorLights)
        {
            l.enabled = false;
        }
    }
    IEnumerator HalfPuzzle(int counter)
    {
        EnableSwitches(false);

        int cont = counter;
        while (cont >= 0)
        {
            for (int x = 0; x < m_colorLights.Length; x++)
            {
                m_colorLights[x].enabled = !m_colorLights[x].enabled;
            }
            yield return new WaitForSeconds(0.2f);
            cont--;
        }

        EnableSwitches(true);
    }

    IEnumerator WaitForViewTransform()
    {
        PlayerExtras.setNewLookAt.Invoke(m_CupboardTransform, 3f, true);
        PlayerExtras.startLookAtCinematic.Invoke();
        yield return new WaitForSeconds(1f);
        AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.DRAWER);
        m_Animator.Play("CabinetOpen");
        NewPlayerController.startRoom.Invoke("PalindromRoom", false);
    }

    private void EnableSwitches(bool enabled)
    {
       for(int x = 0, c = _switches.Count; x < c; x++)
        {
            _switches[x].GetComponent<BoxCollider>().enabled = enabled;
        }
    }

}
