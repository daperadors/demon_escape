using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementsView : MonoBehaviour
{
    [Header("Objects Room")]
    [SerializeField] private GameObject[] _mirrors;
    [SerializeField] private GameObject[] _elements;

    public delegate void RefreshMirrors();
    public static RefreshMirrors onRefreshMirrors;


    private void Start()
    {
        onRefreshMirrors += RefreshRoomMirrors;
    }

    public void EnableRandomMirror()
    {
        int noReflectionProbability = Random.Range(0, 100);


        if(noReflectionProbability < 20)
        {
            _mirrors[1].SetActive(true);
        }
        else
        {
            _mirrors[0].SetActive(true);
        }

    }

    public void EnableMirrors(bool enable)
    {
        for(int i = 0; i < _mirrors.Length; i++) 
        {
            _mirrors[i].SetActive(enable);
        }
    }

    public void EnableObjectsDecoration(bool active)
    {
        for (int i = 0; i < _elements.Length; i++)
        {
            _elements[i].SetActive(active);
        }
    }

    public void EnableObjectIndexDecorator(int index) 
    {
        _elements[index].SetActive(true);
    }

    public GameObject[] GetElements() {
        return _elements;
    }


    public int GetObjectValue(int index)
    {
        int value = 0;
        
        switch (_elements[index].tag)
        {
            case "Reloj":
                value = 2;
                break;
            case "Cruz":
                value = 4;
                break;
            case "Cuadro":
                value = 6;
                break;
            default:
                break;
        }

        return value;

    }

    private void RefreshRoomMirrors()
    {
        _mirrors[0].GetComponentInChildren<ReflectionProbe>().RenderProbe();
    }

}

