using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorRoomController : MonoBehaviour
{
    [Header("Elements Room")]
    [SerializeField] private ElementsView[] _objectsRoom;
    [SerializeField] private DoorController _roomDoor;

    [Header("Canvas Help")]
    [SerializeField] private CanvasController _canvasController;

    [SerializeField] private NewPlayerController _player;

    [Header("Canvas elements")]

    [Header("Wait values")]
    private float _timeOutHelp = 5f;

    private int _quantityMirrors;
    private int[] _elementsValues = { 2, 4, 6 };

    [Header("Corutines Mirror Room")]
    private Coroutine _helpPaperCoroutine;
    private Coroutine _optionsCoroutine;

    private int _valueRoom = 0;
    private bool _endPuzzle = false;
    private int _indexCorrectOption;

    private void Awake()
    {
        MenuController.continueGame += ContinueGame;
    }
    void Start()
    {
        CanvasController.OnOpenCanvas += EnableMirrorHelp;
        //DoorController.OnInitializeMirrorRoom += InitializeMirrorRoom;
        CanvasController.OnOptionsOpen += InitializeOptions;

        _quantityMirrors = _objectsRoom.Length / 2;

        InitializeMirrorRoom();
        SQLController.Instance.SaveStatusRoom("MirrorRoom", 0);
    }

    //private void OnDisable()
    //{
    //    CanvasController.OnOpenCanvas -= EnableMirrorHelp;
    //    //DoorController.OnInitializeMirrorRoom -= InitializeMirrorRoom;
    //    CanvasController.OnOptionsOpen -= InitializeOptions;
    //}

    public void EnableMirrorHelp()
    {
        if (_helpPaperCoroutine == null)
        {
            _helpPaperCoroutine = StartCoroutine(MirrorHelp());
        }
    }

    private IEnumerator MirrorHelp()
    {
        _player.enabled = false;
        _canvasController.ShowUI();
        _player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        yield return new WaitForSeconds(_timeOutHelp);
        _canvasController.HideUI();
        _player.enabled = true;
        _helpPaperCoroutine = null;
    }

    private void InitializeMirrorRoom()
    {

        _valueRoom = 0;

        for (int i = 0; i < _objectsRoom.Length; i++)
        {
            _objectsRoom[i].EnableObjectsDecoration(false);
        }

        for (int x = 0; x < _objectsRoom.Length; x++)
        {
            int randomObject = Random.Range(0, _objectsRoom[x].GetElements().Length);
            ActiveRoomElements(_objectsRoom[x], randomObject);
            _valueRoom += _objectsRoom[x].GetObjectValue(randomObject);
        }

        ElementsView.onRefreshMirrors?.Invoke();
        SetOptionsValues();
        _endPuzzle = false;
    }

    private void ActiveRoomElements(ElementsView element, int index)
    {
        int objectProbability = Random.Range(0, 100);
        element.EnableRandomMirror();
        if (objectProbability > 20)
        {
            element.EnableObjectIndexDecorator(index);
        }
    }

    

    private void InitializeOptions()
    {
        print("Entra");
        if (_optionsCoroutine == null && !_endPuzzle)
        {
            _player.enabled = false;
            _optionsCoroutine = StartCoroutine(_canvasController.EnableOptionsCorrutine(true));
            Cursor.lockState = CursorLockMode.None;
            _optionsCoroutine = null;
        }

    }

    private void SetOptionsValues()
    {
        List<int> sumeOptions = new List<int>();
        int randomOption = Random.Range(0, _canvasController._valuesText.Length);

        _indexCorrectOption = randomOption;

        for (int x = 0; x < _canvasController._valuesText.Length; x++)
        {
            if (x == randomOption)
                _canvasController._valuesText[x].text = _valueRoom.ToString();
            else
            {
                SetOtherOptionsValues(x, sumeOptions);
            }
        }

        Debug.Log(_valueRoom.ToString()+ " AAAAAA");

    }

    private void SetOtherOptionsValues(int index, List<int> sumes)
    {
        int sume = TakeNewOption();

        while (sumes.Contains(sume))
        {
            sume = TakeNewOption();
        }

        sumes.Add(sume);

        _canvasController._valuesText[index].text = sume.ToString();

        int TakeNewOption()
        {
            int otherValue = _elementsValues[Random.Range(0, _elementsValues.Length)];
            int operand = Random.Range(0, 2);
            if (operand == 0)
            {
                sume = _valueRoom + otherValue;
            }
            else
            {
                sume = _valueRoom - otherValue;
            }

            return sume;
        }
    }

    public void ComprovePlayerOption(int index)
    {
        if (_endPuzzle) return;

        if (index == _indexCorrectOption)
        {
            _roomDoor.Interact();
            _roomDoor.tag = "Door";
            _endPuzzle = true;
            SQLController.Instance.SetRoomCompleted("MirrorRoom");
            NewPlayerController.startRoom.Invoke("MirrorRoom", false);
            NewPlayerController.InsideEscapeRoom = true;
        }
        else
        {
            InitializeMirrorRoom();
            Debug.Log("EQUIVOCADO");
        }

        StartCoroutine(_canvasController.EnableOptionsCorrutine(false));
        _player.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
    }
    private void ContinueGame()
    {
        if (SQLController.Instance.GetStatusRoom("MirrorRoom")) GameManager.Instance.StartOrDisableRoom(gameObject, false);
    }

    IEnumerator WaitForCoroutine()
    {
        yield return new WaitForSeconds(.5f);
    }
}
