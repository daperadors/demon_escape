using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{
    public bool _enabledCanvas = false;


    [SerializeField] private CanvasGroup _canvas;
    [SerializeField] private Camera m_Camera;
    [SerializeField] private GameObject[] _options;
    [SerializeField] public TextMeshProUGUI[] _valuesText;
    [SerializeField] public LayerMask _layerPaper;
    [SerializeField] private NewPlayerController _player;
    public delegate void CanvasOpen();
    public static event CanvasOpen OnOpenCanvas;

    public delegate void OptionsOpen();
    public static event OptionsOpen OnOptionsOpen;

    private InputActionAsset m_Input;
    private bool fadeIn = false;
    private bool fadeOut = false;
    private bool m_OpenEffectFinished = false;
    private bool _canOpenHelp;

    private float _waitShowOption = 1f;

    [SerializeField] private Button[] _buttons;


    private void Start()
    {
        m_Input = NewPlayerController.InputM;
        _canOpenHelp = true;
        EnableOptions(false);
    }

    //OPTIONS FUNCTIONS


    public void EnableOptions(bool active)
    {
        for (int x = 0; x < _options.Length; x++)
        {
            _options[x].gameObject.SetActive(active);
        }
    }

    public IEnumerator EnableOptionsCorrutine(bool active)
    {
        if (active)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        m_OpenEffectFinished = false;
        InteractionButtons(false);
        for (int x = 0; x < _options.Length; x++)
        {
            _options[x].SetActive(active);
            if (active)
                yield return new WaitForSeconds(_waitShowOption);
        }

        if (active)
            InteractionButtons(true);

        m_OpenEffectFinished = true;
    }


    //

    public void EnableCanvas(bool enable)
    {
                _canvas.gameObject.SetActive(enable);
    }

    public GameObject[] GetOptions()
    {
        return _options;
    }

    public void ShowUI()
    {
        fadeIn = true;
    }

    public void HideUI()
    {
        fadeOut = true;
    }


    public void InteractionButtons(bool interactable)
    {
        for (int x = 0; x < _buttons.Length; x++)
        {
            _buttons[x].interactable = interactable;
        }
    }


    private void Update()
    {
        if (m_Input.FindAction("Interact").triggered)
        {
            RaycastHit hit;
            Ray rayOrigin = m_Camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(rayOrigin, out hit, 5f, _layerPaper))
            {
                if (hit.collider.gameObject.tag.Equals("HelpPaper"))
                {
                    if(_canOpenHelp)
                    {
                        AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.PAPER);
                        _canOpenHelp = false;
                        OnOpenCanvas?.Invoke();
                    }
                }
                if (hit.collider.gameObject.tag.Equals("DoorMirrorRoom"))
                {
                    print(hit.collider.gameObject.tag);
                    OnOptionsOpen?.Invoke();
                }
            }
        }
        if (m_Input.FindAction("Escape").triggered && m_OpenEffectFinished)
        {
            fadeOut = false;
            _player.enabled = true;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            EnableOptions(false);
            m_OpenEffectFinished = false;
        }

        EfectoCanvas();

    }


    private void EfectoCanvas()
    {
        if (fadeIn)
        {
            if (_canvas.alpha < 1)
            {
                _canvas.alpha += Time.deltaTime;
                if (_canvas.alpha >= 1)
                {
                    fadeIn = false;
                }
            }
        }

        if (fadeOut)
        {
            if (_canvas.alpha >= 0)
            {
                _canvas.alpha -= Time.deltaTime;
                if (_canvas.alpha == 0)
                {
                    fadeOut = false;
                    _canOpenHelp = true;
                }
            }
        }


    }


}
