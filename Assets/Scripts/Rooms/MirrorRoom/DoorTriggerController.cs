using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTriggerController : MonoBehaviour
{
    [SerializeField] private DoorController _roomDoor;
  
    private bool _canOpenHelp = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (_canOpenHelp == true)
                OpenCanvas();
        }
    }

    private void OpenCanvas()
    {
        _canOpenHelp = false;
        //_roomDoor.EnableDoor(true);
    }
}
