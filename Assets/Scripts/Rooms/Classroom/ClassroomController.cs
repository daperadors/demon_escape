using System.Collections;
using System.Linq;
using UnityEngine;

public class ClassroomController : MonoBehaviour
{
    [SerializeField] private GameObject[] m_Riddles;
    [SerializeField] private LaptopFormAnswer[] m_RiddlesList;
    [SerializeField] private GameObject m_Keypad;
    [SerializeField] private GameObject m_FixedKeypad;
    [SerializeField] private MaterialsBoard[] m_MaterialsBoard;
    [SerializeField] private SafeController m_KeypadController;
    [SerializeField] private Animator m_KeypadAnimator;

    private LaptopFormAnswer m_ActualRiddle;
    private GameObject m_ActualRiddleGameObject;
    private int m_CrashCounter = 0;

    public delegate void NewRiddle(LaptopFormAnswer riddle);
    public static NewRiddle newRiddle;
    public LaptopFormAnswer ActualRiddle { get { return m_ActualRiddle; } }
    void Start()
    {
        DisableEnableAllRiddles(false);
        DisableAllMaterials(false);
        GetAndStartRiddle();
        m_Keypad.SetActive(false);
        LaptopController.rightAnswer += RightAnswer;
        InteractWithObjects.putKeypad += PutKeypad;
    }
    private void OnEnable()
    {
        if(m_ActualRiddleGameObject != null) m_ActualRiddleGameObject.SetActive(true);
    }
    private void DisableEnableAllRiddles(bool status)
    {
        foreach (GameObject riddle in m_Riddles)
            riddle.SetActive(status);
    }
    private void DisableAllMaterials(bool status)
    {
        foreach (MaterialsBoard material in m_MaterialsBoard)
        {
            material.material.SetActive(status);
        }
    }
    private void GetAndStartRiddle()
    {
        if (m_Riddles.Length > 0)
        {
            int randomNum = Random.Range(0, m_Riddles.Length);
            print(randomNum);
            GameObject selectedRiddle = m_Riddles[randomNum];
            m_ActualRiddle = m_RiddlesList.Where(r => r.name == selectedRiddle.name).FirstOrDefault();
            if (m_ActualRiddle == null)
            {
                m_CrashCounter++;
                if (m_CrashCounter < 5)
                {
                    GetAndStartRiddle();
                    m_CrashCounter = 0;
                }
            }
            m_ActualRiddleGameObject = m_Riddles.Where(g => g.name == m_ActualRiddle.name).FirstOrDefault();
            print(m_ActualRiddleGameObject);
            newRiddle.Invoke(m_ActualRiddle);

            MaterialsBoard newMaterial = m_MaterialsBoard[Random.Range(0, m_MaterialsBoard.Length)];
            newMaterial.material.SetActive(true);
            m_KeypadController.SetNewCode(newMaterial.value.ToString());
        }
    }
    private void RightAnswer()
    {
        m_Keypad.SetActive(true);
        StartCoroutine(WaitForLooAt());
    }

    private void PutKeypad()
    {
        m_FixedKeypad.SetActive(true);
        m_KeypadAnimator.Play("PutKeypad");
    }
    IEnumerator WaitForLooAt()
    {
        yield return new WaitForSeconds(1f);
        PlayerExtras.setNewLookAt.Invoke(m_Keypad.transform, 4, true);
        PlayerExtras.startLookAtCinematic.Invoke();
    }
}
[System.Serializable]
public class MaterialsBoard
{
    public GameObject material;
    public int value;
}
