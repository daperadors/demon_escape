using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuervosDuros : MonoBehaviour
{
    [SerializeField] private Transform m_InitPoint;
    [SerializeField] private Transform m_FinalPoint;
    [SerializeField] private float m_Speed = 7f;

    public delegate void StartFlyAction();
    public static StartFlyAction onFlyAction;


    private Rigidbody m_Rigidbody;
    private AudioSource m_Audio;
    private SoundsLookAtEnum soundAnimal = SoundsLookAtEnum.CUERVOS;
    private bool m_Finished = false;
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Audio = GetComponent<AudioSource>();

        //onFlyAction += StartFly;

        PlayerExtras.newObjectOrAnimalSound += StartFly;
        //Delegado para que inicien a volar
    }
    void StartFly(SoundsLookAtEnum sound)
    {
        //if (sound != soundAnimal || m_Finished) return;
        StartCoroutine(FlyCoroutine());
    }

    IEnumerator FlyCoroutine()
    {
        gameObject.SetActive(true);
        Vector3 direction = new Vector3(0, 0, -Mathf.Sign(m_FinalPoint.position.z));
        m_Audio.Play();
        while (Unity.Mathematics.math.distancesq(transform.position, m_FinalPoint.position) > 0.1f)
        {
            m_Rigidbody.MovePosition(m_Rigidbody.position + direction * Time.fixedDeltaTime  * m_Speed);
            yield return new WaitForFixedUpdate();
        }

        NewPlayerController.playerFinishFollowTarget?.Invoke();
        gameObject.SetActive(false);
        m_Audio.Stop();
        m_Finished = true;
    }
}
public enum SoundsLookAtEnum
{
    NOTHING,
    CUERVOS,
    CIERVO
}