using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsLookAt : MonoBehaviour
{
    [SerializeField] private Transform m_SoundTransform;
    [SerializeField] private SoundsLookAtEnum m_SoundObject;
    public Transform SoundTransform { get { return m_SoundTransform; } }
    public SoundsLookAtEnum SoundObject { get { return m_SoundObject; } }

    private void Start()
    {
        MenuController.continueGame += () =>
        {
            if (SQLController.Instance.GetStatusRoom("InsideHouse"))
            {
                GetComponent<BoxCollider>().enabled = true;
                PHController.activePikup.Invoke();
            }
        };
    }
}
