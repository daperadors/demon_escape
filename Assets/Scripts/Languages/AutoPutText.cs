using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class AutoPutText : MonoBehaviour
{
    [SerializeField] private string m_TextID;
    [SerializeField] private InputActionReference m_ActionRef;
    private InputAction m_Action;
    private string m_Prefix;

    void Start()
    {
        if(m_ActionRef != null)
            m_Action = m_ActionRef.ToInputAction();
        TranslationController.languageChanged += LanguageChaged;
        StartCoroutine(WaitForPutText());
    }
    IEnumerator WaitForPutText()
    {
        yield return new WaitForSeconds(1f);
        PutText();
    }
    private void LanguageChaged()
    {
        PutText();
    }
    private void PutText()
    {
        if (m_ActionRef != null)
            m_Prefix = '\"'+InputControlPath.ToHumanReadableString(m_Action.bindings[0].effectivePath, InputControlPath.HumanReadableStringOptions.OmitDevice)+'\"'+" ";
        if (m_TextID != "")
        {
            TranslationController.putNewText.Invoke(gameObject.GetComponent<TextMeshProUGUI>(), m_TextID, m_Prefix);
        }
    }
    public void ChengeID(string id)
    {
        m_TextID = id;
        PutText();
    }
}
