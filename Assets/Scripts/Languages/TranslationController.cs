using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class TranslationController : MonoBehaviour
{
    [SerializeField] private Languages m_Languages;
    [SerializeField] private StringScriptable m_ActualLanguage;

    private int m_MaxLength = 13;
    public static TranslationController Instance;
    public StringScriptable ActualLanguage { get { return m_ActualLanguage; } }

    public delegate void PutNewText(TextMeshProUGUI textUI, string idText, string prefix);
    public delegate void NewLanguage(string language);
    public delegate void LanguageChanged();
    public static PutNewText putNewText;
    public static NewLanguage newLanguage;
    public static LanguageChanged languageChanged;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        TranslationController.putNewText += PutText;
        TranslationController.newLanguage += SetNewLanguage;
        SetNewLanguage(m_ActualLanguage.stringValue);
    }
    private void PutText(TextMeshProUGUI textUI, string idText, string prefix)
    {
        string finalText = GetText(idText, ActualLanguage);
        //if (finalText.Length > m_MaxLength) finalText = finalText.Substring(0, 8) + "...";
        textUI.text = prefix +""+ finalText;
    }
    private string GetText(string idText, StringScriptable language)
    {
        return m_Languages.translations.Where(l => l.language == language.stringValue).FirstOrDefault().languageList.Where(t => t.id == idText).FirstOrDefault().text;
    }    
    public string GetText(string idText)
    {
        return m_Languages.translations.Where(l => l.language == m_ActualLanguage.stringValue).FirstOrDefault().languageList.Where(t => t.id == idText).FirstOrDefault().text;
    }
    private void SetNewLanguage(string language)
    {
        m_ActualLanguage.stringValue = language;
        languageChanged.Invoke();
    }
}
