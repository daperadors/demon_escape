using FiniteStateMachine;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class GoToNoise : State
{
    EnemyController m_Enemy;
    Coroutine m_CoroutineInFronOf;

    int destinationNode = 1;
    string m_Animation;


    public GoToNoise(FSM fsm, string animation)
        : base(fsm)
    {
        m_Animation = animation;
    }

    public override void Init()
    {
        base.Init();
        AudioLibrary.enemySound.Invoke(EnemyStatesSounds.RUNING, true);
        m_FSM.Owner.GetComponent<Animator>().applyRootMotion = false;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_Enemy = m_FSM.Owner.GetComponent<EnemyController>();
        m_Enemy.m_EnemyCollider.radius += m_Enemy.WalkingRange;
        m_CoroutineInFronOf = m_Enemy.StartCoroutine(m_Enemy.InFronOfPlayerCoroutine());
        m_Enemy.RecalulatePlayerPath();
        destinationNode = 1;

        //NewPlayerController.playerHide += PlayerAlreadyHide;
    }

    public override void Exit()
    {
        base.Exit();
        m_Enemy.m_EnemyCollider.radius -= m_Enemy.WalkingRange;
        m_Enemy.StopCoroutine(m_CoroutineInFronOf);
        //NewPlayerController.playerHide -= PlayerAlreadyHide;
    }

    public override void Update()
    {
        base.Update();
        m_Enemy.FingerPrintEnemy();
        m_Enemy.CheckIfCollidesFurniture();
        if (m_Enemy.InFrontOfThePlayer())
            m_FSM.ChangeState<FollowState>();
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (!m_Enemy.MovemeEnemyToNode(ref destinationNode, m_Enemy.SpeedRunning))
        {
            Debug.Log("Llega al ruido");
            if (m_Enemy.InFrontOfThePlayer()) m_FSM.ChangeState<FollowState>();
            else m_FSM.ChangeState<IdleState>();
        }
    }
    //private void PlayerAlreadyHide()
    //{
    //    Debug.Log("AAAAAA");
    //    m_FSM.ChangeState<IdleState>();
    //}
}