using System.Collections;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.AI;

public class StunnedState : State
{

    EnemyController m_Enemy;
    Coroutine m_stunnedCorutine;
  
    string m_Animation;
    string m_AnimationSyringe;


    public StunnedState(FSM fsm, string animationStunned, string animationSyringe)
        : base(fsm)
    {
        m_Animation = animationStunned;
        m_AnimationSyringe = animationSyringe;
    }

    public override void Init()
    {
        base.Init();
        EnemyController.stopAudioEnemy?.Invoke();
        m_Enemy = m_FSM.Owner.GetComponent<EnemyController>();
        m_FSM.Owner.GetComponent<Animator>().Play(m_AnimationSyringe);
        m_FSM.Owner.GetComponent<Animator>().applyRootMotion = false;
        m_Enemy.StartCoroutine(StickSyringe());
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void Update()
    {
        base.Update();
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }
   
    private IEnumerator TimeStunned()
    {
        yield return new WaitForSeconds(m_Enemy.StunnedTime);
        m_FSM.ChangeState<IdleState>();
    }    
    private IEnumerator StickSyringe()
    {
        yield return new WaitForSeconds(3f);
        m_stunnedCorutine = m_Enemy.StartCoroutine(TimeStunned());

    }
}
