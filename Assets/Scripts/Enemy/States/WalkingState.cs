using FiniteStateMachine;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class WalkingState : State
{
    EnemyController m_Enemy;
    Coroutine m_CoroutineInFronOf;
    Coroutine m_CoroutineCheckPlayer;

    int destinationNode = 1;
    string m_Animation;

    public WalkingState(FSM fsm, string animation)
        : base(fsm)
    {
        m_Animation = animation;
    }

    public override void Init()
    {
        base.Init();
        AudioLibrary.enemySound.Invoke(EnemyStatesSounds.WALKING, true);
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_FSM.Owner.GetComponent<Animator>().applyRootMotion = false;
        m_Enemy = m_FSM.Owner.GetComponent<EnemyController>();
        m_Enemy.m_EnemyCollider.radius += m_Enemy.WalkingRange;

        m_CoroutineCheckPlayer = m_Enemy.StartCoroutine(m_Enemy.ListeningPlayer<IdleState>(
            new int[] { Random.Range(40, 60), Random.Range(40, 60) },
            Random.Range(4, 6))
            );
        m_CoroutineInFronOf = m_Enemy.StartCoroutine(m_Enemy.InFronOfPlayerCoroutine());

        destinationNode = 1;
        m_Enemy.RecalculatePath();
    }

    public override void Exit()
    {
        base.Exit();
        m_Enemy.m_EnemyCollider.radius -= m_Enemy.WalkingRange;

        m_Enemy.StopCoroutine(m_CoroutineCheckPlayer);
        m_Enemy.StopCoroutine(m_CoroutineInFronOf);
    }

    public override void Update()
    {
        base.Update();
        //if (m_Enemy.InFrontOfThePlayer()) m_FSM.ChangeState<FollowState>();
        m_Enemy.CheckIfCollidesFurniture();
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (!m_Enemy.MovemeEnemyToNode(ref destinationNode, m_Enemy.Speed)) m_Enemy.RecalculatePath();
    }
    public override void OnTriggerStay(Collider player)
    {
        if (player.CompareTag("Player"))
        {
            m_Enemy.LastPlayerPosition = player.gameObject.transform.position;
            m_FSM.ChangeState<GoToNoise>();

        }
    }
}