using FiniteStateMachine;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class CinematicState : State
{

    string m_Animation;
    EnemyController m_Enemy;
    private Coroutine _entranceCinematic;


    public CinematicState(FSM fsm, string animation)
         : base(fsm)
    {
        m_Animation = animation;
    }


    public override void Init()
    {
        base.Init();
        EnemyController.onEnemyIntoCinematicAction += ExecuteEntranceCoroutine;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_FSM.Owner.GetComponent<Animator>().applyRootMotion = false;
        m_Enemy = m_FSM.Owner.GetComponent<EnemyController>();
        m_Enemy.transform.position = m_Enemy.initialPoint.position;
        m_Enemy.transform.rotation = Quaternion.LookRotation(m_Enemy.finishPoint.position - m_Enemy.transform.position);
    }

    private void ExecuteEntranceCoroutine()
    {
        _entranceCinematic = m_Enemy.StartCoroutine(m_Enemy.RunEntranceAnimation(m_Enemy.finishPoint.transform));
    }

    public override void Exit()
    {
        base.Exit();
        m_Enemy.StopCoroutine(_entranceCinematic);
    }

    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);

        if(collider.gameObject.CompareTag("HalfAnimationPath"))
        {
            NewPlayerController.playerFinishFollowTarget?.Invoke();
        }
    }

}
