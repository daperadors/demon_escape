using FiniteStateMachine;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using static EnemyController;

public class FollowState : State
{
    EnemyController m_Enemy;
    Rigidbody m_Rigidbody;
    NavMeshPath m_Path;
    Coroutine m_CoroutineCheckPlayer;
    string m_Animation;
    string m_SlowAnimation;
    string m_FinalAttackAnimation;
    private bool _inLowSpeed = false;
    bool m_PathPending = false;
    private float _lowSpeed = 0.2f;
    float m_NodeDistance = 2f;
    int destinationNode = 1;
    int m_Collapse = 0;

    public FollowState(FSM fsm, string animation, string slowAnimation, string finalAttackAnimation)
        : base(fsm)
    {
        m_Animation = animation;
        m_SlowAnimation = slowAnimation;
        m_FinalAttackAnimation = finalAttackAnimation;
    }

    public override void Init()
    {
        base.Init();
        AudioLibrary.enemySound.Invoke(EnemyStatesSounds.RUNING, true);
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_FSM.Owner.GetComponent<Animator>().applyRootMotion = false;
        m_Rigidbody = m_FSM.Owner.GetComponent<Rigidbody>();
        m_Enemy = m_FSM.Owner.GetComponent<EnemyController>();
        m_Path = new NavMeshPath();
        RecalculatePath(m_Enemy.Player.transform.position);
        m_CoroutineCheckPlayer = m_Enemy.StartCoroutine(CheckPlayerVisibility());
        m_PathPending = false;
        NewPlayerController.onStunEnemy += IntroInStunnedState;
        NewPlayerController.onSetEnemyVelocity += SetSpeed;
        NewPlayerController.playerHide += PlayerAlreadyHide;
        NewPlayerController.playerDead += PlayerDead;
        NewPlayerController.playerEnterRoom += PlayerAlreadyHide;
    }

    public override void Exit()
    {
        base.Exit();
        NewPlayerController.onStunEnemy -= IntroInStunnedState;
        NewPlayerController.onSetEnemyVelocity -= SetSpeed;
        NewPlayerController.playerHide -= PlayerAlreadyHide;
        NewPlayerController.playerEnterRoom -= PlayerAlreadyHide;

        _inLowSpeed = false;
        m_Enemy.StopCoroutine(m_CoroutineCheckPlayer);
    }
    public override void Update()
    {
        base.Update();
        m_Enemy.CheckIfCollidesFurniture();
        m_Enemy.FingerPrintEnemy();
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        SetDestination(m_Enemy.Player.transform.position);
    }

    private void SetDestination(Vector3 target)
    {
        if (m_Path.corners.Length > destinationNode && !m_Enemy.CalculateDistanceAgent(m_Path.corners[destinationNode], m_NodeDistance))
        {
            Debug.DrawLine(m_Path.corners[destinationNode], m_Path.corners[destinationNode] + Vector3.up, Color.red, 10f);
            Vector3 direction = m_Path.corners[destinationNode] - m_Enemy.transform.position;
            direction.Normalize();
            Vector3 lookAt = Vector3.ProjectOnPlane(direction, Vector3.up);
            m_Rigidbody.MovePosition(m_Enemy.transform.position + direction * (_inLowSpeed ? _lowSpeed : m_Enemy.SpeedRunning) * Time.fixedDeltaTime);
            Quaternion rotation = Quaternion.LookRotation(lookAt);
            m_Enemy.transform.rotation = Quaternion.Lerp(m_Enemy.transform.rotation, rotation, Time.fixedDeltaTime * m_Enemy.RotateSpeed);
            m_PathPending = true;
            return;
        }
        m_PathPending = false;
        RecalculatePath(target);
        destinationNode = m_Path.corners.Length > 2 ? 2 : 1;
        return;
    }
    private void RecalculatePath(Vector3 target)
    {

        Vector3 finalPosition = target;

        m_Path = new NavMeshPath();
        if (NavMesh.CalculatePath(m_Enemy.transform.position, finalPosition, NavMesh.AllAreas, m_Path))
        {
            //Debug.Log("Valid path has been found");
            Debug.DrawLine(m_Path.corners[0], m_Path.corners[1], Color.cyan);
            for (int i = 0; i < m_Path.corners.Length - 1; i++)
                Debug.DrawLine(m_Path.corners[i], m_Path.corners[i + 1], Color.blue, 40f);
            m_Collapse = 0;
        }
        else
        {
            Debug.Log("No path to that position, picking a new point");
            teleportEnemy?.Invoke();
            RecalculatePath(target);
            if(m_Collapse >= 10)
            {
                //guardar partida y exit || cambiar estado.
                Debug.Log("CRASH");
                m_FSM.ChangeState<IdleState>();
            }
        }
    }
    IEnumerator CheckPlayerVisibility()
    {
        while (true)
        {
            yield return new WaitForSeconds(4f);
            if (!m_Enemy.InFrontOfThePlayer()) m_FSM.ChangeState<IdleState>();
        }
    }
    private void IntroInStunnedState()
    {
        m_FSM.ChangeState<StunnedState>();
    }

    private void SetSpeed(string animation, bool status)
    {
        EnemyController.stopAudioEnemy?.Invoke();
        m_FSM.Owner.GetComponent<Animator>().Play(animation);
        _inLowSpeed = status;
    }
    private void PlayerAlreadyHide()
    {
        m_FSM.ChangeState<IdleState>();
    
    }
    private void PlayerDead()
    {
        AudioLibrary.enemySound.Invoke(EnemyStatesSounds.ATTACK, false);
        m_FSM.Owner.GetComponent<Animator>().Play(m_FinalAttackAnimation);
    }

}