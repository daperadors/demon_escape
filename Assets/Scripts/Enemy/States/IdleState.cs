using FiniteStateMachine;
using UnityEngine;

public class IdleState : State
{
    EnemyController m_Enemy;
    Coroutine m_CoroutineListenPlayer;
    Coroutine m_CoroutineInFronOf;

    string m_Animation;

    public IdleState(FSM fsm, string animation)
        : base(fsm)
    {
        m_Animation = animation;
    }

    public override void Init()
    {
        base.Init();
        AudioLibrary.enemySound.Invoke(EnemyStatesSounds.IDLE, true);
        m_FSM.Owner.GetComponent<Animator>().applyRootMotion = false;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_Enemy = m_FSM.Owner.GetComponent<EnemyController>();
        m_Enemy.m_EnemyCollider.radius += m_Enemy.ListenRange;
        m_CoroutineListenPlayer = m_Enemy.StartCoroutine(m_Enemy.ListeningPlayer<WalkingState>(
            new int[] { Random.Range(7, 13), Random.Range(7, 13) },
            Random.Range(2, 4))
            );
        //m_CoroutineInFronOf = m_Enemy.StartCoroutine(m_Enemy.InFronOfPlayerCoroutine());
    }

    public override void Exit()
    {
        base.Exit();
        m_Enemy.StopCoroutine(m_CoroutineListenPlayer);
        m_Enemy.m_EnemyCollider.radius -= m_Enemy.ListenRange;
        //m_Enemy.StopCoroutine(m_CoroutineInFronOf);
    }
    public override void Update()
    {
        base.Update();
        m_Enemy.CheckIfCollidesFurniture();
        if (m_Enemy.InFrontOfThePlayer()) m_FSM.ChangeState<FollowState>();
    }

    public override void OnTriggerStay(Collider player)
    {
        if (player.CompareTag("Player"))
        {
            if (!m_Enemy.InFrontOfThePlayer())
            {
                m_Enemy.LastPlayerPosition = player.gameObject.transform.position;
                m_FSM.ChangeState<GoToNoise>();
            }
        }
    }
}