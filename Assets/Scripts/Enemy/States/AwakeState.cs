using FiniteStateMachine;
using UnityEngine;

public class AwakeState : State
{
    public AwakeState(FSM fsm)
         : base(fsm)
    {

    }

    public override void Init()
    {
        base.Init();
    }

    public override void Exit()
    {
        base.Exit();

    }

}
