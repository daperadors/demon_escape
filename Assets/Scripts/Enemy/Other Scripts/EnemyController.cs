using FiniteStateMachine;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
public class EnemyController : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private Transform[] m_Waypoints;
    [SerializeField] private NewPlayerController m_Player;
    [SerializeField] private LayerMask m_LayerFurnitures;
    [SerializeField] private LayerMask m_LayerPlayer;
    [SerializeField] private float m_RoteteSpeed = 1.5f;
    [SerializeField] private float m_Speed = 2.5f;
    [SerializeField] private float m_SpeedRunning = 3f;
    [SerializeField] private float m_DistanceCollides = 1f;
    [SerializeField] private float m_LaunchForce = 4f;
    [SerializeField] private float m_NodeDistance = .25f;
    [SerializeField] private float m_StunnedTime = 10f;
    [SerializeField] private LayerMask m_LayerVisionAngle;
    [SerializeField] public SphereCollider m_EnemyCollider;
    public NewPlayerController Player { get { return m_Player; } }
    private Rigidbody m_RigidBody;
    private FSM m_FSM;
    public FSM fms { get { return m_FSM; } }
    public Rigidbody rigidbody { get { return m_RigidBody; } }
    private NavMeshPath m_Path;
    private Vector3 m_NewWaypoint;
    public Vector3 NewWaypoit { get { return m_NewWaypoint; } set { m_NewWaypoint = value; } }
    private float walkRadius = 2000;
    public NavMeshPath Path { get { return m_Path; } }
    public LayerMask LayerVisionAngle { get { return m_LayerVisionAngle; } }
    public float Speed { get { return m_Speed; } }
    public float SpeedRunning { get { return m_SpeedRunning; } }
    public float RotateSpeed { get { return m_RoteteSpeed; } }
    public float StunnedTime { get { return m_StunnedTime; } }


    //Door Delegate
    public delegate void onDoorCollision(bool enable);
    public static onDoorCollision onDoorCheckCollision;

    public delegate void EnableEnemy();
    public static EnableEnemy onEnableEnemy;

    public delegate void EnemyIntoCinematicAction();
    public static EnemyIntoCinematicAction onEnemyIntoCinematicAction;

    public delegate void SetCinematicTargets(Transform initial, Transform final);
    public static SetCinematicTargets onSetCinematicTargets;

    public delegate void TeleportEnemy();
    public static TeleportEnemy teleportEnemy;  
    
    public delegate void StopEnemySounds();
    public static StopEnemySounds stopAudioEnemy;



    [Space(20)]
    [Header("Listen State")]
    [SerializeField] private int m_ListenCounter = 4;
    [SerializeField, Range(1f, 15f)] private float m_ListenRange = 4;
    public int ListenCounter { get { return m_ListenCounter; } }
    public float ListenRange { get { return m_ListenRange; } }


    [Space(20)]
    [Header("Walking State")]
    [SerializeField, Range(1f, 3f)] private float m_WalkingRange = 2;
    [SerializeField] private int m_PlayerNotFoundCounter = 5;
    public int PlayerNotFoundCounter { get { return m_PlayerNotFoundCounter; } }
    public float WalkingRange { get { return m_WalkingRange; } }

    public Vector3 LastPlayerPosition = Vector3.zero;

    [Header("Sounds")]
    [SerializeField] private AudioLibrary _audioLibrary;
    [SerializeField] private AudioSource _audioSource;

    [Space(20)]
    [Header("Player states config")]
    [SerializeField, Range(1f, 30f)] private int m_SphereRadiusIdle = 3;
    [SerializeField, Range(1f, 30f)] private int m_SphereRadiusWalking = 15;
    [SerializeField, Range(1f, 30f)] private int m_SphereRadiusRunning = 25;
    [SerializeField, Range(1f, 30f)] private int m_SphereRadiusShift = 4;
    [SerializeField, Range(1f, 30f)] private float m_DistanceForFollowState = 10f;
    public bool PlayerIsNear = false;
    public int SphereRadiusIdle { get { return m_SphereRadiusIdle; } }
    public int SphereRadiusWalking { get { return m_SphereRadiusWalking; } }
    public int SphereRadiusRunning { get { return m_SphereRadiusRunning; } }
    public int SphereRadiusShift { get { return m_SphereRadiusShift; } }
    public float DistanceForFollowState { get { return m_DistanceForFollowState; } }

    private bool _stunned;


    //Cinematic Points
    [SerializeField] private Transform[] _entranceCinematicPoints;
    [SerializeField] private Transform[] _finalCinematicPoints;

    public Transform[] entranceCinematicPoints { get { return _entranceCinematicPoints; } }
    public Transform[] finalCinematicPoints { get { return _finalCinematicPoints; } }

    [SerializeField] private Transform _initialPoint;
    public Transform initialPoint { get { return _initialPoint; } }

    [SerializeField] private Transform _finishPoint;
    public Transform finishPoint { get { return _finishPoint; } }

    [SerializeField] private Transform _spawnPoint;



    [Header("Entrance cinematic values")]
    private bool _arriveTargetCinematic = false;


    private void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody>();
        m_Path = new NavMeshPath();
        m_FSM = new FSM(gameObject);
        m_FSM.AddState(new IdleState(m_FSM, "Listen"));
        m_FSM.AddState(new WalkingState(m_FSM, "Walk"));
        m_FSM.AddState(new GoToNoise(m_FSM, "Run"));
        m_FSM.AddState(new FollowState(m_FSM, "Run", "RunSlow", "FinalAttack"));
        m_FSM.AddState(new StunnedState(m_FSM, "Stunned", "StickSyringe"));
        m_FSM.AddState(new CinematicState(m_FSM, "Listen"));
        m_FSM.AddState(new AwakeState(m_FSM));

        m_FSM.ChangeState<AwakeState>();
        NewPlayerController.insideHouse += ContinueAndPlayerInsideHouse;
        SpiritBox.onEnemySignal += EmitSignal;
        onSetCinematicTargets += SetCinematicPoints;
    }

    private void Update()
    {
        m_FSM.Update();
    }

    private void FixedUpdate()
    {
        m_FSM.FixedUpdate();
    }

    public Vector3 GetRandomPoint()
    {
        return m_Waypoints[Random.Range(0, m_Waypoints.Length - 1)].position;
    }
    public bool CalculateDistanceAgent(Vector3 position, float distance)
    {
        if (Unity.Mathematics.math.distancesq(transform.position, position) <= 0.09f)
        {
            return true;
        }
        return false;
    }
    //public bool IsNearPlayer(float distance)
    //{
    //    if (Unity.Mathematics.math.distancesq(transform.position, m_Player.transform.position) <= distance)
    //    {
    //        PlayerIsNear = true;
    //        return true;
    //    }
    //    return false;
    //}
    public bool MovemeEnemyToNode(ref int destinationNode, float speed)
    {
        if (destinationNode < m_Path.corners.Length)
        {
            if (!CalculateDistanceAgent(m_Path.corners[destinationNode], m_NodeDistance))
            {
                Debug.DrawLine(m_Path.corners[destinationNode], m_Path.corners[destinationNode] + Vector3.up, Color.red, 10f);
                Vector3 direction = m_Path.corners[destinationNode] - transform.position;
                direction.Normalize();
                Vector3 lookAt = Vector3.ProjectOnPlane(direction, Vector3.up);

                m_RigidBody.MovePosition(transform.position + direction * speed * Time.fixedDeltaTime);
                Quaternion rotation = Quaternion.LookRotation(lookAt);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.fixedDeltaTime * m_RoteteSpeed);
            }
            else
            {
                destinationNode++;
                //if (InFrontOfThePlayer())
                //    m_FSM.ChangeState<FollowState>();
            }
            return true;

        }
        destinationNode = 1;
        return false;

    }
    public void RecalculatePath()
    {

        Vector3 finalPosition = GetRandomPoint();

        m_Path.ClearCorners();
        if (NavMesh.CalculatePath(transform.position, finalPosition, NavMesh.AllAreas, m_Path))
        {
            Debug.Log("Valid path has been found");
            Debug.DrawLine(m_Path.corners[0], m_Path.corners[1], Color.cyan);
            for (int i = 0; i < m_Path.corners.Length - 1; i++)
                Debug.DrawLine(m_Path.corners[i], m_Path.corners[i + 1], Color.blue, 40f);
            m_NewWaypoint = finalPosition;
        }
        else
        {
            Debug.Log("No path to that position, picking a new point");
            teleportEnemy?.Invoke();
            //RecalculatePath();
        }
    }
    public void RecalulatePlayerPath()
    {


        m_Path = new NavMeshPath();
        if (NavMesh.CalculatePath(transform.position, LastPlayerPosition, NavMesh.AllAreas, m_Path))
        {
            Debug.DrawLine(m_Path.corners[0], m_Path.corners[1], Color.cyan);
            for (int i = 0; i < m_Path.corners.Length - 1; i++)
                Debug.DrawLine(m_Path.corners[i], m_Path.corners[i + 1], Color.blue, 40f);
        }
        else
        {
            Debug.Log("No path to that position, picking a new point");
            teleportEnemy?.Invoke();
            //RecalulatePlayerPath();
        }
    }
    private Vector3 GetSampleRandomPoint()
    {
        Vector3 randomDirection = Random.insideUnitSphere * walkRadius;
        randomDirection += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, walkRadius, 1);
        return hit.position;
    }
    public void CheckIfCollidesFurniture()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, m_DistanceCollides, m_LayerFurnitures))
        {
            Debug.DrawLine(transform.position, transform.forward, Color.red, 4f);
            if (hit.collider.gameObject.CompareTag("Furniture"))
            {
                Rigidbody rigidbody = null;
                if (hit.collider.gameObject.TryGetComponent<Rigidbody>(out rigidbody))
                {
                    Vector3 normal = -hit.normal.normalized;
                    Vector3 normalPlus4 = new Vector3(-hit.normal.normalized.x + 4, -hit.normal.normalized.y + 4, -hit.normal.normalized.z + 4);
                    Vector3 direction = new Vector3(
                        Random.Range(normal.x, normalPlus4.x),
                        Random.Range(normal.y, normalPlus4.y),
                        Random.Range(normal.z, normalPlus4.z)
                        );
                    rigidbody.AddForce(direction * m_LaunchForce, ForceMode.Impulse);
                }
            }
        }
    }


    public IEnumerator ListeningPlayer<NotFound>(int[] randomRange, int interationsPlayer) where NotFound : State
    {
        int playerNotFoundIteration = interationsPlayer;

        while (playerNotFoundIteration >= 0)
        {
            yield return new WaitForSeconds(Random.Range(randomRange[0], randomRange[1]));
            playerNotFoundIteration--;
        }
        if(!InFrontOfThePlayer())
            m_FSM.ChangeState<NotFound>();
    }
    public void OnPlayerStay(Collider player)
    {
        if(!m_Player.Hide && CheckHeight(player.transform.position) && PlayerInsideHouse() && NewPlayerController.InsideEscapeRoom)
            m_FSM.OnTriggerStay(player);
    }
    private bool CheckHeight(Vector3 playerPosition)
    {
        float resultHeight = (playerPosition.y > transform.position.y ? playerPosition.y - transform.position.y : transform.position.y - playerPosition.y);
        bool sameHeight = resultHeight < 1f;
        return sameHeight;

    }

    private void OnTriggerEnter(Collider other)
    {
        m_FSM.OnTriggerEnter(other);
        if (other.CompareTag("FurnitureDetection") || other.CompareTag("TableDetection"))
        {
            other.gameObject.GetComponent<ObstacleDetections>().InvokeObstacleParent(other.gameObject.tag);
        }
    }

    public bool InFrontOfThePlayer()
    {
        if (m_Player.Hide || !PlayerInsideHouse() || !NewPlayerController.InsideEscapeRoom) return false;

        Vector3 direction = Vector3.Normalize(m_Player.transform.position - transform.position);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, Mathf.Infinity, LayerVisionAngle))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                Debug.DrawLine(transform.position, m_Player.transform.position, Color.white, 5f);
                if (Vector3.Dot(transform.forward, direction) > 0)
                {
                    Debug.Log("infront");
                    return true;
                }
            }
            //Debug.Log("noinfront");
            return false;
        }
        return false;
    }
    
    public IEnumerator InFronOfPlayerCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            if (InFrontOfThePlayer() && PlayerInsideHouse()) m_FSM.ChangeState<FollowState>();
        }
    }

    public bool EmitSoundDistance()
    {
        if (Unity.Mathematics.math.distancesq(transform.position, m_Player.transform.position) <= 500f)
        {
            return true;
        }
        return false;
    }

    private void EmitSignal()
    {
        if(EmitSoundDistance())
        {
            
        }
        else
        {
            
        }
    }


    private bool ThrowEnemyRaycast(string tag)
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, 1f))
        {
            if (hit.collider.gameObject.CompareTag(tag))
            {
                return true;
            }
        }
        return false;
    }
    public bool PlayerInsideHouse()
    {
        return m_Player.PlayerInsideHouse;
    }
    public void FingerPrintEnemy()
    {
        if(ThrowEnemyRaycast("Door"))
        {
            onDoorCheckCollision?.Invoke(true);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (m_FSM.GetState().GetType().ToString() != "CinematicState" && m_FSM.GetState().GetType().ToString() != "StunnedState")
            {
                collision.gameObject.GetComponent<NewPlayerController>().FSM.ChangeState<DeadState>();
            }
        }
    }

    public IEnumerator RunEntranceAnimation(Transform target)
    {
        float distance;

        gameObject.GetComponent<Animator>().Play("Run");

        while (true)
        {
            transform.rotation = Quaternion.LookRotation(target.position - transform.position);
            Vector3 newPos = Vector3.MoveTowards(transform.position, target.position, 4 * Time.deltaTime);
            m_RigidBody.MovePosition(newPos);

            distance = Unity.Mathematics.math.distancesq(transform.position, target.position);

            if(distance < 1)
            {
                _arriveTargetCinematic = true;
                yield return new WaitForSeconds(3);
                transform.position = _spawnPoint.position;
                transform.rotation = new Quaternion(0, 0, 0, 0);
                m_FSM.ChangeState<IdleState>();
                break;
            }

            yield return new WaitForFixedUpdate();
        }
    }
    private void ContinueAndPlayerInsideHouse()
    {
        m_FSM.ChangeState<IdleState>();
    }

    private void SetCinematicPoints(Transform initial, Transform final)
    {
        _initialPoint = initial;
        _finishPoint = final;
    }
}
