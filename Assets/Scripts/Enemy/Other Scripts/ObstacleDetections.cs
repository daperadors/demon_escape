using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDetections : MonoBehaviour
{

    [SerializeField] private ObstacleController _parent;

    public void InvokeObstacleParent(string tag)
    {
        if(tag.Equals("FurnitureDetection"))
        {
            _parent.EnableObstacleMovement();
        }

        if(tag.Equals("TableDetection"))
        {
            _parent.TablesDown();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _parent._mover = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _parent._mover = true;
        }
    }


}
