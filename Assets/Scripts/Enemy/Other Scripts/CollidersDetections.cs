using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollidersDetections : MonoBehaviour
{
    private EnemyController m_Enemy;

    private void Awake()
    {
        m_Enemy = GetComponentInParent<EnemyController>();
    }

    public void OnTriggerStay(Collider other)
    {
        m_Enemy.OnPlayerStay(other);
    }
}
