using UnityEngine;

[CreateAssetMenu(fileName = "GameEventInteger", menuName = "GameEvent /GameEvent - Integer")]
public class GameEventInteger : GameEvent<int> { }

