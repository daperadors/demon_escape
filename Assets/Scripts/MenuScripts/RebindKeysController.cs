using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class RebindKeysController : MonoBehaviour
{
    [SerializeField] private KeyRebindingStats m_Keys;
    [SerializeField] private TextMeshProUGUI m_StatsText;
    private string m_ActualKey;
    private InputAction m_Action;
    private void Start()
    {
        m_Action = m_Keys.Action.ToInputAction();
        ControlSetter();
        m_StatsText.gameObject.SetActive(false);

        MenuController.getActualControls += ControlSetter;
        //else
        //{
        //    for (int i = 0; i < m_Action.bindings.Count; i++)
        //    {
        //        print(InputControlPath.ToHumanReadableString(m_Action.bindings[i].effectivePath, InputControlPath.HumanReadableStringOptions.OmitDevice));
        //    }
        //}
    }
    private void ControlSetter()
    {
        if (m_Action.bindings[0].effectivePath != "2DVector")
        {
            string actualKey = m_Action.bindings[0].effectivePath.Substring(11).ToUpper();
            m_ActualKey = actualKey;
            m_Keys.Text.text = CheckTextLength(InputControlPath.ToHumanReadableString(m_Action.bindings[0].effectivePath, InputControlPath.HumanReadableStringOptions.OmitDevice));
        }
    }
    public void KeyBinding()
    {
        string controlBeforeChange = m_Action.bindings[0].effectivePath;

        m_Keys.Text.text = "...";

        m_Action.Disable();
        m_Action.PerformInteractiveRebinding()
            .WithControlsExcluding("Mouse")
            //.WithCancelingThrough("<Mouse>/delta")
            .OnMatchWaitForAnother(0.1f)
            .OnComplete(operation =>
            {
                string newKey = m_Keys.Text.text = InputControlPath.ToHumanReadableString(m_Action.bindings[0].effectivePath, InputControlPath.HumanReadableStringOptions.OmitDevice);

                QueryResult queryResult = SQLController.Instance.CheckIfControlIsDefault(m_Action.bindings[0].effectivePath, m_Action.bindings[0].action);
                if (queryResult == QueryResult.DEFAULT)
                {
                    SQLController.Instance.DeleteControl(m_Action.bindings[0].action);
                    print("This is your default key");
                    m_ActualKey = InputControlPath.ToHumanReadableString(operation.action.bindings[0].effectivePath, InputControlPath.HumanReadableStringOptions.OmitDevice);
                    SetOldBinding(operation.action.bindings[0].effectivePath, m_ActualKey);
                    StartCoroutine(WaitForDissolveText("key_changed"));
                }
                else if (queryResult == QueryResult.EXISTS
                    || newKey.ToLower() == "W"
                    || newKey.ToLower() == "A"
                    || newKey.ToLower() == "S"
                    || newKey.ToLower() == "D")
                {
                    SetOldBinding(controlBeforeChange, m_ActualKey);
                    print("This key already exists");
                    StartCoroutine(WaitForDissolveText("key_in_use"));
                }
                else if (queryResult == QueryResult.NOT_EXISTS)
                {
                    SQLController.Instance.SaveControls(controlBeforeChange, operation.action.bindings[0].effectivePath, operation.action.bindings[0].action, false);
                    print("Setting new key...");
                    SetOldBinding(operation.action.bindings[0].effectivePath, newKey);
                    StartCoroutine(WaitForDissolveText("key_changed"));
                }
                operation.Dispose();
                m_Action.Enable();
                TranslationController.languageChanged.Invoke();
            })
            .OnCancel(operation =>
            {
                m_Action.Enable();
                operation.Dispose();
                m_Keys.Text.text = CheckTextLength(m_ActualKey);
                StartCoroutine(WaitForDissolveText("key_cancel"));
            }).Start();
    }
    public void ResetDefaultKey()
    {
        m_Action.Disable();
        m_Action.Reset();
        m_Action.Enable();
    }
    private void SetOldBinding(string controlBeforeChange, string oldKey)
    {
        m_Action.ChangeBinding(0)
        .WithPath(controlBeforeChange);
        m_Action.ApplyBindingOverride(controlBeforeChange);
        m_Keys.Text.text = CheckTextLength(oldKey);
    }
    private string CheckTextLength(string text)
    {
        if(text.Length > 10)
        {
            text = text.Substring(0, text.Length - 3);
            text += "...";
        }
        return text;
    }

    IEnumerator WaitForDissolveText(string textID)
    {
        m_StatsText.gameObject.SetActive(true);
        m_StatsText.gameObject.GetComponent<AutoPutText>().ChengeID(textID);
        yield return new WaitForSeconds(1.5f);
        m_StatsText.gameObject.SetActive(false);
    }
}