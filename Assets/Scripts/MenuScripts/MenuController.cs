using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Camera m_MenuCamera;
    [SerializeField] private MenuContainers[] m_Containers;
    [SerializeField] private GraphicsText[] m_GraphicsText;
    [SerializeField] private GraphicsList m_Graphics;
    [SerializeField] private AudioMixer m_AudioMixer;
    [SerializeField] private Slider m_SliderAudio;
    [SerializeField] private Button m_ContinueGameButton;
    private List<MenuContainers> m_PreviousMenu;
    private Menu m_ActualMenu;
    private bool m_GameStarted;
    private bool m_MenuEnabled;

    public delegate void StartGame(bool status);
    public delegate void GetActualControls();
    public delegate void ContinueGame();
    public delegate void SavePositions();
    public delegate void NewGame();
    public static StartGame startGame;
    public static ContinueGame continueGame;
    public static SavePositions savePositions;
    public static GetActualControls getActualControls;
    public static NewGame startNewGame;
    void Start()
    {
        m_PreviousMenu = new List<MenuContainers>();
        m_MenuCamera.enabled = true;
        m_MenuEnabled = true;
        m_GameStarted = false;
        SQLController.Instance.SaveStatusRoom("GameSaved", 0);
        DisableOrEnableAllMenus(false);
        EnableSingleMenu(Menu.HOME);
        SaveUserRebinds();
        StartCoroutine(WaitForInitSettings());
    }
    private void Update()
    {
        if (!m_GameStarted) return;
        if (NewPlayerController.InputM.FindAction("PauseMenu").triggered)
        {
            if (m_MenuEnabled)
            {
                ExitPauseMenu();
            }
            else
            {
                if (NewPlayerController.Instance.FSM.GetState().GetType().ToString() != "AtackPlayerState")
                {
                    Time.timeScale = 0;
                    AudioLibrary.stopPlayAllSounds?.Invoke(true);
                    EnableOrDisableCamera(true);
                    GameManager.Instance.DisableOrEnableOtherObjects(false);
                    DisableOrEnableAllMenus(false);
                    EnableSingleMenu(Menu.PAUSE);
                    CheckContinueGame();
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    NewPlayerController.stopAudio?.Invoke();
                }
            }
            m_MenuEnabled = !m_MenuEnabled;

        }
    }
    void DisableOrEnableAllMenus(bool status)
    {
        foreach (MenuContainers menu in m_Containers)
        {
            menu.gameObject.SetActive(status);
        }
    }
    void EnableSingleMenu(Menu menu)
    {
        m_Containers.Where(c => c.name == menu).FirstOrDefault().gameObject.SetActive(true);
        if (menu == Menu.CONTROLS) getActualControls?.Invoke();
    }
    private void GetActiveMenu()
    {
        foreach (MenuContainers menu in m_Containers)
        {
            if (menu.gameObject.activeInHierarchy)
            {
                m_PreviousMenu.Add(m_Containers.Where(c => c.name == menu.name).FirstOrDefault());
                return;
            }
        }
    }
    private void SetAntiAliasing(GraphicsStats graphic)
    {
        QualitySettings.antiAliasing = SetBoolQuality(graphic) ? 1 : 0;
    }
    private void SetFullScreen(GraphicsStats graphic)
    {
        Screen.fullScreen = SetBoolQuality(graphic);
    }
    private void SetQuality(GraphicsStats graphic, bool status)
    {
        if (status)
        {
            if (graphic.statusGraphSelected + 1 > graphic.statusText.Length - 1) graphic.statusGraphSelected = 0;
            else graphic.statusGraphSelected += 1;
        }
        else
        {
            if (graphic.statusGraphSelected - 1 < 0) graphic.statusGraphSelected = graphic.statusText.Length - 1;
            else graphic.statusGraphSelected -= 1;
        }
        SetQualityText(graphic);
        QualitySettings.SetQualityLevel(graphic.statusGraphSelected);
    }
    private void SetQualityText(GraphicsStats graphic)
    {
        AutoPutText putText = m_GraphicsText.Where(t => t.name == graphic.type).FirstOrDefault().text.gameObject.GetComponent<AutoPutText>();
        putText.ChengeID(graphic.statusText[graphic.statusGraphSelected]);
    }
    private bool SetBoolQuality(GraphicsStats graphic)
    {
        AutoPutText putText = m_GraphicsText.Where(t => t.name == graphic.type).FirstOrDefault().text.gameObject.GetComponent<AutoPutText>();
        if (graphic.status)
        {
            graphic.status = !graphic.status;
            putText.ChengeID("off");
        }
        else
        {
            graphic.status = !graphic.status;
            putText.ChengeID("on");
        }
        return graphic.status;
    }
    private void InitSettings()
    {
        InitSettingsQuality();
        LoadUserRebinds();
        LoadVolume();
        CheckContinueGame();
    }
    void CheckContinueGame()
    {
        if (!CheckIfTransformsFileExists()) m_ContinueGameButton.interactable = false;
        else m_ContinueGameButton.interactable = true;
    }
    bool CheckIfTransformsFileExists()
    {
        return SQLController.Instance.GetIfRoomIsStarted("GameSaved");
    }
    private void InitSettingsQuality()
    {
        foreach (GraphicsStats graphic in m_Graphics.grafics)
        {
            if (graphic.type == "antialiasing")
            {
                QualitySettingsDB quality = SQLController.Instance.GetSettings(graphic.type);
                if (quality == null) return;
                graphic.status = quality.value == 1 ? true : false;
                graphic.status = !graphic.status;
                SetAntiAliasing(graphic);
            }
            if (graphic.type == "fullscreen")
            {
                QualitySettingsDB quality = SQLController.Instance.GetSettings(graphic.type);
                if (quality == null) return;
                graphic.status = quality.value == 1 ? true : false;
                graphic.status = !graphic.status;
                SetFullScreen(graphic);
            }
            if (graphic.type == "quality")
            {
                QualitySettingsDB quality = SQLController.Instance.GetSettings(graphic.type);
                if (quality == null) return;
                graphic.statusGraphSelected = quality.value;
                QualitySettings.SetQualityLevel(graphic.statusGraphSelected);
                SetQualityText(graphic);
            }
        }
    }
    private void LoadVolume()
    {
        VolumeSettings volume = SQLController.Instance.GetVolume("General");
        if (volume != null)
        {
            m_SliderAudio.value = float.Parse(volume.value);
            m_AudioMixer.SetFloat("GlobalAudioVolume", Mathf.Log10(float.Parse(volume.value)) * 20);
        }
    }
    public void GoTo(int menu)
    {
        GetActiveMenu();
        DisableOrEnableAllMenus(false);
        EnableSingleMenu((Menu)menu);
        m_ActualMenu = (Menu)menu;
    }
    public void Return()
    {
        DisableOrEnableAllMenus(false);
        EnableSingleMenu(m_PreviousMenu[m_PreviousMenu.Count - 1].name);
        SaveUserRebinds();
        SaveVolumeSettings();
        //LoadUserRebinds();
        SaveInfo(m_ActualMenu);
        m_ActualMenu = m_PreviousMenu[m_PreviousMenu.Count - 1].name;
        m_PreviousMenu.Remove(m_PreviousMenu[m_PreviousMenu.Count - 1]);
    }
    private void SaveVolumeSettings()
    {
        SQLController.Instance.SaveVolume("General", m_SliderAudio.value.ToString());
    }
    private void SaveInfo(Menu menu)
    {
        if (menu == Menu.GRAPHICS)
        {
            //Save graphics
            foreach (GraphicsStats graphic in m_Graphics.grafics)
            {
                if (graphic.type == "antialiasing") SQLController.Instance.SaveSettings(graphic.type, graphic.status ? 1 : 0);
                if (graphic.type == "fullscreen") SQLController.Instance.SaveSettings(graphic.type, graphic.status ? 1 : 0);
                if (graphic.type == "quality") SQLController.Instance.SaveSettings(graphic.type, graphic.statusGraphSelected);
            }
        }
    }
    private void SaveUserRebinds()
    {
        string rebinds = NewPlayerController.InputM.ToJson();
        SQLController.Instance.SaveControls(rebinds);
        foreach (InputAction action in NewPlayerController.InputM)
        {
            if (action.bindings[0].effectivePath != "2DVector")
                SQLController.Instance.SaveControls("", action.bindings[0].effectivePath, action.bindings[0].action, true);
        }
    }
    private void LoadUserRebinds()
    {
        string result = SQLController.Instance.GetControls();
        Debug.Log("result - " + result);
        if (result != "")
        {
            NewPlayerController.InputM.LoadBindingOverridesFromJson(result, true);
        }
    }
    public void NewLanguage(string language)
    {
        TranslationController.newLanguage.Invoke(language);
    }
    public void ChangeGraphicsMore(string type)
    {
        GraphicsStats graphic = m_Graphics.grafics.Where(g => g.type == type).First();
        if (graphic != null)
        {
            if (graphic.type == "antialiasing") SetAntiAliasing(graphic);
            if (graphic.type == "fullscreen") SetFullScreen(graphic);
            if (graphic.type == "quality") SetQuality(graphic, true);
        }
    }
    public void ChangeGraphicsLess(string type)
    {
        GraphicsStats graphic = m_Graphics.grafics.Where(g => g.type == type).First();
        if (graphic != null)
        {
            if (graphic.type == "antialiasing") SetAntiAliasing(graphic);
            if (graphic.type == "fullscreen") SetFullScreen(graphic);
            if (graphic.type == "quality") SetQuality(graphic, false);
        }
    }
    public void StartNewGame()
    {
        if (CheckIfTransformsFileExists())
        {
            DisableOrEnableAllMenus(false);
            EnableSingleMenu(Menu.CONFIRM_STARTNEWGAME);
        }
        else
        {
            FinalStartNewGame();
        }

    }
    public void FinalStartNewGame()
    {
        DisableOrEnableAllMenus(false);
        startGame.Invoke(true);
        EnableOrDisableCamera(false);
        m_GameStarted = true;
        m_MenuEnabled = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        startNewGame.Invoke();
        savePositions.Invoke();
        NewPlayerController.startGameLook?.Invoke();
    }
    public void CancelStartGame()
    {
        DisableOrEnableAllMenus(false);
        EnableSingleMenu(Menu.HOME);

    }
    private void EnableOrDisableCamera(bool status)
    {
        m_MenuCamera.enabled = status;
        m_MenuCamera.gameObject.SetActive(status);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    private IEnumerator WaitForInitSettings()
    {
        yield return new WaitForSeconds(1f);
        InitSettings();
    }
    private void ExitPauseMenu()
    {
        DisableOrEnableAllMenus(false);
        GameManager.Instance.DisableOrEnableOtherObjects(true);
        EnableOrDisableCamera(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        AudioLibrary.stopPlayAllSounds?.Invoke(false);
        Time.timeScale = 1;
    }
    public void ContinuePause()
    {
        ExitPauseMenu();
        m_MenuEnabled = false;
    }
    public void Continue()
    {
        continueGame?.Invoke();
        DisableOrEnableAllMenus(false);
        startGame.Invoke(true);
        EnableOrDisableCamera(false);
        m_GameStarted = true;
        m_MenuEnabled = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    public void SaveGame()
    {
        SQLController.Instance.SetRoomStarted("GameSaved");
        savePositions.Invoke();
    }
    public void SetNewAudio(float value)
    {
        m_AudioMixer.SetFloat("GlobalAudioVolume", Mathf.Log10(value) * 20);
    }
}
[System.Serializable]
public class MenuContainers
{
    public Menu name;
    public GameObject gameObject;
}
[System.Serializable]
public class GraphicsText
{
    public string name;
    public TextMeshProUGUI text;
}
public enum Menu
{
    HOME = 0,
    SETTINGS = 1,
    GRAPHICS = 2,
    CONTROLS = 3,
    PAUSE = 4,
    VOLUME = 5,
    CONFIRM_STARTNEWGAME = 6,
}