using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public enum KeysBinding
{
    MOVEMENT,
    JUMP,
    INTERACT,
    EXIT,
    HIDE_ITEM,
    DROP_ITEM,
    FLASHLIGHT_ULTRAVIOLET,
    FLASHLIGHT,
    SHIFT,
    ENTER_MENU,
}
[System.Serializable]
public class KeyRebindingStats
{
    public KeysBinding KeyBinding;
    public InputActionReference Action;
    public TextMeshProUGUI Text;
}