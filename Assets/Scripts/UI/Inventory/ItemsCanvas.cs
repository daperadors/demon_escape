using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemsCanvas : MonoBehaviour
{
    [SerializeField] private Image m_itemImage;
    [SerializeField] private TextMeshProUGUI m_QuantityItems;
    [SerializeField] private GameObject m_ItemsContainer;
    [SerializeField] private ItemChange m_Inventory;
    void Start()
    {
        m_ItemsContainer.SetActive(false);
        gameObject.SetActive(false);
        ItemChange.itemChaged += NewItemChanged;
        MenuController.startGame += StartGame;
    }
    private void NewItemChanged(Sprite sprite)
    {
        m_QuantityItems.text = m_Inventory.getItemsInInvetary().ToString() == "0" ? "" : m_Inventory.getItemsInInvetary().ToString();
        if (sprite == null)
        {
            m_itemImage.gameObject.SetActive(false);
            return;
        }
        m_itemImage.gameObject.SetActive(true);
        m_ItemsContainer.SetActive(true);
        m_itemImage.sprite = sprite;
        
    }
    private void StartGame(bool status)
    {
        gameObject.SetActive(true);
    }

}
