using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicVoiceID : MonoBehaviour
{
    [SerializeField] private int m_ID;
    public bool Active;
    public int ID { get { return m_ID; } }
}
