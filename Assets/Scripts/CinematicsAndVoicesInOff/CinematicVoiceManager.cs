using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CinematicVoiceManager : MonoBehaviour
{
    [SerializeField] private Transform[] m_Transforms;
    private List<CinematicVoiceID> m_Colliders;
    private void Awake()
    {
        MenuController.continueGame += LoadData;
    }
    void Start()
    {
        m_Colliders = new List<CinematicVoiceID>();
        foreach (Transform transform in m_Transforms)
        { 
            GetAllChildsAndComponent(transform);
        }
        InsertDataObjects();
    }
    void GetAllChildsAndComponent(Transform transforms)
    {
        CinematicVoiceID collider = null;
        foreach (Transform t in transforms)
        {
            if (t.gameObject.TryGetComponent<CinematicVoiceID>(out collider)) m_Colliders.Add(collider);
            GetAllChildsAndComponent(t);
        }
    }
    void InsertDataObjects()
    {
        foreach (CinematicVoiceID c in m_Colliders)
        {
            SQLController.Instance.SaveCinematicOrVoiceCollidersStart(c.ID);
        }
    }
    void LoadData()
    {
        List<int> ids = SQLController.Instance.GetAllCollidersDisabled();
        foreach (int id in ids)
        {
            CinematicVoiceID collider = m_Colliders.Where(c => c.ID == id).FirstOrDefault();
            print(id + " " + collider);
            if (collider != null)
            {
                collider.gameObject.SetActive(false);
            }
        }
    }
}
