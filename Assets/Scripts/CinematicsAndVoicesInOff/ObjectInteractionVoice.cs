using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectInteractionVoice : MonoBehaviour
{

    [SerializeField] private List<InteractionVoices> _BoxesList;
    [SerializeField] public List<InteractionVoices> _ItemsList;

    private ItemsList _itemsListVoices;

    public delegate void InitializeVoiceBox(string name);
    public static InitializeVoiceBox onInitializeVoiceBox;

    public delegate void InitializeVoiceItem(string name);
    public static InitializeVoiceItem onInitializeVoiceItem;

    public delegate void SetVoiceExecute(ItemsList items);
    public static SetVoiceExecute onSetVoiceExecute;


    private void Start()
    {
        onInitializeVoiceBox += IntializeBoxAssociatedVoice;
        onInitializeVoiceItem += IntializeItemAssociatedVoice;
        onSetVoiceExecute += SetVoiceExecuteIfItemHas;
    }

    public void IntializeBoxAssociatedVoice(string boxTag)
    {
        Debug.Log("Initialize"+" "+ boxTag);
        InteractionVoices boxVoice = _BoxesList.Where(x => x._objectName == boxTag).FirstOrDefault();
        VoiceManager.onPlayVoice(boxVoice._voiceAssociated);
    }

    public void IntializeItemAssociatedVoice(string itemTag)
    {
        Debug.Log("Initialize" + " " + itemTag);

        ItemStats item = _itemsListVoices._itemsList.Where(x => x._nameItem.Equals(itemTag)).FirstOrDefault();
        InteractionVoices itemVoice = _ItemsList.Where(x => x._objectName == itemTag).FirstOrDefault();
        
        if(itemVoice != null && item._voiceToExecute == true)
        {
            item._voiceToExecute = false;
            VoiceManager.onPlayVoice(itemVoice._voiceAssociated); 
        }
    }

 
    private void SetVoiceExecuteIfItemHas(ItemsList itemList)
    {
        Debug.Log(itemList+" items");
        _itemsListVoices = itemList;

        for(int x = 0, c = _ItemsList.Count; x < c; x++)
        {
            ItemStats item = itemList._itemsList.Where(i => i._nameItem == _ItemsList[x]._objectName).FirstOrDefault();
            item._voiceToExecute = true;
        }
    }

}


[System.Serializable]
public class InteractionVoices
{
    public string _objectName;
    public Voices _voiceAssociated;
}


