using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoaderController : MonoBehaviour
{
    public static LoaderController Instance;
    [SerializeField] private Slider m_ProgressBar;
    [SerializeField] private TextMeshProUGUI m_ProgressText;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        m_ProgressBar.value = 0;
        StartCoroutine(LoadGameScene());
    }
    private IEnumerator LoadGameScene()
    {
        AsyncOperation sceneOperation = SceneManager.LoadSceneAsync("GameScene");
        gameObject.SetActive(true);
        while (!sceneOperation.isDone)
        {
            Task.Delay(100);
            m_ProgressText.text = ((sceneOperation.progress + .1f) * 100).ToString() + "/100%";
            m_ProgressBar.value = sceneOperation.progress+.1f;
            yield return null;
        }
        yield return new WaitForSeconds(5f);
        gameObject.SetActive(false);
    }
}
