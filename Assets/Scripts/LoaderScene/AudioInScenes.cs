using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioInScenes : MonoBehaviour
{
    public static AudioInScenes Instance;
    public delegate void DoraemonSong();
    public static DoraemonSong doraemonSong;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            doraemonSong += PlayDoraemonSong;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void PlayDoraemonSong()
    {
        gameObject.GetComponent<AudioSource>().Play();
    }
}
