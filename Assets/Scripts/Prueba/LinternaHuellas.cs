using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinternaHuellas : MonoBehaviour
{

    public delegate void onFlashLightEnable(bool enable);
    public static onFlashLightEnable flashLightEnable;

    public delegate void onCanSee(bool can);
    public static onCanSee canSee;

    public bool _enable = false;
    private bool _canSee;
    [SerializeField] private Light _flashLight;

    private void Start()
    {
        _flashLight.enabled = false;
    }

    public void CanSee(bool can)
    { 
        canSee?.Invoke(can);
    }

    public void EnableFlashLight()
    {
        _enable = !_enable;
        _flashLight.enabled = _enable;
        if(_flashLight.enabled == false) 
        {
            canSee?.Invoke(false);
        }
    }


}
