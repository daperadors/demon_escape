using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class VoiceRecognition : MonoBehaviour
{
   
    KeywordRecognizer _keywordRecognizer;
    private Dictionary<string, Action> _actions;

    private void Start()
    {
        _actions = new Dictionary<string, Action>();
        _actions.Add("loro", Function);
        _actions.Add("super", Function);

        _keywordRecognizer = new KeywordRecognizer(_actions.Keys.ToArray());
        _keywordRecognizer.OnPhraseRecognized += WordRecognize;
        _keywordRecognizer.Start();

    }

    private void WordRecognize(PhraseRecognizedEventArgs word)
    {
        Debug.Log(word.text);
        _actions[word.text]?.Invoke();
    }

    private void Function()
    {
        Debug.Log("Daemon");
    }

}
