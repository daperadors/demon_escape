using System.Collections;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _enemy;
    [SerializeField] private GameObject[] m_GameObjects;
    [SerializeField] private EscapeRooms[] m_EscapeRooms;
    [SerializeField] private EscapeRooms[] m_Minigames;
    [SerializeField] private Camera m_MainCamera;
    [SerializeField] private EscapeRoomEntranceWaypoint[] m_EscapeRoomWaypoint;
    [SerializeField] private GameObject m_LoadingGame;
    private Camera m_MinigameCamera;
    public Camera minigameCamera { get { return m_MinigameCamera; } }

    public static GameManager Instance;

    public delegate void EnableGameEnemy(bool enable);
    public static EnableGameEnemy onEnableGameEnemy;


    private MonoBehaviour m_Script;
    private Camera m_Camera;
    private ReflectionProbe m_ReflectionProbe;
    private Light m_Light;
    private string m_ActualEscapeRoom;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        NewPlayerController.startRoom += ActiveOrDisableSingleRoom;
        MenuController.startGame += StartGame;
    }
    void Start()
    {
        StartCoroutine(DisableAllRoomsAndOther());
        StartCoroutine(WaitForStart());
        StartCoroutine(AutoSaveGame());
    }

    private void EnableEnemy(bool enable)
    {
        _enemy.gameObject.SetActive(enable);
    }
    private void StartGame(bool status)
    {
        ContinueGame();
        DisableOrEnableOtherObjects(status);
    }
    public void DisableOrEnableOtherObjects(bool status)
    {
        foreach (GameObject go in m_GameObjects)
        {
            Canvas canvas = null;
            if (!go.TryGetComponent<Canvas>(out canvas))
            {
                DisableEnableComponents(go, status, false);
                ActiveOrDisableChilds(go, status, false);
            }
            else go.SetActive(status);
        }
    }
    void ActiveOrDisableAllRooms(bool status)
    {
        foreach (EscapeRooms room in m_EscapeRooms)
        {
            DisableEnableComponents(room.room, status);
            ActiveOrDisableChilds(room.room, status);

        }
    }
    void ActiveOrDisableChilds(GameObject parent, bool status, bool disableInitObject = true)
    {
        for (int i = 0; i < parent.transform.childCount; i++)
        {
            if(!PlayerInsideMinigame(status, parent, i, disableInitObject)) DisableEnableComponents(parent.transform.GetChild(i).gameObject, status, disableInitObject);
            ActiveOrDisableChilds(parent.transform.GetChild(i).gameObject, status, disableInitObject);
        }
    }
    bool PlayerInsideMinigame(bool status, GameObject parent, int i, bool disableInitObject)
    {
        if (status)
        {
            if (parent.transform.GetChild(i).gameObject.name == "PlayerCamera")
            {
                if (NewPlayerController.Instance.MinigameCameras.Cupboard != null || NewPlayerController.Instance.MinigameCameras.Minigame != null) return true;
            }
        }
        return false;
    }
    private void DisableEnableComponents(GameObject gameObject, bool status, bool disableInitObject = true)
    {
        if (gameObject.tag != "IgnoreCamera" && gameObject.TryGetComponent<Camera>(out m_Camera))
        {
            m_Camera.enabled = status;
        }
        if (gameObject.name == "mirrorAMirror")
        {
            gameObject.SetActive(status);
        }
        if (gameObject.TryGetComponent<Light>(out m_Light))
        {
            m_Light.enabled = status;
        }
        if (gameObject.TryGetComponent<MonoBehaviour>(out m_Script))
        {
            m_Script.enabled = status;
        }
    }
    private void ActiveOrDisableSingleRoom(string roomName, bool status)
    {
        //ActiveOrDisableAllRooms(false);
        //if(m_ActualEscapeRoom != "")
        if (roomName != "All")
        {
            GameObject roomToOpen = m_EscapeRooms.Where(r => r.roomName == roomName).FirstOrDefault().room;
            m_ActualEscapeRoom = !status ? "" : roomName;
            DisableEnableComponents(roomToOpen, status);
            foreach (Transform room in roomToOpen.transform)
            {
                DisableEnableComponents(room.gameObject, status);
                ActiveOrDisableChilds(room.gameObject, status);

            }
        }
    }
    public void ActiveOrDisableMinigame(string roomName, bool status)
    {
        GameObject roomToOpen = m_EscapeRooms.Where(r => r.roomName == roomName).FirstOrDefault().room;
        Debug.Log("Starting minigame " + roomName + "...");
        DisableEnableComponents(roomToOpen, status);
        foreach (Transform room in roomToOpen.transform)
        {
            DisableEnableComponents(room.gameObject, status);
            ActiveOrDisableChilds(room.gameObject, status);
        }
    }
    public void StartEscapeRoom(string room, bool value, DoorController door, string tagDoor, GameObject trigger, Transform entraceWaypoint)
    {

        NewPlayerController.startRoom.Invoke(room, true);
        door.tag = tagDoor;
        ActiveOrDisableSingleRoom(room, value);
        door.CloseDoor();
        trigger.SetActive(false);
        SQLController.Instance.SetAllRoomFinished();
        SQLController.Instance.SetRoomStarted(room);
        Debug.Log("Starting room " + room + "...");
    }
    public void StartOrDisableRoom(GameObject room, bool start)
    {
        TriggersInteract triggerController = room.GetComponentInChildren<TriggersInteract>();
        if (triggerController != null)
        {
            if (start)
            {
                triggerController.StartRoom();
                triggerController.gameObject.SetActive(false);
            }
            else
            {
                triggerController.DisableOrEnableDoor(false);
                triggerController.gameObject.SetActive(false);
            }
        }
    }
    public Transform GetEntranceRoom(string roomName)
    {
        EscapeRoomEntranceWaypoint room = m_EscapeRoomWaypoint.Where(e => e.roomName == roomName).FirstOrDefault();
        return room != null ? room.waypoint : null;
    }
    void ContinueGame()
    {
        m_LoadingGame.SetActive(false);
    }
    public void DisableAllMinigames()
    {
        foreach (EscapeRooms minigame in m_Minigames)
        {
            if (minigame != null)
            {
                ActiveOrDisableSingleRoom(minigame.roomName, false);
            }
        }
    }
    IEnumerator DisableAllRoomsAndOther()
    {
        yield return new WaitForSeconds(3f);
        ActiveOrDisableAllRooms(false);
        DisableOrEnableOtherObjects(false);
    }
    IEnumerator WaitForStart()
    {
        yield return new WaitForSeconds(3f);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    IEnumerator AutoSaveGame()
    {
        while (true)
        {
            yield return new WaitForSeconds(180f);
            m_LoadingGame.SetActive(true);
            SQLController.Instance.SetRoomStarted("GameSaved");
            MenuController.savePositions?.Invoke();
            yield return new WaitForSeconds(3f);
            m_LoadingGame.SetActive(false);
        }
    }
}
[System.Serializable]
public class EscapeRooms
{
    public string roomName;
    public GameObject room;
}
[System.Serializable]
public class EscapeRoomEntranceWaypoint
{
    public string roomName;
    public Transform waypoint;

    public EscapeRoomEntranceWaypoint(string name, Transform waypoint)
    {
        this.roomName = name;
        this.waypoint = waypoint;
    }
}