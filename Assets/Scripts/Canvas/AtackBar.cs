using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AtackBar : MonoBehaviour
{
    [SerializeField] private GameObject _Bar;
    [SerializeField] private Transform _reference;
    [SerializeField] private Image _movementBar;
    [SerializeField] private float _movementSpeed;
    [SerializeField] private TextMeshProUGUI _feedBackText;

    [SerializeField] private AtackBarCreator[] _atacksBar;
    private AtackBarCreator _actualAtackBar;

    [SerializeField] private float _speedMax = 7;
    private int _fillMax = 1;
    private bool _finish;

    private Coroutine _atackBarCoroutine;

    private void Start()
    {
        //_actualAtackBar = _atacksBar[Random.Range(0, _atacksBar.Length)];
    }

    public void Enable(bool enable)
    {
        _feedBackText.enabled = enable;
        _Bar.SetActive(enable);
    }

    public void Initialize()
    {
        _actualAtackBar = _atacksBar[Random.Range(0, _atacksBar.Length)];
        _reference.GetComponent<RectTransform>().anchoredPosition = _actualAtackBar._referencePosition;
        _movementSpeed = .5f;
        _movementBar.fillAmount = 0;
        _Bar.SetActive(true);
        _atackBarCoroutine = StartCoroutine(MoveAtackBar());
    }

    private IEnumerator MoveAtackBar()
    {

        while (true)
        {
            while (_movementBar.fillAmount < _fillMax)
            {
                _movementBar.fillAmount += .5f * Time.fixedDeltaTime * _movementSpeed;
                yield return new WaitForSeconds(0.01f);
            }

            while (_movementBar.fillAmount > 0)
            {
                _movementBar.fillAmount -= .5f * Time.fixedDeltaTime * _movementSpeed;
                yield return new WaitForSeconds(0.01f);
            }

            if (_movementSpeed < _speedMax) _movementSpeed += .2f;
        }
    }

    public bool CheckBarAtackPosition()
    {
        _feedBackText.enabled = true; 
        if (InCorrectBarPosition())
        {
            _feedBackText.text = "GREAT!";
            return true;

        }
        else
        {
            _feedBackText.text = "GOOD BYE";
            return false;
        }


    }
    public void StopCoroutineAttackBar()
    {
        if(_atackBarCoroutine != null) StopCoroutine(_atackBarCoroutine);
    }
    private bool InCorrectBarPosition()
    {
        print(_movementBar.fillAmount +" "+ _actualAtackBar._minFill+" "+ _actualAtackBar._maxFill);
        if ((_movementBar.fillAmount >= _actualAtackBar._minFill) && (_movementBar.fillAmount <= _actualAtackBar._maxFill))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public TextMeshProUGUI GetFeedBackText()
    {
        return _feedBackText;
    }

}
