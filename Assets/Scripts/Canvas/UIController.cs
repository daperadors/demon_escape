using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI _interactionText;
    [SerializeField] private AtackBar _atackBar;
    [SerializeField] private Image _deadImage;
    [SerializeField] private float _animationDuration = 1.2f;
    private Tweener _twenerScale;

    private Vector3 _originalScale;
    private Vector3 _scaleTo;
   

    private void Start()
    {
        _originalScale = _interactionText.transform.localScale;
        _scaleTo = _originalScale * 1.2f;
        _atackBar.Enable(false);
        NewPlayerController.playerDead += FadeInDead;
    }


    // ATACK BAR
    public void InitializeAtackBar()
    {
        _interactionText.enabled = true;
        ScaleText();  
        _atackBar.Initialize();
    }

    public void DisableSystemAtack()
    {
        _interactionText.enabled = false;
        _atackBar.StopCoroutineAttackBar();
        _twenerScale.Kill();
        _atackBar.Enable(false);
    }

    
    public bool CheckAtackBar()
    {
        return _atackBar.CheckBarAtackPosition();
    }

    private void ScaleText()
    {
        _twenerScale = _interactionText.transform.DOScale(_scaleTo, _animationDuration).SetEase(Ease.Linear).OnComplete(() =>
        {
            _interactionText.transform.DOScale(_originalScale, _animationDuration)
            .SetEase(Ease.Linear)
            .OnComplete(ScaleText);
        }).SetLoops(-1);  
    }

    private void FadeInDead()
    {
        NewPlayerController.stopAudio?.Invoke();
        StartCoroutine(FadeIn());
    }
    IEnumerator FadeIn()
    {
        Color colorImage = _deadImage.color;
        while (colorImage.a < 1f)
        {
            colorImage.a = Mathf.Lerp(colorImage.a, 2f, 1.5f * Time.deltaTime);
            _deadImage.color = colorImage;
            yield return new WaitForFixedUpdate();
        }
        StartCoroutine(FadeInOutAndRestartGame());
    }    
    IEnumerator FadeInOutAndRestartGame()
    {
        yield return new WaitForSeconds(2.5f);
        if (!SQLController.Instance.GetIfRoomIsStarted("GameSaved") && !SQLController.Instance.GetIfRoomIsStarted("InsideHouse"))
        {
            if(!SQLController.Instance.GetStatusRoom("GameSaved") && !SQLController.Instance.GetStatusRoom("InsideHouse"))
                SQLController.Instance.ResetAllTables();
        }
        MenuController.continueGame.Invoke();
        MenuController.startGame.Invoke(true);
        Color colorImage = _deadImage.color;
        while (colorImage.a > 0.1f)
        {
            colorImage.a = Mathf.Lerp(colorImage.a, 0f, 1.5f * Time.deltaTime);
            _deadImage.color = colorImage;
            yield return new WaitForFixedUpdate();
        }
    }
}
