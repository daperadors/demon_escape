using System.Collections;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{

    [SerializeField] NewPlayerController _player;
    public bool _mover = false;
    private Animator m_Animator;
    private Rigidbody _rigidBody;
    private Coroutine _movementCorutine;

    private bool _fallenBoards = false;

    private void Start()
    {
        m_Animator = GetComponent<Animator>();
        _rigidBody = GetComponent<Rigidbody>();
    }

    public void EnableObstacleMovement()
    {
        if (_mover == true)
        {
            _movementCorutine = StartCoroutine(MoveObstacle());
        }
    }

    private IEnumerator MoveObstacle()
    {
        while (_mover == true)
        {
            float distancia = Unity.Mathematics.math.distancesq(transform.position, _player.transform.position);
            distancia *= distancia;
            if (Mathf.Abs(distancia) > 3f)
            {
                Vector3 direction = new Vector3(0, 0, Mathf.Sign(distancia));
                _rigidBody.MovePosition(_rigidBody.position + direction * Time.deltaTime * 2);
            }
            else
            {
                _mover = false;
            }

            yield return new WaitForEndOfFrame();
        }
    }


    public void TablesDown()
    {
        if(_fallenBoards == false)
        {
            m_Animator.Play("Down");
            _fallenBoards = true;
        }
    }


    public void Interact()
    {
        m_Animator.Play("Up");
        _fallenBoards = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            if (gameObject.CompareTag("FurnitureObstacle"))
            {
                if (_movementCorutine != null) StopCoroutine(_movementCorutine);
            }
        }
    }
}