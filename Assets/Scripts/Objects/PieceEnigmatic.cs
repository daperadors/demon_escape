using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceEnigmatic : MonoBehaviour
{
    [SerializeField] private int m_ID;
    [SerializeField] private GameObject m_GroundObject;
    [SerializeField] private GameObject m_AnimatorObject;

    public int ID { get { return m_ID; } }
    public GameObject GroundObject { get { return m_GroundObject; } }
    public GameObject AnimatorObject { get { return m_AnimatorObject; } }
}
