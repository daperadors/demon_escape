using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnitureController : MonoBehaviour
{

    private float _speed = 10.0f;

    private void Update()
    {
        MoveFurniture();
    }


    public void MoveFurniture()
    {
        if (Input.GetMouseButton(0))
        {
            float mouseX = Input.GetAxis("Mouse X") * _speed * Time.deltaTime;
            transform.position += new Vector3(mouseX, 0f, mouseX);
        }
    }
}
