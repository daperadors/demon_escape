using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PHController : MonoBehaviour
{
    [SerializeField] private Transform m_NewTransform;
    [SerializeField] private Transform m_FinalPosition;
    [SerializeField] private GameObject m_Cosas;
    [SerializeField] private Image _blackImage;

    private Rigidbody m_Rigidbody;
    private Coroutine m_Coroutine;
    public delegate void ActivePHPickup();
    public delegate void StartDrive();
    public static ActivePHPickup activePikup;
    public static StartDrive startDrive;
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();

        activePikup += ActivePickup;
        startDrive += PikupStartDrive;
        MenuController.continueGame += () =>
        {
            if (SQLController.Instance.GetStatusRoom("InsideHouse")) {
                ActivePickup();
                m_Cosas.SetActive(true);
                m_Cosas.GetComponentInChildren<Camera>().enabled = false;
            }
        };
        m_Cosas.SetActive(false);
    }
    private void ActivePickup()
    {
        if (m_NewTransform != null)
        {
            m_Cosas.SetActive(true);
            m_Cosas.GetComponentInChildren<Camera>().enabled= false;
            transform.position = m_NewTransform.position;
            transform.rotation = m_NewTransform.rotation;
            gameObject.layer = LayerMask.NameToLayer("Interactable");
        }
    }
    private void PikupStartDrive()
    {
        m_Cosas.GetComponentInChildren<Camera>().enabled = true;
        StartCoroutine(PikupStartDriveCoroutine());
    }
    IEnumerator PikupStartDriveCoroutine()
    {
        Vector3 direction = m_FinalPosition.position - transform.position;
        direction.Normalize();
        while (Unity.Mathematics.math.distancesq(transform.position, m_FinalPosition.position) > 10f)
        {
            m_Rigidbody.MovePosition(m_Rigidbody.position + direction * Time.fixedDeltaTime * 5f);
            if(Unity.Mathematics.math.distancesq(transform.position, m_FinalPosition.position) < 200f)
            {
                if(m_Coroutine == null)
                {
                    AudioInScenes.doraemonSong?.Invoke();
                    m_Coroutine = StartCoroutine(FadeInAndChangeScene());
                }
            }
            yield return new WaitForFixedUpdate();
        }
        NewPlayerController.playerFinishFollowTarget?.Invoke();
    }
    IEnumerator FadeInAndChangeScene()
    {
        Color colorImage = _blackImage.color;
        while (colorImage.a < 1f)
        {
            colorImage.a = Mathf.Lerp(colorImage.a, 2f, .5f * Time.deltaTime);
            _blackImage.color = colorImage;
            yield return new WaitForEndOfFrame();
        }
        Invoke("ChengeScen", 2f);
    }
    private void ChengeScen()
    {
        SQLController.Instance.SetRoomFinished("GameSaved");
        SceneManager.LoadScene("CreditScene");
    }
}
