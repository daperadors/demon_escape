using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightTorchController : MonoBehaviour
{
    [SerializeField] private float[] m_Intensity;
    [SerializeField] private float m_Speed;

    private Light m_TorchLight;
    private bool m_Increasing = true;

    private void Start()
    {
        m_TorchLight = GetComponent<Light>();
    }
    private void Update()
    {
        float currentIntensity = m_TorchLight.intensity;
        if (currentIntensity >= m_Intensity[1] - 1)
        {
            m_Increasing = false;
            m_Speed = Random.Range(m_Speed - 2, m_Speed + 2);
        }
        else if (currentIntensity <= m_Intensity[0] - 1) m_Increasing = true;

        if (m_Increasing) currentIntensity += m_Speed * Time.deltaTime;
        else currentIntensity -= m_Speed * Time.deltaTime;

        if (currentIntensity > 25) m_Increasing = false;
        m_TorchLight.intensity = currentIntensity;
    }
}
