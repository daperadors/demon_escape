using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsId : MonoBehaviour
{
    [SerializeField] private int m_Id;
    public int id { get { return m_Id; } }
}
