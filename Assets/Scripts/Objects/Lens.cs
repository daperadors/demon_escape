using UnityEngine;

public class Lens : MonoBehaviour
{
    private Animator m_Animator;
    void Start()
    {
        m_Animator = GetComponent<Animator>();
    }
    void Update()
    {
        if(Input.GetMouseButtonDown(1))
            m_Animator.SetBool("ZoomIn", true);      
        if(Input.GetMouseButtonDown(0))
        {
            if(m_Animator.GetBool("ZoomIn"))
                m_Animator.SetBool("ZoomIn", false);
        }
    }
}
