using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsExtraId : MonoBehaviour
{
    [SerializeField] private int m_Id;
    [SerializeField] private bool m_IsTaken;
    public int id { get { return m_Id; } }
    public bool IsTaken { get { return m_IsTaken; } }
}
