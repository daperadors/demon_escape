using System.Collections.Generic;
using UnityEngine;

public class ObjectsExtraController : MonoBehaviour
{
    [SerializeField] private Transform m_House;
    private static List<ObjectsExtraId> m_Objects;
    public static List<ObjectsExtraId> Objects => m_Objects;
    private void Awake()
    {
        MenuController.continueGame += ContinueGame;
    }
    void Start()
    {
        m_Objects = new List<ObjectsExtraId>();
        GetAllChildsAndComponent(m_House);
        InsertDataObjects();
    }
    void GetAllChildsAndComponent(Transform transform)
    {
        ObjectsExtraId objectExtra = null;
        foreach (Transform t in transform)
        {
            if (t.gameObject.TryGetComponent<ObjectsExtraId>(out objectExtra)) m_Objects.Add(objectExtra); 
            GetAllChildsAndComponent(t);
        }
    }
    void InsertDataObjects()
    {
        foreach (ObjectsExtraId o in m_Objects)
        {
            SQLController.Instance.SaveObjectsExtraStart(o.id, o.gameObject.name);
        }
    }
    void ContinueGame()
    {
        foreach (ObjectsExtraId o in m_Objects)
        {
            if (!SQLController.Instance.GetIfItemIsVisible(o.id))
            {
                o.gameObject.SetActive(false);
            }
        }
    }
}
