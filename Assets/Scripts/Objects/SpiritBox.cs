using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;


public class SpiritBox : MonoBehaviour
{

    KeywordRecognizer _keywordRecognizer;
    private Dictionary<string, Action> _actions;
    private bool _executeActions;
    public bool _isActive = false;

    public delegate void EnemySignal();
    static public EnemySignal onEnemySignal;

    private void Start()
    {

        _actions = new Dictionary<string, Action>();
        _actions.Add("dame una se�al", SendSignal);

        _keywordRecognizer = new KeywordRecognizer(_actions.Keys.ToArray());
        _keywordRecognizer.OnPhraseRecognized += UseRecognizeWord;
        _keywordRecognizer.Start();
    }
    private void OnDisable()
    {
        _isActive = false;
        if (_keywordRecognizer != null)
        {
            _keywordRecognizer.Stop();
            _keywordRecognizer.Dispose();
            _actions.Clear();
        }
    }
    private void OnDestroy()
    {
        _isActive = false;
        if (_keywordRecognizer != null)
        {
            _keywordRecognizer.Stop();
            _keywordRecognizer.Dispose();
            _actions.Clear();
        }
    }

    private void UseRecognizeWord(PhraseRecognizedEventArgs word)
    {
        if(_isActive == true)
        {
            Debug.Log(word.text);
            _actions[word.text]?.Invoke();
        }
    }

    private void SendSignal()
    {
        onEnemySignal?.Invoke();
    }


}

