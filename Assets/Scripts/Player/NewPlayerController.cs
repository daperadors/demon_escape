using FiniteStateMachine;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class NewPlayerController : MonoBehaviour
{
    [SerializeField] private InputActionAsset m_Input;
    [SerializeField] private ItemChange m_Inventory;
    [SerializeField] public UIController _uiController;
    [SerializeField] private EnemyController _enemy;
    [SerializeField] public SphereCollider enemyCollider;
    [SerializeField] public Rigidbody _rigidbody;
    [SerializeField] private Camera m_Camera;
    [SerializeField] private Camera m_MinigameCamera;
    [SerializeField] public Camera HideCamera;
    [SerializeField] public Camera _cameraAnimator;
    [SerializeField] public Transform cameraHolder;
    [SerializeField] private Transform m_HeadBobHolder;
    [SerializeField] private Transform m_EnemyHead;
    [SerializeField] private Vector3 m_NormalPositionCameraHolder;
    public Transform _enemyHeadTarget { get { return m_EnemyHead; } }
    [SerializeField] private GameObject m_PlayerHands;
    [SerializeField] private GameObject m_FlashlightGround;
    [SerializeField] private GameObject m_Flashlight;
    [SerializeField] private GameObject[] m_FlashlightStatus;
    [SerializeField] private GameObject m_MinigameLight;
    [SerializeField] LayerMask m_LayerWall;
    [SerializeField] LayerMask m_LayerGround;
    [SerializeField] LayerMask m_LayerDrawer;
    [SerializeField, Range(0.5f, 10)] float m_LookSpeed = 2.0f;
    [SerializeField, Range(10, 120)] float m_LookXLimit = 80.0f;
    [SerializeField, Range(0.1f, 5)] float m_HideDistance = 1.5f;
    [SerializeField, Range(1, 10)] float m_WalkingSpeed = 3.0f;
    [SerializeField, Range(1, 10)] float m_RunningSpeed = 5.5f;
    [SerializeField, Range(1, 10)] float m_ShiftingSpeed = 2f;
    [SerializeField] float m_RunningFOV = 65.0f;
    [SerializeField] float m_SpeedToFOV = 4.0f;


    [Header("Player Extras")]
    [SerializeField] private PlayerExtras _playerExtraConfig;

    public delegate void OnAtack();
    public delegate void StunEnemy();
    public delegate void SetEnemyVelocity(string animation, bool status);
    public delegate void StartRoom(string room, bool status);
    public delegate void MinigameFinished();
    public delegate void PlayerDead();
    public delegate void PlayerHideIntoCupboard();
    public delegate void PlayPianoFa();
    public delegate void SetNewAudio(PlayerStatesSounds state, bool outside);
    public delegate void StopAudioMovement();
    public delegate void InsideHouse();
    public delegate void StartGameLook();
    public delegate void onCinematicPlayer();
    public delegate void PlayerEnterRoom();
    public delegate void CanSeeFingerPrint(bool can);


    [Header("Character delegates")]
    public static OnAtack onAtackInteraction;
    public static StunEnemy onStunEnemy;
    public static SetEnemyVelocity onSetEnemyVelocity;
    public static StartRoom startRoom;
    public static MinigameFinished minigameFinished;
    public static PlayerDead playerDead;
    public static PlayerHideIntoCupboard playerHide;
    public static PlayPianoFa playPiano;
    public static SetNewAudio setNewAudio;
    public static StopAudioMovement stopAudio;
    public static InsideHouse insideHouse;
    public static StartGameLook startGameLook;
    public static onCinematicPlayer playerFinishFollowTarget;
    public static PlayerEnterRoom playerEnterRoom;
    public static CanSeeFingerPrint onCanSeeFingerPrint;


    [Header("Sounds Controller")]
    public AudioLibrary _audioLibrary;

    private FSM m_FSM;
    public FSM FSM { get { return m_FSM; } }
    private static InputActionAsset m_InputStatic;
    public static InputActionAsset InputM => m_InputStatic;
    public MinigameCameras MinigameCameras;
    private GameObject m_cupboard;
    private string m_ActualMinigame;
    private float[] m_PuzzleClamp = { 166, 193 };
    private float _atackDistance = 10f;
    private float m_InitFov;
    public float vertical;
    public float horizontal;
    private float m_Yaw;
    private float m_Pitch;
    private float rotationX;
    private float m_HeadBobTimer;
    private bool m_WallDistance;
    private static bool m_InsideEscapeRoom = true;
    public static bool InsideEscapeRoom { get { return m_InsideEscapeRoom; } set { m_InsideEscapeRoom = value; } }
    private bool m_FlashlightDisabled = false;
    private bool m_VioletFlashlightDisabled = false;
    private bool m_Hide = false;
    private bool m_CameraDisabled = false;
    private bool m_GameStarted = false;
    public bool m_IsInsideHoue = false;
    private bool _targetAnimationFinish = false;
    public static NewPlayerController Instance;
    public float[] puzzleClamp { get { return m_PuzzleClamp; } }
    private bool _canSeeFingerPrint;

    public float lookSpeed { get { return m_LookSpeed; } }
    public float lookXLimit { get { return m_LookXLimit; } }
    public float runningFOV { get { return m_RunningFOV; } }
    public float speedToFOV { get { return m_SpeedToFOV; } }
    public float hideDistance { get { return m_HideDistance; } }
    public float initFov { get { return m_InitFov; } }
    public float walkingSpeed { get { return m_WalkingSpeed; } }
    public float runningSpeed { get { return m_RunningSpeed; } }
    public float shiftingSpeed { get { return m_ShiftingSpeed; } }
    public bool Hide { get { return m_Hide; } set { m_Hide = value; } }
    public bool CameraDisabled { get { return m_CameraDisabled; } }
    public bool PlayerInsideHouse { get { return m_IsInsideHoue; } set { m_IsInsideHoue = value; } }
    public bool GameStarted { get { return m_GameStarted; } }
    public LayerMask LayerWall { get { return m_LayerWall; } }
    public LayerMask LayerDrawer { get { return m_LayerDrawer; } }
    public ItemChange inventory { get { return m_Inventory; } }
    public Camera cameraMain { get { return m_Camera; } }
    public Camera minigameCamera { get { return m_MinigameCamera; } set { m_MinigameCamera = value; } }
    public GameObject flashlight { get { return m_Flashlight; } }
    public GameObject minigameLight { get { return m_MinigameLight; } }
    public GameObject PlayerHands { get { return m_PlayerHands; } }
    public PlayerExtras playerExtraConfig { get { return _playerExtraConfig; } }
    public GameObject cupboard { get { return m_cupboard; } set { m_cupboard = value; } }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        //m_Input = new InputManager();
        TriggersInteract.newMinigameCamera += SetMinigameCamera;
        //LinternaHuellas.canSee += EnableSeeFingerPrint;
        MenuController.startGame += StartGame;
        MenuController.continueGame += ContinueGame;
        PlayerExtras.startLookAtCinematic += ChangeToLookAtState;
        insideHouse += PlayerEnterHouse;
        startGameLook += InitializeStartGameCinematic;
        onCanSeeFingerPrint += EnableFingerPrintVision;

        m_Input.Enable();

        m_InputStatic = m_Input;

        m_FSM = new FSM(gameObject);
        m_FSM.AddState(new IdlePlayerState(m_FSM, m_Input));
        m_FSM.AddState(new WalkingPlayerState(m_FSM, m_Input));
        m_FSM.AddState(new RunningPlayerState(m_FSM, m_Input));
        m_FSM.AddState(new ShiftingPlayerState(m_FSM, m_Input));
        m_FSM.AddState(new AtackPlayerState(m_FSM, _cameraAnimator.gameObject.transform, m_EnemyHead.transform, m_Input));
        m_FSM.AddState(new DeadState(m_FSM, m_EnemyHead.transform, m_Camera.transform));
        m_FSM.AddState(new MinigameState(m_FSM, m_Input));
        m_FSM.AddState(new CinematicPlayerState(m_FSM, m_Camera));
        m_FSM.AddState(new LookAtState(m_FSM, m_Camera));

        m_FSM.ChangeState<IdlePlayerState>();

    }
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;

        if (m_MinigameCamera != null) m_MinigameCamera.enabled = false;
        if (HideCamera != null) HideCamera.enabled = false;
        if (m_MinigameLight != null) m_MinigameLight.SetActive(false);

        MinigameCameras = new MinigameCameras(minigameCamera, HideCamera);

        _rigidbody = GetComponentInChildren<Rigidbody>();
        Physics.gravity = new Vector3(0, -15f, 0);

        _cameraAnimator.enabled = false;
        _canSeeFingerPrint = false;
        m_Camera = GetComponentInChildren<Camera>();
        m_InitFov = m_Camera.fieldOfView;

        m_FlashlightStatus[0].SetActive(true);
        m_FlashlightStatus[1].SetActive(false);
        m_Flashlight.SetActive(false);
        SQLController.Instance.SaveInventoryStart(m_Flashlight.GetComponent<ObjectsId>().id, "Flashlight");
        if (m_Inventory == null && GetComponent<ItemChange>()) m_Inventory = GetComponent<ItemChange>();

        m_FlashlightStatus[3].SetActive(false);

    }

    private void Update()
    {
        if (!m_GameStarted) return;
        m_FSM.Update();
    }

    private void FixedUpdate()
    {
        if (!m_GameStarted) return;
        m_FSM.FixedUpdate();
    }
    private void LateUpdate()
    {
        if (!m_GameStarted) return;
        m_FSM.LateUpdate();
    }
    public void CameraMovement(float rotationSpeed, float rotationXLimit, float fov, float transFov, float hideDistance, LayerMask layerMask, ItemChange items)
    {
        if (!m_Camera.enabled) return;
        if (Cursor.lockState != CursorLockMode.Locked) return;

        RaycastHit m_ObjectCheck;

        m_Yaw = -m_Input.FindAction("Camera").ReadValue<Vector2>().y;
        m_Pitch = m_Input.FindAction("Camera").ReadValue<Vector2>().x;

        rotationX += m_Yaw * (rotationSpeed / 10);
        rotationX = Mathf.Clamp(rotationX, -rotationXLimit, rotationXLimit);
        cameraHolder.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
        transform.rotation *= Quaternion.Euler(0, m_Pitch * rotationSpeed / 10, 0);

        m_Camera.fieldOfView = Mathf.Lerp(m_Camera.fieldOfView, fov, transFov * Time.deltaTime);

        if (m_WallDistance != Physics.Raycast(m_Camera.transform.position, m_FSM.Owner.transform.forward, out m_ObjectCheck, hideDistance, layerMask))
        {
            m_WallDistance = Physics.Raycast(m_Camera.transform.position, m_FSM.Owner.transform.forward, out m_ObjectCheck, hideDistance, layerMask);
            //items.ani.SetBool("Hide", m_WallDistance);
            //items.DefiniteHide = m_WallDistance;
            items.HideItemHand(m_WallDistance);
        }
    }
    public void interactFurnitures(float rotationSpeed, LayerMask layerMask)
    {
        RaycastHit hit;
        float posIni;
        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, 3f, layerMask))
            {
                if (hit.collider.gameObject.tag == "HorizontalL")
                {
                    //hit.transform.localRotation = Quaternion.Euler(0, Mathf.Clamp(hit.transform.localRotation.eulerAngles.y, 0, 90), 0);
                    //hit.transform.localRotation *= Quaternion.Euler(0, -m_Pitch * rotationSpeed / 3, 0);

                    if (hit.transform.localRotation.y >= 0 && hit.transform.localRotation.y <= 0.7)
                    {
                        hit.transform.localRotation *= Quaternion.Euler(0, -m_Pitch * rotationSpeed / 3, 0);
                        Debug.Log(hit.transform.localRotation.y);
                    }
                    else if (hit.transform.localRotation.y > 0.7)
                    {
                        hit.transform.localRotation = Quaternion.Euler(0, 88.5f, 0);
                    }
                    else
                    {
                        hit.transform.localRotation = Quaternion.Euler(0, 0, 0);
                    }
                }
                if (hit.collider.gameObject.tag == "HorizontalR")
                {
                    //hit.transform.localRotation = Quaternion.Euler(0, Mathf.Clamp(hit.transform.localRotation.eulerAngles.y, 0, 270f), 0);
                    //hit.transform.localRotation *= Quaternion.Euler(0, -m_Pitch * rotationSpeed / 3, 0);

                    HingeJoint hingeJoint = hit.collider.gameObject.GetComponent<HingeJoint>();

                    if (hit.transform.localRotation.y <= 0 && hit.transform.localRotation.y >= -0.7)
                    {
                        hit.transform.localRotation *= Quaternion.Euler(0, -m_Pitch * rotationSpeed / 3, 0);
                        Debug.Log(hit.transform.localRotation.y);
                    }
                    else if (hit.transform.localRotation.y < -0.7)
                    {
                        hit.transform.localRotation = Quaternion.Euler(0, -88.5f, 0);
                    }
                    else
                    {
                        hit.transform.localRotation = Quaternion.Euler(0, 0, 0);
                    }
                }

                if (hit.collider.gameObject.tag == "Vertical")
                {
                    posIni = Mathf.Clamp(hit.transform.localPosition.z, 0.03f, 0.46f);
                    hit.transform.localPosition = new Vector3(hit.transform.localPosition.x, hit.transform.localPosition.y, Mathf.Clamp(hit.transform.localPosition.z, 0.03f, 0.46f));

                    hit.transform.position += new Vector3(0, 0, m_Yaw * rotationSpeed / 500);
                }
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, 3f))
                if (hit.collider.gameObject.tag == "Piano")
                {
                    AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.PIANO);
                }
        }
    }
    public bool isGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, GetComponent<CapsuleCollider>().bounds.extents.y * 1.3f, m_LayerGround);
    }
    private void SetMinigameCamera(Camera camera, GameObject light, float n1, float n2, string minigameName, bool cameraDisabled)
    {
        m_MinigameCamera = camera;
        m_MinigameLight = light;
        m_PuzzleClamp[0] = n1;
        m_PuzzleClamp[1] = n2;
        m_ActualMinigame = minigameName;
        m_CameraDisabled = cameraDisabled;
    }
    private void OnTriggerEnter(Collider other)
    {
        m_FSM.OnTriggerEnter(other);
    }

    public bool InFrontOfTheEnemy()
    {
        RaycastHit hit;
        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, _enemy.LayerVisionAngle))
        {
            if (hit.collider.gameObject.CompareTag("Enemy"))
            {
                if (CheckVisionAngle())
                {
                    return true;
                }
            }
            return false;
        }
        return false;
    }
    private bool CheckVisionAngle()
    {
        Vector3 direction = Vector3.Normalize(_enemy.transform.position - transform.position);
        if (Vector3.Dot(transform.forward, direction) > 0)
        {
            return true;
        }
        return false;
    }
    public void DisableFlashlight()
    {
        if (m_Input.FindAction("Flashlight").triggered)
        {
            if(m_Flashlight.activeInHierarchy) AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.SWITCH);

            if (m_FlashlightStatus[3].gameObject.activeInHierarchy)
            {
                
                onCanSeeFingerPrint?.Invoke(false);
                m_FlashlightStatus[3].SetActive(false);
            }

            m_FlashlightDisabled = !m_FlashlightDisabled;

            if (m_FlashlightDisabled)
            {
                m_FlashlightStatus[2].GetComponent<Light>().intensity = .5f;
                m_FlashlightStatus[0].SetActive(false);
                m_FlashlightStatus[1].SetActive(true);
            }
            else
            {
                m_FlashlightStatus[2].GetComponent<Light>().intensity = 17.46f;
                m_FlashlightStatus[0].SetActive(true);
                m_FlashlightStatus[1].SetActive(false);
            }
        }
    }


    public void EnableUltraVioletLight()
    {
        int huellaLayer;

        if (m_Input.FindAction("FlashlightUltraviolet").triggered)
        {
            if (m_FlashlightStatus[2].gameObject.activeInHierarchy)
                m_FlashlightStatus[2].GetComponent<Light>().intensity = 0;

            m_VioletFlashlightDisabled = !m_VioletFlashlightDisabled;

            if (m_VioletFlashlightDisabled)
            {
                m_FlashlightStatus[3].SetActive(true);
                m_FlashlightStatus[0].SetActive(true);
                m_FlashlightStatus[1].SetActive(false);
            }
            else
            {
                onCanSeeFingerPrint?.Invoke(false);
                m_FlashlightStatus[3].SetActive(false);
                m_FlashlightStatus[0].SetActive(false);
                m_FlashlightStatus[1].SetActive(true);
            }
        }
    }

    private void EnableFingerPrintVision(bool canSee)
    {
        int huellaLayer;

        if (canSee)
        {
            huellaLayer = LayerMask.NameToLayer("Huella");
            m_Camera.cullingMask |= (1 << huellaLayer);
        }
        else
        {
            huellaLayer = LayerMask.NameToLayer("Huella");
            m_Camera.cullingMask &= ~(1 << huellaLayer);
        }
    }

    public bool EnemyInDistance(float distance)
    {
        if (Unity.Mathematics.math.distancesq(transform.position, _enemy.transform.position) <= distance)
        {
            return true;
        }
        return false;
    }
    public bool IsNearEnemy()
    {
        if (Unity.Mathematics.math.distancesq(transform.position, _enemy.transform.position) <= _atackDistance)
        {
            if (CheckVisionAngle())
            {
                if (_enemy.InFrontOfThePlayer())
                    return true;
            }
        }
        return false;
    }

    public void SetCameraCinematic(string animation, bool status)
    {
        if (status)
        {
            m_Camera.enabled = false;
            _cameraAnimator.enabled = true;
            _cameraAnimator.gameObject.GetComponent<Animator>().Play(animation);
        }
        else
        {
            _cameraAnimator.enabled = false;
            m_Camera.enabled = true;
        }
    }


    private void StartGame(bool status)
    {
        m_GameStarted = true;
        m_Camera.transform.rotation = new Quaternion(0, 0, 0, 0);
        m_HeadBobHolder.position = Vector3.zero;
        m_HeadBobHolder.rotation = new Quaternion(0, 0, 0, 0);
        cameraHolder.transform.position = new Vector3(0, 0.5620003f, 0);
        cameraHolder.transform.rotation = new Quaternion(0, 0, 0, 0);
        m_FSM.ChangeState<IdlePlayerState>();
    }
    private void ContinueGame()
    {
        if (SQLController.Instance.GetIfFlashlightIsActive())
        {
            m_Flashlight.SetActive(true);
            m_FlashlightGround.SetActive(false);
        }
    }


    public IEnumerator InitialCinematicAction(Transform transform)
    {

        while (true)
        {
            Quaternion rotation = Quaternion.LookRotation(_playerExtraConfig._targetOnCinematic.position - m_Camera.transform.position);
            m_Camera.transform.rotation = Quaternion.Slerp(m_Camera.transform.rotation, rotation, Time.deltaTime * 5);

            if (Quaternion.Angle(m_Camera.transform.rotation, rotation) <= 0.3f)
            {
                break;
            }

            yield return new WaitForFixedUpdate();
        }

        yield return new WaitForSeconds(1f);

        PlayerExtras.onElementIntoCinematicAction?.Invoke(_playerExtraConfig.cinematicInitialElement);

        VoiceManager.onPlayVoice?.Invoke(_playerExtraConfig._voiceIntro);

        while (_targetAnimationFinish == false)
        {
            Quaternion rotation = Quaternion.LookRotation(_playerExtraConfig._targetOnCinematic.position - m_Camera.transform.position);
            m_Camera.transform.rotation = Quaternion.Slerp(m_Camera.transform.rotation, rotation, Time.deltaTime * 6);
            yield return new WaitForFixedUpdate();
        }
    }


    public IEnumerator CameraToTargetCinematic(Quaternion initial)
    {
        float dist;
        Vector3 dir;
        bool rotateBack = false;

        //yield return new WaitForSeconds(1f);

        while (true)
        {
            if (!rotateBack)
            {
                Quaternion rotation = Quaternion.LookRotation(_playerExtraConfig._targetCameraMovement.position - m_Camera.transform.position);

                if (RotateCamera(rotation))
                {
                    if (_playerExtraConfig.cinematicType == CinematicType.EXECUTE_ACTION)
                    {
                        yield return CinematicManager.onCinematicAction?.Invoke();
                    }

                    VoiceManager.onPlayVoice(_playerExtraConfig._voiceIntermidiate);
                    yield return new WaitForSeconds(_playerExtraConfig._returnToInitialDuration);
                    rotateBack = true;
                }
            }

            else
            {
                if (playerExtraConfig._returnCamera == true)
                {
                    if (RotateCamera(initial))
                    {
                        m_FSM.ChangeState<IdlePlayerState>();
                        VoiceManager.onPlayVoice?.Invoke(_playerExtraConfig._voiceExit);
                        break;
                    }
                }
                else
                {
                    m_FSM.ChangeState<IdlePlayerState>();
                    break;
                }
            }

            yield return new WaitForFixedUpdate();
        }

    }

    public IEnumerator NoCameraTargetCinematic(Quaternion initial)
    {
        if (_playerExtraConfig.cinematicType == CinematicType.EXECUTE_ACTION)
        {
            yield return CinematicManager.onCinematicAction?.Invoke();
            yield return new WaitForSeconds(_playerExtraConfig._returnToInitialDuration);
        }

        while (true)
        {
            if (RotateCamera(initial, 2f))
            {
                m_FSM.ChangeState<IdlePlayerState>();
                break;
            }

            yield return new WaitForFixedUpdate();
        }
        VoiceManager.onPlayVoice(_playerExtraConfig._voiceExit);
    }

    private bool RotateCamera(Quaternion rotate, float velocity = 3)
    {
        m_Camera.transform.rotation = Quaternion.Slerp(m_Camera.transform.rotation, rotate, Time.deltaTime * velocity);
        if (Quaternion.Angle(m_Camera.transform.rotation, rotate) <= 0.01f)
        {
            return true;
        }

        return false;
    }

    private void InitializeStartGameCinematic()
    {
        StartCoroutine(StartCinematic());
    }

    private IEnumerator StartCinematic()
    {
        PlayerExtras.setNewLookAt(_playerExtraConfig._startTarget, 1f, true);
        yield return new WaitForSeconds(1f);
        ChangeToLookAtState();
    }

    private void ChangeToLookAtState()
    {
        m_FSM.ChangeState<LookAtState>();
    }
    private void PlayerEnterHouse()
    {
        m_IsInsideHoue = true;
    }
    public void SetDefaultPositionCameraHolder()
    {
        cameraHolder.position = m_NormalPositionCameraHolder;
    }
}

[System.Serializable]
public class MinigameCameras
{
    public Camera Minigame;
    public Camera Cupboard;
    public MinigameCameras(Camera cameraM, Camera cameraC)
    {
        Minigame = cameraM;
        Cupboard = cameraC;
    }
}