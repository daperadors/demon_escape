using FiniteStateMachine;
using UnityEngine;

public class MovementPlayerState : State
{
    private NewPlayerController m_Player;
    private Rigidbody m_Rigidbody;
    private Camera m_Camera;
    private InputManager m_Input;
    private RaycastHit m_ObjectCheck;
    private Vector3 m_MovementVector;

    private float m_Yaw;
    private float m_Pitch;
    private float rotationX;
    private bool m_WallDistance;
    public MovementPlayerState(FSM fsm, InputManager input)
        : base(fsm)
    {
        m_Input = input;
    }
    public override void Init()
    {
        base.Init();
        m_Player = m_FSM.Owner.GetComponent<NewPlayerController>();
        m_Rigidbody = m_FSM.Owner.GetComponent<Rigidbody>();
        m_Camera = m_FSM.Owner.GetComponentInChildren<Camera>();
        //rotationX = m_Player.Camera.transform.localEulerAngles.x;
    }
    public override void Update()
    {
        base.Update();
    }
    //public void CameraMovement(float rotationSpeed, float rotationXLimit, float fov, float transFov, float hideDistance, LayerMask layerMask, ItemChange items)
    //{
    //    if (!m_Camera.enabled) return;
    //    if (Cursor.lockState != CursorLockMode.Locked) return;

    //    m_Yaw = -m_Input.InFloor.Camera.ReadValue<Vector2>().y;
    //    m_Pitch = m_Input.InFloor.Camera.ReadValue<Vector2>().x;

    //    rotationX += m_Yaw * (rotationSpeed / 10);
    //    rotationX = Mathf.Clamp(rotationX, -rotationXLimit, rotationXLimit);
    //    m_Player.Camera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
    //    m_FSM.Owner.transform.rotation *= Quaternion.Euler(0, m_Pitch * rotationSpeed / 10, 0);

    //    m_Camera.fieldOfView = Mathf.Lerp(m_Camera.fieldOfView, fov, transFov * Time.deltaTime);

    //    if (m_WallDistance != Physics.Raycast(m_Camera.transform.position, m_FSM.Owner.transform.forward, out m_ObjectCheck, hideDistance, layerMask))
    //    {
    //        m_WallDistance = Physics.Raycast(m_Camera.transform.position, m_FSM.Owner.transform.forward, out m_ObjectCheck, hideDistance, layerMask);
    //        items.ani.SetBool("Hide", m_WallDistance);
    //        items.DefiniteHide = m_WallDistance;
    //    }
    //}
}