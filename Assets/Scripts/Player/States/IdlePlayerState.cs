using FiniteStateMachine;
using System;
using Unity.Burst.CompilerServices;
using UnityEngine;
using UnityEngine.InputSystem;

public class IdlePlayerState : State
{
    private NewPlayerController m_Player;
    private EnemyController m_Enemy;
    private InputActionAsset m_Input;

    public IdlePlayerState(FSM fsm, InputActionAsset input)
        : base(fsm)
    {
        m_Input = input;
    }

    public override void Init()
    {
        base.Init();
        m_Player = m_FSM.Owner.GetComponent<NewPlayerController>();
        m_Enemy = m_Player.enemyCollider.gameObject.GetComponentInParent<EnemyController>();
        m_Player.enemyCollider.radius = m_Enemy.SphereRadiusIdle;

        NewPlayerController.stopAudio?.Invoke();
        InteractWithObjects.canInteract?.Invoke();

        m_Input.FindAction("Shift").performed += StartShift;
        InteractWithObjects.hideCupboard += HideCupboard;
        m_Input.FindAction("Run").performed += StartRun;
    }
    public override void Exit()
    {
        base.Exit();
        m_Input.FindAction("Shift").performed -= StartShift;
        m_Input.FindAction("Run").performed -= StartRun;
        InteractWithObjects.hideCupboard -= HideCupboard;
        NewPlayerController.stopAudio?.Invoke();
    }
    public override void Update()
    {
        base.Update();
        m_Player.CameraMovement(m_Player.lookSpeed, m_Player.lookXLimit, m_Player.initFov, m_Player.speedToFOV, m_Player.hideDistance, m_Player.LayerWall, m_Player.inventory);
        m_Player.interactFurnitures(m_Player.lookSpeed,m_Player.LayerDrawer);
        if(m_Input.FindAction("Movement").ReadValue<Vector2>().x != 0 || m_Input.FindAction("Movement").ReadValue<Vector2>().y != 0) m_FSM.ChangeState<WalkingPlayerState>();
        if(m_Enemy.gameObject.activeInHierarchy)
        {
            if(m_Enemy.fms.GetState().GetType().ToString() != "StunnedState" && m_Player.IsNearEnemy()) m_FSM.ChangeState<AtackPlayerState>();
        }
        //m_Player.InteractFurnitures();
        m_Player.DisableFlashlight();
        m_Player.EnableUltraVioletLight();
    }
    private void StartShift(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        m_FSM.ChangeState<ShiftingPlayerState>();
    }
    private void StartRun(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        m_FSM.ChangeState<RunningPlayerState>();
    }
    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        if (collider.gameObject.CompareTag("Minigame"))
        {
            collider.gameObject.GetComponent<TriggersInteract>().StartMinigame();
            m_FSM.ChangeState<MinigameState>();
        }
        if (collider.gameObject.CompareTag("RoomTrigger")) collider.gameObject.GetComponent<TriggersInteract>().StartRoom();
        if (collider.gameObject.CompareTag("TriggerEnterHouse"))
        {
            m_Player.PlayerInsideHouse = collider.gameObject.GetComponent<EnterHouseTrigger>().TriggerEnter;
            if (m_Player.PlayerInsideHouse)
            {
                SQLController.Instance.SaveStatusRoomWithoutCheckStart("InsideHouse", 1);
                MenuController.savePositions.Invoke();
                NewPlayerController.setNewAudio?.Invoke(PlayerStatesSounds.RUNNING, m_Player.m_IsInsideHoue);
            }
            else SQLController.Instance.SaveStatusRoomWithoutCheckStart("InsideHouse", 0);

        }
    }
    private void HideCupboard(float n1, float n2)
    {
        m_Player.puzzleClamp[0] = n1;
        m_Player.puzzleClamp[1] = n2;

        m_FSM.ChangeState<MinigameState>();
    }
}