using FiniteStateMachine;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using static NewPlayerController;

public class MinigameState : State
{
    private NewPlayerController m_Player;
    private EnemyController m_Enemy;
    private InteractWithObjects m_InteractController;
    private Camera m_CameraMain;
    private InputActionAsset m_Input;
    private GameObject m_Flashlight;
    private GameObject m_Light;

    private float lookSpeed = 0.2f;
    private float rotationX = 0;
    private float rotationY = 0;
    private float m_Yaw;
    private float m_Pitch;
    public MinigameState(FSM fsm, InputActionAsset input)
        : base(fsm)
    {
        m_Input = input;
    }

    public override void Init()
    {
        base.Init();
        m_Player = m_FSM.Owner.GetComponent<NewPlayerController>();
        m_Enemy = m_Player.enemyCollider.gameObject.GetComponentInParent<EnemyController>();
        m_CameraMain = m_Player.cameraMain;
        m_Flashlight = m_Player.flashlight;
        m_Light = m_Player.minigameLight;
        m_InteractController = m_CameraMain.gameObject.GetComponent<InteractWithObjects>();
        EnableOrDisableObjects(true);

        m_Player.inventory.ani.SetBool("Hide", true);
        if(m_Player.inventory._itemActual != null) m_Player.inventory._itemActual.SetActive(false);

        m_Player.minigameCamera.transform.localEulerAngles = new Vector3(m_Player.minigameCamera.transform.localEulerAngles.x, 180, m_Player.minigameCamera.transform.localEulerAngles.z);
        rotationY = m_Player.minigameCamera.transform.localEulerAngles.y;

        NewPlayerController.minigameFinished += ExitState;
    }

    public override void Exit()
    {
        base.Exit();
        NewPlayerController.minigameFinished -= ExitState;
    }
    public override void Update()
    {
        base.Update();
        if(m_Input.FindAction("Escape").triggered)
        {
            ExitState();
        }
        if (m_Enemy.fms.GetState().GetType().ToString() != "StunnedState" && m_Player.IsNearEnemy()) m_FSM.ChangeState<AtackPlayerState>();
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }
    public override void LateUpdate()
    {
        if (m_Player.minigameCamera.enabled && !m_Player.CameraDisabled)
        {
            m_Yaw = -Input.GetAxis("Mouse Y");
            m_Pitch = Input.GetAxis("Mouse X");

            rotationX += m_Yaw * lookSpeed;
            rotationY += m_Pitch * lookSpeed;
            rotationX = Mathf.Clamp(rotationX, -5, 10);
            rotationY = Mathf.Clamp(rotationY, m_Player.puzzleClamp[0], m_Player.puzzleClamp[1]);
            m_Player.minigameCamera.transform.localRotation = Quaternion.Euler(rotationX, rotationY, 0);
        }
    }
    private void ExitState()
    {
        if (m_Player.cupboard != null)
        {
            m_Player.StartCoroutine(ExitCupboard());
            return;
        }
        EnableOrDisableObjects(false);
        m_Player.minigameCamera = null;
        m_FSM.ChangeState<IdlePlayerState>();
        GameManager.Instance.DisableAllMinigames();
    }
    IEnumerator ExitCupboard()
    {
        AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.CUPBOARD);
        m_Player.cupboard.GetComponentInParent<Animator>().Play("Unhide");
        yield return new WaitForSeconds(1.4f);
        m_Player.gameObject.transform.rotation = m_Player.cupboard.transform.rotation;
        m_Player.HideCamera = null;
        m_Player.cupboard = null;
        m_Player.Hide = false;

        EnableOrDisableObjects(false);
        m_Player.FSM.ChangeState<IdlePlayerState>();
    }
    private void EnableOrDisableObjects(bool status)
    {
        if (m_Light != null) m_Light.SetActive(status);
        m_CameraMain.enabled = !status;
        m_InteractController.enabled= !status;
        m_Player.minigameCamera.enabled = status;
        if (status)
        {
            m_Player.MinigameCameras.Cupboard = m_Player.HideCamera;
            m_Player.MinigameCameras.Minigame = m_Player.minigameCamera;
        }
        else
        {
            m_Player.MinigameCameras.Cupboard = null;
            m_Player.MinigameCameras.Minigame = null;
        }
        m_Player.PlayerHands.SetActive(!status);
    }

}