using FiniteStateMachine;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class AtackPlayerState : State
{

    private NewPlayerController _player;
    private UIController _uiController;
    private ItemStats _itemAtack;
    private Transform m_EnemyHead;
    private Transform m_CinematicCamera;
    private InputActionAsset m_Input;
    private bool _canAtack = false;

    public AtackPlayerState(FSM fSM, Transform cinematicCamera, Transform enemyTransform, InputActionAsset input) : base(fSM)
    {
        m_CinematicCamera = cinematicCamera;
        m_EnemyHead = enemyTransform;
        m_Input = input;
    }

    public override void Init()
    {
        base.Init();
        
        _player = m_FSM.Owner.GetComponent<NewPlayerController>();
        _uiController = _player._uiController;
        NewPlayerController.stopAudio?.Invoke();
        _player.StartCoroutine(LookAtEnemy());

        if (ComproveItemAtack())
        {
            InvokeSetVelocityDelegate("RunSlow", true);
            NewPlayerController.onAtackInteraction?.Invoke();
            _canAtack = true;
        }
    }

    public override void Exit()
    {
        base.Exit();
        _player.cameraHolder.transform.rotation = new Quaternion(0, 0, 0, 0);
        _uiController.DisableSystemAtack();
    }

    public override void Update() 
    {
        base.Update();
        if(_canAtack == true) 
            UseItemAtack();
    }

    private bool ComproveItemAtack()
    {
        _itemAtack = _player.inventory.ListItemsGet().Where(i => i._nameItem == "Syringe").FirstOrDefault();
        Debug.Log(_itemAtack);

        if (_itemAtack._enable)
        {
            _uiController.InitializeAtackBar();
            return true;
        }
        else
        {
            m_FSM.ChangeState<DeadState>();
            return false;
        }
    }

    private void UseItemAtack()
    {
        if(m_Input.FindAction("Interact").triggered)
        {
            _player.inventory.SetActualItem(_itemAtack);
            _player.inventory.DropInventoryItem();
            if (_uiController.CheckAtackBar())
            {
                _player.StartCoroutine(SoundOffset());
                NewPlayerController.onStunEnemy?.Invoke();
                _player.SetCameraCinematic("SyringeCinematic", true);
                _player.StartCoroutine(WaitForCinematic(3f));
                _player.StartCoroutine(LookAtCamera());
            }
            else
            {
                m_FSM.ChangeState<DeadState>();
            }
            InvokeSetVelocityDelegate("Run", false);
            _uiController.DisableSystemAtack();
            NewPlayerController.onAtackInteraction?.Invoke();
        }
    }


    private void InvokeSetVelocityDelegate(string animation, bool state)
    {
        NewPlayerController.onSetEnemyVelocity?.Invoke(animation, state);
    }
    IEnumerator WaitForCinematic(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        _player.SetCameraCinematic("-", false);
        NewPlayerController.onAtackInteraction?.Invoke();
        m_FSM.ChangeState<IdlePlayerState>();
        _canAtack = false;
    }
    IEnumerator SoundOffset()
    {
        yield return new WaitForSeconds(1.55f);
        AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.SYRINGE);
    }
    IEnumerator LookAtCamera()
    {
        while (_canAtack)
        {
            Vector3 direction = m_EnemyHead.position - m_CinematicCamera.position;
            Quaternion targetRotation = Quaternion.LookRotation(direction);
            m_CinematicCamera.transform.rotation = Quaternion.Lerp(m_CinematicCamera.transform.rotation, targetRotation, Time.deltaTime * 5f);
            yield return new WaitForSeconds(.1f);
        }
    }    
    IEnumerator LookAtEnemy()
    {
        Vector3 direction = m_EnemyHead.position - _player.cameraHolder.transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        while (Quaternion.Angle(_player.cameraHolder.transform.rotation, targetRotation) > 0.1f)
        {
            _player.cameraHolder.transform.rotation = Quaternion.Slerp(_player.cameraHolder.transform.rotation, targetRotation, Time.deltaTime * 5f); 
            yield return new WaitForFixedUpdate();
        }
    }
}
