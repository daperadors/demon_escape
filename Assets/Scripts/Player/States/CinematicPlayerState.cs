using FiniteStateMachine;
using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;

public class CinematicPlayerState : State
{
    private Quaternion initialRotation;
    private Quaternion targetRotation;
    private Quaternion finalRotation;
    private Camera m_Camera;
    private NewPlayerController m_Player;
    private PlayerExtras m_PlayerExtras;
    private Transform m_TargetLook;
    private Transform m_TargetCinematic;
    private bool rotateBack = false;
    Coroutine m_cinematicCameraCoroutine;
    Coroutine m_cinematicLookAtCoroutine;

    public CinematicPlayerState(FSM fsm, Camera cameraPlayer)
        : base(fsm)
    {
        m_Camera = cameraPlayer;
    }

    public override void Init()
    {
        base.Init();
        m_Player = m_FSM.Owner.GetComponent<NewPlayerController>();
        m_PlayerExtras = m_FSM.Owner.GetComponent<PlayerExtras>();
        initialRotation = m_Camera.transform.rotation;
        m_cinematicLookAtCoroutine = m_Player.StartCoroutine(m_Player.InitialCinematicAction(m_TargetCinematic));
        NewPlayerController.playerFinishFollowTarget += InitializeCameraMovement;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void Update()
    {
        base.Update();
    }
    
    private void InitializeCameraMovement()
    {
        m_Player.StopCoroutine(m_cinematicLookAtCoroutine);

        if(m_PlayerExtras.cameraType == CameraTypes.CAMERA_MOVEMENT)
        {
            m_cinematicCameraCoroutine = m_Player.StartCoroutine(m_Player.CameraToTargetCinematic(initialRotation));

        }else
        {
            m_Player.StartCoroutine(m_Player.NoCameraTargetCinematic(initialRotation));
        }
    }
}
