using FiniteStateMachine;
using System.Collections;
using UnityEngine;

public class LookAtState : State
{
    private Quaternion initialRotation;
    private Quaternion targetRotation;
    private Quaternion finalRotation;
    private Camera m_Camera;
    private NewPlayerController m_Player;
    private Transform m_Target;
    private bool rotateBack = false;

    public LookAtState(FSM fsm, Camera cameraPlayer)
        : base(fsm)
    {
        //initialRotation = initialPosition;
        m_Camera = cameraPlayer;
    }

    public override void Init()
    {
        base.Init();
        m_Player = m_FSM.Owner.GetComponent<NewPlayerController>();
        initialRotation = m_Camera.transform.rotation;
        NewPlayerController.stopAudio?.Invoke();
        m_Player.StartCoroutine(m_Player.CameraToTargetCinematic(initialRotation));
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void Update()
    {
        base.Update();
        //CameraToTarget();
    }

    private void CameraToTarget()
    {
        if (!rotateBack)
        {
            targetRotation = Quaternion.LookRotation(m_Player.playerExtraConfig._targetCameraMovement.position - m_Camera.transform.position);

            m_Camera.transform.rotation = Quaternion.Slerp(m_Camera.transform.rotation, targetRotation, Time.deltaTime * 2);
            if (Quaternion.Angle(m_Camera.transform.rotation, targetRotation) < 0.1f)
            {
                rotateBack = true;
            }
        }

        else 
        {
            if(m_Player.playerExtraConfig._returnCamera == true)
            {
                m_Camera.transform.rotation = Quaternion.Slerp(m_Camera.transform.rotation, initialRotation, Time.deltaTime * 2);
                if (Quaternion.Angle(m_Camera.transform.rotation, initialRotation) < 0.1f)
                {
                    m_FSM.ChangeState<IdlePlayerState>();
                }
            }
            else
            {
                m_FSM.ChangeState<IdlePlayerState>();
            }
        }
    }

}
