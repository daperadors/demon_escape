using FiniteStateMachine;
using UnityEngine;
using UnityEngine.InputSystem;
using static MenuController;

public class RunningPlayerState : State
{
    private NewPlayerController m_Player;
    private EnemyController m_Enemy;
    private Rigidbody m_Rigidbody;
    private InputActionAsset m_Input;
    private Vector3 m_MoveDirection;
    public RunningPlayerState(FSM fsm, InputActionAsset input)
        : base(fsm)
    {
        m_Input = input;
    }

    public override void Init()
    {
        base.Init();
        m_Player = m_FSM.Owner.GetComponent<NewPlayerController>();
        m_Rigidbody = m_FSM.Owner.GetComponent<Rigidbody>();

        m_Enemy = m_Player.enemyCollider.gameObject.GetComponentInParent<EnemyController>();
        m_Player.enemyCollider.radius = m_Enemy.SphereRadiusRunning;

        NewPlayerController.setNewAudio?.Invoke(PlayerStatesSounds.RUNNING, !m_Player.m_IsInsideHoue);

        m_Input.FindAction("Run").canceled += CancelRun;
        m_Input.FindAction("Shift").performed += StartShift;

        NewPlayerController.insideHouse += PlayerEnterHouse;
    }
    public override void Exit()
    {
        base.Exit();
        m_Input.FindAction("Run").canceled -= CancelRun;
        m_Input.FindAction("Shift").performed -= StartShift;
        NewPlayerController.stopAudio?.Invoke();
        NewPlayerController.insideHouse -= PlayerEnterHouse;
    }
    public override void Update()
    {
        base.Update();
        m_Player.CameraMovement(m_Player.lookSpeed, m_Player.lookXLimit, m_Player.runningFOV, m_Player.speedToFOV, m_Player.hideDistance, m_Player.LayerWall, m_Player.inventory);
        if (m_Enemy.gameObject.activeInHierarchy)
        {
            if (m_Enemy.fms.GetState().GetType().ToString() != "StunnedState" && m_Player.IsNearEnemy()) m_FSM.ChangeState<AtackPlayerState>();
        }
        m_Player.DisableFlashlight();
        m_Player.EnableUltraVioletLight();
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        m_MoveDirection = m_Input.FindAction("Movement").ReadValue<Vector2>();
        if (m_MoveDirection.x != 0f || m_MoveDirection.y != 0f)
        {
            m_Player.horizontal = m_MoveDirection.y;
            m_Player.vertical = m_MoveDirection.x;
            Vector3 direction = (m_Player.transform.right * m_MoveDirection.x + m_Player.transform.forward * m_MoveDirection.y).normalized * m_Player.runningSpeed;
            m_Rigidbody.velocity = new Vector3(direction.x, m_Rigidbody.velocity.y, direction.z);
        }
        else m_FSM.ChangeState<IdlePlayerState>();
    }
    private void CancelRun(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        m_FSM.ChangeState<WalkingPlayerState>();
    }
    private void StartShift(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        m_FSM.ChangeState<ShiftingPlayerState>();
    }
    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        if (collider.gameObject.CompareTag("Minigame"))
        {
            collider.gameObject.GetComponent<TriggersInteract>().StartMinigame();
            m_FSM.ChangeState<MinigameState>();
        }
        if (collider.gameObject.CompareTag("RoomTrigger"))
        {
            collider.gameObject.GetComponent<TriggersInteract>().StartRoom();
            NewPlayerController.setNewAudio?.Invoke(PlayerStatesSounds.RUNNING, !m_Player.m_IsInsideHoue);
        }
        if (collider.gameObject.CompareTag("TriggerEnterHouse"))
        {
            m_Player.PlayerInsideHouse = collider.gameObject.GetComponent<EnterHouseTrigger>().TriggerEnter;
            if (m_Player.PlayerInsideHouse)
            {
                SQLController.Instance.SaveStatusRoomWithoutCheckStart("InsideHouse", 1);
                MenuController.savePositions.Invoke();
                NewPlayerController.setNewAudio?.Invoke(PlayerStatesSounds.RUNNING, m_Player.m_IsInsideHoue);
            }
            else SQLController.Instance.SaveStatusRoomWithoutCheckStart("InsideHouse", 0);
            
        }
    }
    private void PlayerEnterHouse()
    {
        NewPlayerController.setNewAudio?.Invoke(PlayerStatesSounds.RUNNING, !m_Player.m_IsInsideHoue);
    }
}