﻿using FiniteStateMachine;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class DeadState : State
{
    private Transform m_EnemyHead;
    private Transform m_Camera;
    public DeadState(FSM fSM, Transform enemyTransform, Transform camera) : base(fSM)
    {
        m_EnemyHead = enemyTransform;
        m_Camera = camera;
    }

    public override void Init()
    {
        base.Init();
        NewPlayerController.playerDead.Invoke();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void Update()
    {
        base.Update();
        LookAtEnemy();
    }

    private void LookAtEnemy()
    {
        Vector3 direction = m_EnemyHead.position - m_Camera.position;
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        m_Camera.transform.rotation = Quaternion.Lerp(m_Camera.transform.rotation, targetRotation, Time.deltaTime * 5f);
    }
    
}
