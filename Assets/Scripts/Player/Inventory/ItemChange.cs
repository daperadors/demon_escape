using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class ItemChange : MonoBehaviour
{
    [Header("Item Change")]
    [SerializeField] public Animator ani;
    //[SerializeField] bool LoopItems = true;
    [SerializeField, Tooltip("You can add your new item here.")] GameObject[] Items;
    [SerializeField] private static List<GameObject> _auxiliarItems;
    [SerializeField] private string[] _excludedItems;
    [SerializeField] int ItemIdInt = 0;
    int MaxItems;
    int ChangeItemInt;

    public GameObject _itemActual;
    public ItemStats _itemActualStats;
    private NewPlayerController _player;
    private InputActionAsset m_Input;

    [SerializeField] private ItemsList _listItems;
    private int _itemsIterator = 0;

    [HideInInspector] public bool DefiniteHide;

    private bool _activeItemChangeCorutine;
    private bool _canDropItem = true;
    private string[] normalStates = { "IdlePlayerState", "WalkingPlayerState", "RunningPlayerState", "ShiftingPlayerState" };
    public delegate void ItemChaged(Sprite sprite);
    public static ItemChaged itemChaged;
    public static List<GameObject> AuxiliarItems => _auxiliarItems;
    private void Awake()
    {
        MenuController.continueGame += GetAndSetItemsInInventory;
    }
    private void Start()
    {
        ObjectInteractionVoice.onSetVoiceExecute?.Invoke(_listItems);
        _auxiliarItems = new List<GameObject>();
        _player = GetComponentInParent<NewPlayerController>();
        m_Input = NewPlayerController.InputM;
        InsertDataObjectsIntoDB();
        if (ani == null && GetComponent<Animator>()) ani = GetComponent<Animator>();
        DefiniteHide = false;
        MaxItems = 4;
    }
    private void Update()
    {

        ChangeItemMouseController();
        ChangeItemNumbersController();

        if (m_Input.FindAction("HideItem").triggered)
        {
            if (ani.GetBool("Hide")) Hide(false);
            else Hide(true);
        }
    }
    private void InsertDataObjectsIntoDB()
    {
        foreach (GameObject gameObject in Items)
        {
            SQLController.Instance.SaveInventoryStart(gameObject.GetComponent<ObjectsId>().id, gameObject.name);
        }
    }
    private void GetAndSetItemsInInventory()
    {
        if(_auxiliarItems.Count > 0) return;
        _itemsIterator = 0;
        List<int> ids = SQLController.Instance.GetAllItemsInInventory();
        foreach (int id in ids)
        {
            GameObject item = Items.Where(i => i.GetComponent<ObjectsId>().id == id).FirstOrDefault();
            if(item != null)
            {
                if (item.gameObject.tag == "Key") continue;
                ItemStats itemStats = _listItems._itemsList.Where(i => i._nameItem == item.tag).FirstOrDefault();
                int idObjectReference = SQLController.Instance.GetObjectReferenceInItemInventory(id);
                ObjectsExtraId objectReferenceID = ObjectsExtraController.Objects.Where(o => o.id == idObjectReference).FirstOrDefault();
                if (objectReferenceID == null) continue;
                GameObject objectReference = objectReferenceID.gameObject;
                if (itemStats != null)
                {
                    _itemActual = item;
                    _itemActualStats = itemStats;
                    itemStats._enable = true;
                    ItemIdInt = _itemsIterator;
                    _itemsIterator++;
                    _auxiliarItems.Add(objectReference);
                    objectReference.SetActive(false);
                    itemChaged?.Invoke(_itemActualStats.image);
                }
            }
        }
    }
    public void Hide(bool Hide)
    {
        DefiniteHide = Hide;
        ani.SetBool("Hide", Hide);
    }
    private bool PlayerInMinigame()
    {
        return System.Array.IndexOf(normalStates, _player.FSM.GetState().GetType().ToString()) == -1 ? true : false;
    }
    private IEnumerator IEItemChange()
    {

        if (!DefiniteHide) ani.SetBool("Hide", true);

        yield return new WaitForSeconds(0.3f);

        ActiveItem();

        if (!DefiniteHide) ani.SetBool("Hide", false);
    }

    public void InventoryAddObject(string nameArm, GameObject obj)
    {

        ItemStats item = _listItems._itemsList.FirstOrDefault(item => item._nameItem == nameArm);

        if (item != null)
        {
            if (item._enable == false && _itemsIterator < MaxItems)
            {
                //Disable item in floor
                obj.SetActive(false);

                // Put the picked item as current item
                if (_itemActual != null) { 
                    _itemActual.SetActive(false);
                }

                // Add object into auxiliar list
                _auxiliarItems.Add(obj);
                //Active item taken
                ActiveObjectOnList(obj);
                item._enable = true;

                ItemIdInt = _itemsIterator;

                // Increment items quantity
                _itemsIterator++;
                itemChaged?.Invoke(_itemActualStats.image);
                SQLController.Instance.SaveItemInventoryStatus(_itemActual.GetComponent<ObjectsId>().id, 1);
                ObjectsExtraId objID = obj.GetComponent<ObjectsExtraId>();
                SQLController.Instance.ChangeVisibilityExtraObject(objID.id, false);
                SQLController.Instance.SetObjectReferenceIntoItemInventory(_itemActual.GetComponent<ObjectsId>().id, objID.id);
            }
            else
            {
                Debug.Log(_itemsIterator);
                VoiceManager.onPlayVoice.Invoke(Voices.FULL_INVENTORY);
            }
        }
    }
    public int getItemsInInvetary()
    {
        return _auxiliarItems.Count;
    }

    private void SetNewItemIntoCanvas(GameObject obj)
    {
        _itemActualStats = _listItems._itemsList.FirstOrDefault(item => item._nameItem == obj.tag);
        itemChaged?.Invoke(_itemActualStats.image);
    }

    public bool ItemIsActive(GameObject objectItem)
    {
        string tag = objectItem.tag;
        ItemStats item = _listItems._itemsList.First(item => item._nameItem == tag);

        if (item._enable)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public void HideItemHand(bool status)
    {
        if (_itemActual && System.Array.IndexOf(_excludedItems, _itemActual.tag) == -1)
        {
            ani.SetBool("Hide", status);
            DefiniteHide = status;
        }
    }
    private void ActiveObjectOnList(GameObject obj)
    {
        for (int i = 0; i < Items.Length; i++)
        {
            if (Items[i].tag == obj.tag)
            {
                _itemActual = Items[i];
                if (!PlayerInMinigame()) Items[i].SetActive(true);
                SetNewItemIntoCanvas(obj);

            }
        }
    }
    public bool GetItemByNameStatus(string name)
    {
        return _auxiliarItems.Where(o => o.tag == name).FirstOrDefault() != null;
    }
    private void ActiveItem()
    {
        for (int i = 0; i < Items.Length; i++)
        {
            Items[i].SetActive(false);
        }
        if (ItemIsActive(_auxiliarItems[ItemIdInt]))
        {
            ActiveObjectOnList(_auxiliarItems[ItemIdInt]);
        }
        else
        {
            ActiveObjectOnList(_itemActual);
        }

    }

    private void ChangeItemNumbersController()
    {
        if (Input.GetKeyDown("1"))
        {
            if (_auxiliarItems.Count > 0)
            {
                if (_auxiliarItems.Count > 0 && _auxiliarItems[0] != null)
                {
                    ItemIdInt = 0;
                    StartCoroutine(IEItemChange());
                }
            }
        }
        if (Input.GetKeyDown("2"))
        {
            if (_auxiliarItems.Count > 1)
            {
                if (_auxiliarItems.Count > 0 && _auxiliarItems[1] != null)
                {
                    ItemIdInt = 1;
                    StartCoroutine(IEItemChange());
                }
            }
        }
        if (Input.GetKeyDown("3"))
        {
            if (_auxiliarItems.Count > 2)
            {
                if (_auxiliarItems.Count > 0 && _auxiliarItems[2] != null)
                {
                    ItemIdInt = 2;
                    StartCoroutine(IEItemChange());
                }
            }
        }
        if (Input.GetKeyDown("4"))
        {
            if (_auxiliarItems.Count > 3)
            {
                if (_auxiliarItems.Count > 0 && _auxiliarItems[3] != null)
                {
                    ItemIdInt = 0;
                    StartCoroutine(IEItemChange());
                }
            }
        }

    }

    private void ChangeItemMouseController()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (ItemIdInt < MaxItems && ItemIdInt < _auxiliarItems.Count - 1)
            {
                ItemIdInt++;
                StartCoroutine(IEItemChange());
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (ItemIdInt > 0)
            {
                ItemIdInt--;
                StartCoroutine(IEItemChange());
            }
        }
    }
    public void DropInventoryItem()
    {
        if (_canDropItem && _itemActual != null)
        {
            ItemStats item = _listItems._itemsList.FirstOrDefault(item => item._nameItem == _itemActual.tag);
            item._enable = false;
            _itemActual.SetActive(false);
            SQLController.Instance.DeleteItemInInventory(_itemActual.GetComponent<ObjectsId>().id);
            SQLController.Instance.DeleteObjectReference(_itemActual.GetComponent<ObjectsId>().id);
            GameObject obj = _auxiliarItems.Where(x => x.tag == _itemActual.tag).FirstOrDefault();
            ObjectsId objectsId = null;
            if(!obj.TryGetComponent<ObjectsId>(out objectsId))
                _auxiliarItems.Remove(obj);
            //Destroy(obj);
            obj.SetActive(false);
            SQLController.Instance.ChangeVisibilityExtraObject(obj.GetComponent<ObjectsExtraId>().id, false);
            _itemsIterator--;
            if (_auxiliarItems.Count > 0)
            {
                _itemActual = _auxiliarItems[0];
                _itemActualStats = _listItems._itemsList[0];
                ItemStats itemPicked = _listItems._itemsList.FirstOrDefault(item => item._nameItem == _auxiliarItems[0].tag);
                ItemIdInt = 0;
                StartCoroutine(IEItemChange());
                ItemChange.itemChaged?.Invoke(itemPicked.image);
            }
            else
            {
                _itemActual = null;
                _itemActualStats = null;
                ItemChange.itemChaged?.Invoke(null);
            }
            StartCoroutine(WaitForDrop());
        }
    }

    public void EnableOrDisableItem(ItemStats item, bool status)
    {
        item._enable = status;
        GameObject g = Items.Where(x => x.tag == item.name).FirstOrDefault();
        g.SetActive(false);
    }

    public void DropItem()
    {

        if (_canDropItem && _itemActual != null)
        {
            StartCoroutine(DropItemTimeOfset());
            ItemStats item = _listItems._itemsList.First(item => item._nameItem == _itemActual.tag);

            item._enable = false;
            _itemActual.SetActive(false);
            //Remove droped item in _auxiliarItems list
            GameObject obj = _auxiliarItems.Where(x => x.tag == _itemActual.tag).FirstOrDefault();
            ObjectsId objectsId = null;
            if (!obj.TryGetComponent<ObjectsId>(out objectsId))
                _auxiliarItems.Remove(obj);
            SQLController.Instance.DeleteItemInInventory(_itemActual.GetComponent<ObjectsId>().id);
            SQLController.Instance.DeleteObjectReference(_itemActual.GetComponent<ObjectsId>().id);
            //Destroy(obj);
            // Instanitate droped item
            InstantiateObject(obj);

            if (_auxiliarItems.Count > 0)
            {
                _itemActual = _auxiliarItems[0];
                ItemStats itemPicked = _listItems._itemsList.FirstOrDefault(item => item._nameItem == _auxiliarItems[0].tag);
                if (_itemsIterator > 0) ItemIdInt = 0; StartCoroutine(IEItemChange());
                ItemChange.itemChaged?.Invoke(itemPicked.image);
            }
            else
            {
                _itemActual = null;
                ItemChange.itemChaged?.Invoke(null);
            }
            StartCoroutine(WaitForDrop());
        }
    }

    IEnumerator DropItemTimeOfset()
    {
        yield return new WaitForSeconds(0.15f);
        AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.DROP);
    }

    private void InstantiateObject(GameObject dropedItem)
    {
        Vector3 newPosition = _itemActual.transform.position;
        dropedItem.transform.position = newPosition;
        dropedItem.transform.rotation = Quaternion.identity;

        if (dropedItem.gameObject.tag.Length > 5 && dropedItem.gameObject.tag.Substring(0, 5) == "Piece")
        {
            if (dropedItem.transform.childCount > 0)
            {
                GameObject child = dropedItem.transform.GetChild(0).gameObject;
                child.SetActive(false);
            }
        }
        Animator animator;
        dropedItem.TryGetComponent<Animator>(out animator);
        if (animator != null)
            Destroy(animator);

        MonoBehaviour script;
        dropedItem.TryGetComponent<MonoBehaviour>(out script);
        if (script != null)
        {
            MonoBehaviour[] components = dropedItem.GetComponents<MonoBehaviour>();
            foreach(MonoBehaviour component in components)
            {
                if(component.GetType().ToString() != "ObjectsExtraId")
                {
                    Destroy(component);
                }
            }
        }
            

        dropedItem.gameObject.SetActive(true);

        Rigidbody rigidbody;
        dropedItem.TryGetComponent<Rigidbody>(out rigidbody);
        if (rigidbody == null)
            dropedItem.AddComponent<Rigidbody>();
        dropedItem.GetComponent<BoxCollider>().isTrigger = false;
        Rigidbody rigidbodyObject = dropedItem.GetComponent<Rigidbody>();
        rigidbodyObject.mass = 15f;
        Vector3 direccion = _player.transform.forward;
        rigidbodyObject.AddForce(direccion.normalized * rigidbodyObject.mass, ForceMode.Impulse);
        _itemsIterator--;
    }
    public void EnableItem(ItemStats item, bool enable)
    {
        item._enable = enable;
    }

    public void SetActualItem(ItemStats item)
    {
        _itemActual = Items.Where(i => i.tag == item._nameItem).FirstOrDefault();
    }

    public List<ItemStats> ListItemsGet()
    {
        return _listItems._itemsList;
    }

    IEnumerator WaitForDrop()
    {
        _canDropItem = false;
        yield return new WaitForSeconds(.5f);
        _canDropItem = true;
    }
}