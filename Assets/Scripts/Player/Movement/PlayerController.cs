﻿//by EvolveGames
using System;
using UnityEngine;
using UnityEngine.InputSystem;
public class PlayerController : MonoBehaviour
{
    [Header("PlayerController")]
    [SerializeField] public Transform Camera;
    [SerializeField] public ItemChange Items;
    [SerializeField] public LayerMask m_LayerGround;
    [SerializeField, Range(1, 10)] float walkingSpeed = 3.0f;
    [Range(0.1f, 5)] public float CroughSpeed = 1.0f;
    [SerializeField, Range(2, 20)] float RunningSpeed = 4.0f;
    [SerializeField, Range(0.5f, 10)] float lookSpeed = 2.0f;
    [SerializeField, Range(10, 120)] float lookXLimit = 80.0f;
    [Space(20)]
    [Header("Advance")]
    [SerializeField] float RunningFOV = 65.0f;
    [SerializeField] float SpeedToFOV = 4.0f;
    [SerializeField] float timeToRunning = 2.0f;
    [HideInInspector] public bool canMove = true;
    [HideInInspector] public bool CanRunning = true;
    [SerializeField] private Camera SafeCamera;

    [Space(20)]
    [Header("Hands Hide")]
    [SerializeField] bool CanHideDistanceWall = true;
    [SerializeField, Range(0.1f, 5)] float HideDistance = 1.5f;
    [SerializeField] int LayerMaskInt = 1;

    [Space(20)]
    [Header("Input")]
    private InputManager m_Input;

    [HideInInspector] public Vector3 moveDirection = Vector3.zero;
    float rotationX = 0;
    [HideInInspector] public bool isRunning = false;
    [HideInInspector] public bool isShifting = false;
    Vector3 InstallCameraMovement;
    float InstallFOV;
    Camera cam;
    [HideInInspector] public bool Moving;
    [HideInInspector] public float vertical;
    [HideInInspector] public float horizontal;
    [HideInInspector] public float m_Yaw;
    [HideInInspector] public float m_Pitch;
    float RunningValue;
    bool WallDistance;
    [HideInInspector] public float WalkingValue;
    private Rigidbody m_Rigidbody;

    [Space(20)]
    [Header("Enemy")]
    [SerializeField] private SphereCollider m_EnemyCollider;
    [SerializeField] private EnemyController m_Enemy;

    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        if (Items == null && GetComponent<ItemChange>()) Items = GetComponent<ItemChange>();
        cam = GetComponentInChildren<Camera>();
        InstallCameraMovement = Camera.localPosition;
        InstallFOV = cam.fieldOfView;
        RunningValue = RunningSpeed;
        WalkingValue = walkingSpeed;


        //m_Input = new InputManager();
        m_Input.InFloor.Enable();
        m_Input.InFloor.Shift.performed += IsShifting;
        m_Input.InFloor.Shift.canceled += CancelShifting;
        m_Input.InFloor.Run.performed += IsRunning;
        m_Input.InFloor.Run.canceled += CancelRunning;
    }

    void Update()
    {

        moveDirection = m_Input.InFloor.Movement.ReadValue<Vector2>();
        RaycastHit ObjectCheck;

        Moving = horizontal < 0 || vertical < 0 || horizontal > 0 || vertical > 0 ? true : false;
        //isRunning = CanRunning ? Input.GetKey(KeyCode.LeftShift) : false;
        isShifting = Input.GetKey(KeyCode.LeftControl);
        vertical = canMove ? (isRunning ? RunningValue : WalkingValue) * moveDirection.x : 0;
        horizontal = canMove ? (isRunning ? RunningValue : WalkingValue) * moveDirection.y : 0;
        if (isRunning) RunningValue = Mathf.Lerp(RunningValue, RunningSpeed, timeToRunning * Time.deltaTime);
        else RunningValue = WalkingValue;

        Vector3 direccion = (transform.right * moveDirection.x + transform.forward * moveDirection.y).normalized * (!isRunning ? walkingSpeed : RunningValue);
        m_Rigidbody.velocity = new Vector3(direccion.x, m_Rigidbody.velocity.y, direccion.z);

        if (cam.enabled)
        {
            if (Cursor.lockState == CursorLockMode.Locked && canMove)
            {
                m_Yaw = -Input.GetAxis("Mouse Y");
                m_Pitch = Input.GetAxis("Mouse X");

                rotationX += m_Yaw * lookSpeed;
                rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
                Camera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
                transform.rotation *= Quaternion.Euler(0, m_Pitch * lookSpeed, 0);

                if (isRunning && Moving) cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, RunningFOV, SpeedToFOV * Time.deltaTime);
                else cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, InstallFOV, SpeedToFOV * Time.deltaTime);
            }

            if (WallDistance != Physics.Raycast(GetComponentInChildren<Camera>().transform.position, transform.TransformDirection(Vector3.forward), out ObjectCheck, HideDistance, LayerMaskInt) && CanHideDistanceWall)
            {
                WallDistance = Physics.Raycast(GetComponentInChildren<Camera>().transform.position, transform.TransformDirection(Vector3.forward), out ObjectCheck, HideDistance, LayerMaskInt);
                Items.ani.SetBool("Hide", WallDistance);
                Items.DefiniteHide = WallDistance;
            }
        }
        ChangeRadiusEnemy();
    }
    private void IsShifting(InputAction.CallbackContext obj)
    {
        if (isRunning) return;
    }
    private void CancelShifting(InputAction.CallbackContext obj)
    {
    }
    private void IsRunning(InputAction.CallbackContext obj)
    {
        isRunning = true;
    }
    private void CancelRunning(InputAction.CallbackContext obj)
    {
        isRunning = false;
    }
    public RaycastHit CheckGround()
    {
        RaycastHit info;
        Physics.Raycast(transform.position, Vector3.down, out info, GetComponent<CapsuleCollider>().bounds.extents.y * 1.3f, m_LayerGround);
        return info;
    }
    public bool isGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, GetComponent<CapsuleCollider>().bounds.extents.y * 1.3f, m_LayerGround); ;
    }

    private void ChangeRadiusEnemy()
    {
        if (Moving)
        {
            if (isRunning) m_EnemyCollider.radius = m_Enemy.SphereRadiusRunning;
            else if (isShifting) m_EnemyCollider.radius = m_Enemy.SphereRadiusShift;
            else m_EnemyCollider.radius = m_Enemy.SphereRadiusWalking;
        }
        else m_EnemyCollider.radius = m_Enemy.SphereRadiusIdle;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Safe")
        {
            SafeCamera.enabled = true;
            cam.enabled = false;
        }
    }
    private void OnTriggerStay(Collider other)
    {

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Safe")
        {
            cam.enabled = true;
            SafeCamera.enabled = false;
        }
    }

}
