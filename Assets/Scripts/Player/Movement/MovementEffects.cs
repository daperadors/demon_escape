﻿using UnityEngine;

public class MovementEffects : MonoBehaviour
{
    [Header("MOVEMENT FX")]
    [SerializeField, Range(0.05f, 2)] float RotationAmount = 0.2f;
    [SerializeField, Range(1f, 20)] float RotationSmooth = 6f;
    [Header("Movement")]
    [SerializeField] bool CanMovementFX = true;
    [SerializeField, Range(0.1f, 2)] float MovementAmount = 0.5f;

    Quaternion InstallRotation;
    Vector3 MovementVector;
    Rigidbody m_PlayerRigidBody;
    NewPlayerController Player;
    private void Start()
    {
        m_PlayerRigidBody = GetComponentInParent<Rigidbody>();
        Player = GetComponentInParent<NewPlayerController>();
        InstallRotation = transform.localRotation;
    }

    private void Update()
    {
        float movementX = (Player.vertical * RotationAmount);
        float movementZ = (-Player.horizontal * RotationAmount);
        MovementVector = new Vector3(CanMovementFX ? movementX + m_PlayerRigidBody.velocity.y * MovementAmount : movementX, 0, movementZ);
        transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(MovementVector + InstallRotation.eulerAngles), Time.deltaTime * RotationSmooth);
    }
}
