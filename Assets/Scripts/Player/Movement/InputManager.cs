//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.3.0
//     from Assets/Scripts/Player/Movement/InputManager.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @InputManager : IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputManager()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputManager"",
    ""maps"": [
        {
            ""name"": ""InFloor"",
            ""id"": ""f0081060-6638-4ea2-bc41-f5ee4268d6f0"",
            ""actions"": [
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""d2fe2a55-22f7-4070-b162-c5fb4d072b1e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Camera"",
                    ""type"": ""Value"",
                    ""id"": ""29d25497-29bb-43b5-88af-b94d4c5339f9"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""Movement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""88b4178e-c50d-4607-a831-24d817b48285"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Run"",
                    ""type"": ""Button"",
                    ""id"": ""81f5a15e-1ca6-44e5-9a17-10cc87cce0d1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Shift"",
                    ""type"": ""Button"",
                    ""id"": ""0313444d-4acf-4a17-a2b6-75772827508e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""6c2b3ee3-3124-4455-978f-aaf4df491f11"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Escape"",
                    ""type"": ""Button"",
                    ""id"": ""86cd75d0-9d16-4cb8-9660-47e767dd42bc"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""DropItem"",
                    ""type"": ""Button"",
                    ""id"": ""b9392e78-0983-4d70-9a25-d3163754eb2c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""FlashlightUltraviolet"",
                    ""type"": ""Button"",
                    ""id"": ""43fff163-8e0a-4e5a-8fda-ad7efbbcd567"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""HideItem"",
                    ""type"": ""Button"",
                    ""id"": ""9a97901f-fea4-45c0-abb1-9e4d7c1c0ca8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Flashlight"",
                    ""type"": ""Button"",
                    ""id"": ""48a9ae14-1e41-4b86-b100-d71421f57062"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""PauseMenu"",
                    ""type"": ""Button"",
                    ""id"": ""4f9c9f59-65a2-4a7d-9783-dc4ea3da3538"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""552ab6f1-fe61-4ad8-b3d3-2276ffdb4589"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Player"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""69896efc-bc42-46db-b2a3-cbdc41279622"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Player"",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""0f8436c3-0926-45db-b8d4-5d522da94418"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": ""NormalizeVector2"",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""35e816fe-3b62-48ac-bc7f-3ce5d28c0ac7"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Player"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""d0469077-b11a-48e1-88dd-df1b66a386b1"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Player"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""da27a111-cd6f-4439-98c6-7009151c56c6"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Player"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""bb28536c-1107-4661-9b54-ba03f5858f7e"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Player"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""4cdc9d18-9947-4713-9827-35b59603fb2a"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Player"",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fdf0b3f9-04d2-4a61-b532-1f030ea8778a"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Player"",
                    ""action"": ""Shift"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dc7129be-be77-4414-9349-2e52103ddb52"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d81a6295-508d-446a-89b0-ddda356aa0b8"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Escape"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1839d0bf-6148-411e-89d7-f5b4b01f1a7a"",
                    ""path"": ""<Keyboard>/g"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DropItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""75a74cfd-a52a-4d1e-b0e0-40cea5ba09d6"",
                    ""path"": ""<Keyboard>/b"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FlashlightUltraviolet"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cb2ef3ac-15ee-442a-8809-bac990eabcaa"",
                    ""path"": ""<Keyboard>/h"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HideItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8ba0c983-be32-4f45-b66e-2a3607d1879d"",
                    ""path"": ""<Keyboard>/p"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Flashlight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""faea99ef-0469-4c8f-bf4c-300f22e8ba10"",
                    ""path"": ""<Keyboard>/home"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PauseMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Player"",
            ""bindingGroup"": ""Player"",
            ""devices"": []
        }
    ]
}");
        // InFloor
        m_InFloor = asset.FindActionMap("InFloor", throwIfNotFound: true);
        m_InFloor_Jump = m_InFloor.FindAction("Jump", throwIfNotFound: true);
        m_InFloor_Camera = m_InFloor.FindAction("Camera", throwIfNotFound: true);
        m_InFloor_Movement = m_InFloor.FindAction("Movement", throwIfNotFound: true);
        m_InFloor_Run = m_InFloor.FindAction("Run", throwIfNotFound: true);
        m_InFloor_Shift = m_InFloor.FindAction("Shift", throwIfNotFound: true);
        m_InFloor_Interact = m_InFloor.FindAction("Interact", throwIfNotFound: true);
        m_InFloor_Escape = m_InFloor.FindAction("Escape", throwIfNotFound: true);
        m_InFloor_DropItem = m_InFloor.FindAction("DropItem", throwIfNotFound: true);
        m_InFloor_FlashlightUltraviolet = m_InFloor.FindAction("FlashlightUltraviolet", throwIfNotFound: true);
        m_InFloor_HideItem = m_InFloor.FindAction("HideItem", throwIfNotFound: true);
        m_InFloor_Flashlight = m_InFloor.FindAction("Flashlight", throwIfNotFound: true);
        m_InFloor_PauseMenu = m_InFloor.FindAction("PauseMenu", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }
    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }
    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // InFloor
    private readonly InputActionMap m_InFloor;
    private IInFloorActions m_InFloorActionsCallbackInterface;
    private readonly InputAction m_InFloor_Jump;
    private readonly InputAction m_InFloor_Camera;
    private readonly InputAction m_InFloor_Movement;
    private readonly InputAction m_InFloor_Run;
    private readonly InputAction m_InFloor_Shift;
    private readonly InputAction m_InFloor_Interact;
    private readonly InputAction m_InFloor_Escape;
    private readonly InputAction m_InFloor_DropItem;
    private readonly InputAction m_InFloor_FlashlightUltraviolet;
    private readonly InputAction m_InFloor_HideItem;
    private readonly InputAction m_InFloor_Flashlight;
    private readonly InputAction m_InFloor_PauseMenu;
    public struct InFloorActions
    {
        private @InputManager m_Wrapper;
        public InFloorActions(@InputManager wrapper) { m_Wrapper = wrapper; }
        public InputAction @Jump => m_Wrapper.m_InFloor_Jump;
        public InputAction @Camera => m_Wrapper.m_InFloor_Camera;
        public InputAction @Movement => m_Wrapper.m_InFloor_Movement;
        public InputAction @Run => m_Wrapper.m_InFloor_Run;
        public InputAction @Shift => m_Wrapper.m_InFloor_Shift;
        public InputAction @Interact => m_Wrapper.m_InFloor_Interact;
        public InputAction @Escape => m_Wrapper.m_InFloor_Escape;
        public InputAction @DropItem => m_Wrapper.m_InFloor_DropItem;
        public InputAction @FlashlightUltraviolet => m_Wrapper.m_InFloor_FlashlightUltraviolet;
        public InputAction @HideItem => m_Wrapper.m_InFloor_HideItem;
        public InputAction @Flashlight => m_Wrapper.m_InFloor_Flashlight;
        public InputAction @PauseMenu => m_Wrapper.m_InFloor_PauseMenu;
        public InputActionMap Get() { return m_Wrapper.m_InFloor; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(InFloorActions set) { return set.Get(); }
        public void SetCallbacks(IInFloorActions instance)
        {
            if (m_Wrapper.m_InFloorActionsCallbackInterface != null)
            {
                @Jump.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnJump;
                @Camera.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnCamera;
                @Camera.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnCamera;
                @Camera.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnCamera;
                @Movement.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnMovement;
                @Run.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnRun;
                @Run.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnRun;
                @Run.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnRun;
                @Shift.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnShift;
                @Shift.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnShift;
                @Shift.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnShift;
                @Interact.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnInteract;
                @Escape.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnEscape;
                @Escape.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnEscape;
                @Escape.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnEscape;
                @DropItem.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnDropItem;
                @DropItem.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnDropItem;
                @DropItem.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnDropItem;
                @FlashlightUltraviolet.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnFlashlightUltraviolet;
                @FlashlightUltraviolet.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnFlashlightUltraviolet;
                @FlashlightUltraviolet.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnFlashlightUltraviolet;
                @HideItem.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnHideItem;
                @HideItem.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnHideItem;
                @HideItem.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnHideItem;
                @Flashlight.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnFlashlight;
                @Flashlight.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnFlashlight;
                @Flashlight.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnFlashlight;
                @PauseMenu.started -= m_Wrapper.m_InFloorActionsCallbackInterface.OnPauseMenu;
                @PauseMenu.performed -= m_Wrapper.m_InFloorActionsCallbackInterface.OnPauseMenu;
                @PauseMenu.canceled -= m_Wrapper.m_InFloorActionsCallbackInterface.OnPauseMenu;
            }
            m_Wrapper.m_InFloorActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Camera.started += instance.OnCamera;
                @Camera.performed += instance.OnCamera;
                @Camera.canceled += instance.OnCamera;
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Run.started += instance.OnRun;
                @Run.performed += instance.OnRun;
                @Run.canceled += instance.OnRun;
                @Shift.started += instance.OnShift;
                @Shift.performed += instance.OnShift;
                @Shift.canceled += instance.OnShift;
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @Escape.started += instance.OnEscape;
                @Escape.performed += instance.OnEscape;
                @Escape.canceled += instance.OnEscape;
                @DropItem.started += instance.OnDropItem;
                @DropItem.performed += instance.OnDropItem;
                @DropItem.canceled += instance.OnDropItem;
                @FlashlightUltraviolet.started += instance.OnFlashlightUltraviolet;
                @FlashlightUltraviolet.performed += instance.OnFlashlightUltraviolet;
                @FlashlightUltraviolet.canceled += instance.OnFlashlightUltraviolet;
                @HideItem.started += instance.OnHideItem;
                @HideItem.performed += instance.OnHideItem;
                @HideItem.canceled += instance.OnHideItem;
                @Flashlight.started += instance.OnFlashlight;
                @Flashlight.performed += instance.OnFlashlight;
                @Flashlight.canceled += instance.OnFlashlight;
                @PauseMenu.started += instance.OnPauseMenu;
                @PauseMenu.performed += instance.OnPauseMenu;
                @PauseMenu.canceled += instance.OnPauseMenu;
            }
        }
    }
    public InFloorActions @InFloor => new InFloorActions(this);
    private int m_PlayerSchemeIndex = -1;
    public InputControlScheme PlayerScheme
    {
        get
        {
            if (m_PlayerSchemeIndex == -1) m_PlayerSchemeIndex = asset.FindControlSchemeIndex("Player");
            return asset.controlSchemes[m_PlayerSchemeIndex];
        }
    }
    public interface IInFloorActions
    {
        void OnJump(InputAction.CallbackContext context);
        void OnCamera(InputAction.CallbackContext context);
        void OnMovement(InputAction.CallbackContext context);
        void OnRun(InputAction.CallbackContext context);
        void OnShift(InputAction.CallbackContext context);
        void OnInteract(InputAction.CallbackContext context);
        void OnEscape(InputAction.CallbackContext context);
        void OnDropItem(InputAction.CallbackContext context);
        void OnFlashlightUltraviolet(InputAction.CallbackContext context);
        void OnHideItem(InputAction.CallbackContext context);
        void OnFlashlight(InputAction.CallbackContext context);
        void OnPauseMenu(InputAction.CallbackContext context);
    }
}
