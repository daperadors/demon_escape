using System.Collections;
using UnityEngine;
using static NewPlayerController;
using static PlayerExtras;

public class PlayerExtras : MonoBehaviour
{
    [Header("Target to move camera")]
    public Transform _targetCameraMovement;
    public Transform _targetOnCinematic;

    public Transform _startTarget;

    public Voices _voiceIntro;
    public Voices _voiceIntermidiate;
    public Voices _voiceExit;
    public float _returnToInitialDuration;
    public bool _returnCamera;

    private CameraTypes _cameraType;
    public CameraTypes cameraType { get { return _cameraType; } }

    private CinematicType _cinematicType;
    public CinematicType cinematicType { get { return _cinematicType; } }

    private CinematicInitialElement _cinematicInitialElement;
    public CinematicInitialElement cinematicInitialElement { get { return _cinematicInitialElement; } }


    [SerializeField] private NewPlayerController _player;
    [SerializeField] private EnemyController _enemy;

    [SerializeField] private Transform _entrance;

    public delegate void SetNewLookAt(Transform transform, float duration, bool returnCamera = true);
    public delegate void SetNewCinematic(CinematicInitialElement cinematicInitialElement, Transform transform, CameraTypes cinematic, CinematicType cinematicType, float duration, bool returnCamera = true, Transform targetOnCinematic = null, Voices voiceIntro = Voices.NOTHING, Voices voiceIntermidiate = Voices.NOTHING, Voices voiceExit = Voices.NOTHING);
    public delegate void StartLookAtCinematic();
    public delegate void NewObjectOrAnimalSound(SoundsLookAtEnum sound);
    public delegate void EnemyIntoCinematicAction(CinematicInitialElement initialElement);

    public static SetNewLookAt setNewLookAt;
    public static StartLookAtCinematic startLookAtCinematic;
    public static SetNewCinematic setNewCinematic;
    public static NewObjectOrAnimalSound newObjectOrAnimalSound;
    public static EnemyIntoCinematicAction onElementIntoCinematicAction;


    private void Start()
    {
        setNewLookAt += NewLookAt;
        setNewCinematic += NewCinematic;
        onElementIntoCinematicAction += PerformInitialCinematicAction;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TriggerEnterHouse")
        {
            if (other.gameObject.GetComponent<EnterHouseTrigger>().TriggerEnter)
            {
                PHController.activePikup.Invoke();
                EnemyController.onSetCinematicTargets.Invoke(_enemy.entranceCinematicPoints[0], _enemy.entranceCinematicPoints[1]);
                GameObject.Find("FinalCinematic").GetComponent<BoxCollider>().enabled = true;
                other.gameObject.SetActive(false);
                CinematicManager.onBlockDoor?.Invoke();
                NewCinematic(CinematicInitialElement.ENEMY, _player._enemyHeadTarget, CameraTypes.CAMERA_MOVEMENT, CinematicType.EXECUTE_ACTION, 3f, true, _entrance.transform, Voices.ENTRANCE, Voices.DOORBLOCKED, Voices.ESCAPE);
                _player.FSM.ChangeState<CinematicPlayerState>();
                _enemy.fms.ChangeState<CinematicState>();
            }
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("VoiceBox"))
        {
            ObjectInteractionVoice.onInitializeVoiceBox?.Invoke(other.gameObject.name);
            SQLController.Instance.UpdateColliders(other.gameObject.GetComponent<CinematicVoiceID>().ID);
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == "LookAtSound")
        {
            SoundsLookAt sound = other.gameObject.GetComponent<SoundsLookAt>();
            _targetCameraMovement = sound.SoundTransform;
            _returnCamera = true;
            newObjectOrAnimalSound.Invoke(sound.SoundObject);
            _player.FSM.ChangeState<LookAtState>();
            other.gameObject.SetActive(false);
            SQLController.Instance.UpdateColliders(other.gameObject.GetComponent<CinematicVoiceID>().ID);
        }

        if(other.gameObject.tag == "FinalCinematic")
        {
            other.gameObject.SetActive(false);
            CinematicManager.onBlockDoor?.Invoke();
            EnemyController.onSetCinematicTargets.Invoke(_enemy.finalCinematicPoints[0], _enemy.finalCinematicPoints[1]);
            SoundsLookAt sound = other.gameObject.GetComponent<SoundsLookAt>();
            NewCinematic(CinematicInitialElement.ENEMY, sound.SoundTransform, CameraTypes.NO_CAMERA_MOVEMENT, CinematicType.EXECUTE_ACTION, 3f, true, null, Voices.INITIAL_FINAL_CINEMATIC, Voices.NOTHING, Voices.FINISH_FINAL_CINEMATIC);
            _enemy.fms.ChangeState<CinematicState>();
            _player.FSM.ChangeState<CinematicPlayerState>();
        }
    }

    private void NewLookAt(Transform transform, float duration, bool returnCamera = true)
    {
        _targetCameraMovement = transform;
        _returnToInitialDuration = duration;
        _returnCamera = returnCamera;
        _voiceExit = Voices.NOTHING;
        _voiceIntro= Voices.NOTHING;
        _voiceIntermidiate = Voices.NOTHING;
        _cinematicType= CinematicType.NO_EXECUTE_ACTION;
        _voiceExit = Voices.NOTHING;
        _voiceIntro = Voices.NOTHING;
        _voiceIntermidiate = Voices.NOTHING;
    }

    private void NewCinematic(CinematicInitialElement initialElement, Transform targetBeforeCinematic, CameraTypes cameraType, CinematicType cinematicType, float duration, bool returnCamera = true, Transform targetOnCinematic = null, Voices voiceIntro = Voices.NOTHING, Voices voiceIntermidiate = Voices.NOTHING, Voices voiceExit = Voices.NOTHING)
    {
        _cinematicInitialElement = initialElement;
        _targetOnCinematic = targetBeforeCinematic;
        _targetCameraMovement = targetOnCinematic;
        _voiceIntro = voiceIntro;
        _returnToInitialDuration = duration;
        _voiceIntermidiate = voiceIntermidiate;
        _voiceExit = voiceExit;
        _cameraType = cameraType;
        _cinematicType = cinematicType;
        _returnCamera = returnCamera;
    }


    private void PerformInitialCinematicAction(CinematicInitialElement element)
    {
        switch (element)
        {
            case CinematicInitialElement.ENEMY:
                EnemyController.onEnemyIntoCinematicAction?.Invoke();
                break;
            case CinematicInitialElement.CUERVOS:
                CuervosDuros.onFlyAction?.Invoke();
                break;            
            case CinematicInitialElement.PICKUP:
                PHController.startDrive?.Invoke();
                break;
            default:
                break;
        }
    }

}

[System.Serializable]
public enum CameraTypes
{

    //CAMERA_MOVEMENT va hacia un target i vuelve al estado inicial despues de terminar la fase inicial de la cinematica / Ej : Cuando el personaje mira al demonio
    //pasar por las escaleras, con NO_CAMERA_MOVEMENT al terminar la fase inicial vuelve a la rotacion inicial directamente

    CAMERA_MOVEMENT, NO_CAMERA_MOVEMENT
}

[System.Serializable]
public enum CinematicType
{
    //CINEMATIC ejecuta accion de cinematica, LOOKAT no

    EXECUTE_ACTION, NO_EXECUTE_ACTION
}

[System.Serializable]
public enum CinematicInitialElement
{
    CUERVOS, ENEMY, PICKUP
}