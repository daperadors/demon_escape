using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static ItemChange;

public class EnigmaticController : MonoBehaviour
{
    [SerializeField] private GameObject[] m_Pieces;
    [SerializeField] private PieceEnigmatic[] m_PiecesComponent;
    [SerializeField] private Camera m_Camera;
    [SerializeField] private LayerMask m_Layer;
    [SerializeField] private ItemChange m_Items;
    [SerializeField] private GameObject m_Trigger;
    [SerializeField] private GameObject m_FinalKey;
    [SerializeField] private Light m_FinalLight;
    [SerializeField] private Animator m_JesusAnimator;

    public GameObject[] pieces { get { return m_Pieces; } }
    private int m_PiecesVal;
    private float m_IntensityLight = 5.5f;
    private bool m_CanPut = true;
    private void Awake()
    {
        MenuController.continueGame += GetAndSetVisibiliteEnigmaticPieces;
    }
    void Start()
    {
        SavePieces();
        SetActivePieces(false);
        m_FinalLight.intensity = 0;
        m_FinalKey.SetActive(false);
    }
    private void Update()
    {
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            Ray rayOrigin = m_Camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(rayOrigin, out hit, 3f, m_Layer))
            {
                if(m_CanPut && m_Items._itemActual != null && hit.collider.gameObject.tag == m_Items._itemActual.gameObject.tag)
                {
                    SQLController.Instance.UpdateEnigmaticPiece(m_Items._itemActual.GetComponent<PieceEnigmatic>().ID, true);
                    GameObject piece = m_Pieces[Int32.Parse(m_Items._itemActual.gameObject.tag.Substring(7, 1))-1];
                    m_Items.DropInventoryItem();
                    piece.SetActive(true);
                    piece.GetComponent<Animator>().SetBool("put", true);
                    ItemChange.itemChaged?.Invoke(null);
                    PutPiece();
                    StartCoroutine(WaitForPut());
                }
            }
        }
    }
    private void GetAndSetVisibiliteEnigmaticPieces()
    {
        m_PiecesVal = 0;
        List<int> ids = SQLController.Instance.GetAllEnigmaticPiecesSetted();
        foreach(int id in ids)
        {
            PieceEnigmatic piece = m_PiecesComponent.Where(p => p.ID == id).FirstOrDefault();
            if(piece != null)
            {
                piece.GroundObject.SetActive(false);
                piece.AnimatorObject.SetActive(true);
                piece.AnimatorObject.GetComponent<Animator>().SetBool("put", true);
                PutPiece();
            }
        }
    }
    private void SetActivePieces(bool status)
    {
        foreach (GameObject piece in m_Pieces)
        {
            piece.SetActive(status);
        }
    }
    public void PutPiece()
    {
        m_PiecesVal++;
        m_FinalLight.intensity += m_IntensityLight;
        if (m_PiecesVal == m_Pieces.Length)
        {
            StartCoroutine(SoundOffset());
            NewPlayerController.startRoom.Invoke("EnigmaticMinigame", false);
            NewPlayerController.minigameFinished?.Invoke();
            m_Trigger.SetActive(false);
            m_FinalLight.gameObject.GetComponent<Animator>().Play("EnigameticFinalLight");
            if(!ItemChange.AuxiliarItems.Contains(m_FinalKey)) m_FinalKey.SetActive(true);
            m_JesusAnimator.Play("FinalAnimation");
        }
    }
    void SavePieces()
    {
        foreach(PieceEnigmatic piece in m_PiecesComponent)
        {
            SQLController.Instance.SavePiecesEnigmaticStart(piece.ID, piece.gameObject.GetComponent<ObjectsId>().id);
        }
    }
    IEnumerator WaitForPut()
    {
        m_CanPut = false;
        yield return new WaitForSeconds(.5f);
        m_CanPut = true;
    }
    IEnumerator SoundOffset()
    {
        yield return new WaitForSeconds(0.15f);
        AudioLibrary.objectSound.Invoke(AudioLibrary.AudioName.GEAR);
    }
}
