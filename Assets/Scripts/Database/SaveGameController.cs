using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using static MenuController;

public class SaveGameController : MonoBehaviour
{
    [SerializeField] private Transform m_House;
    [SerializeField] private Transform m_Player;
    [SerializeField] private Transform m_Enemy;
    [SerializeField] private Transform[] m_SpawnEnemy;

    private List<Transform> m_TransformToSave;
    private TransformObjects transforms;
    private const string nameFileData = "ObjectsSaved";
    private const string pathData = "DataSavedGame";
    private void Awake()
    {
        MenuController.continueGame += LoadPositions;
        MenuController.savePositions += SavePositions;
        MenuController.startNewGame += NewGame;
        EnemyController.teleportEnemy += TeleportEnemyToOtherPoint;
    }
    void Start()
    {
        transforms = new TransformObjects();
        m_TransformToSave = new List<Transform>();
    }

    void SavePositions()
    {
        SaveSinglePositions(m_House, false, m_House.name);
        SaveSinglePositions(m_Player, true, m_Player.name);
        SaveSinglePositions(m_Enemy, true, m_Enemy.name);
        SaveLoadSystemData.SaveData(transforms, pathData, nameFileData);
        print("Saved");
        m_TransformToSave.Clear();
        ClearAllTransforms();
    }
    void SaveSinglePositions(Transform transform, bool single, string name)
    {
        if (!single)
        {
            GetAllChilds(transform);

            for (int i = 0; i < m_TransformToSave.Count - 1; i++)
            {
                transforms.name.Add(name);
                transforms.positions.Add(m_TransformToSave[i].position);
                transforms.rotations.Add(m_TransformToSave[i].rotation);
                transforms.sizes.Add(m_TransformToSave[i].localScale);

            }
        }
        else
        {
            m_TransformToSave.Add(transform);
            transforms.name.Add(name);
            transforms.positions.Add(transform.position);
            transforms.rotations.Add(transform.rotation);
            transforms.sizes.Add(transform.localScale);
        }
    }
    void LoadPositions()
    {
        TransformObjects transformsLoaded = SaveLoadSystemData.LoadData<TransformObjects>(pathData, nameFileData);
        if (transformsLoaded != null)
        {
            string room = SQLController.Instance.GetRoomStarted();
            print(room + " started");
            LoadSinglePositions(transformsLoaded, m_House, false, m_House.name);
            if(room != "")
            {
                Transform transform = GameManager.Instance.GetEntranceRoom(room);
                m_Player.transform.position = transform.position;
                SQLController.Instance.SetRoomFinished(room);
                SavePositions();
            }
            else LoadSinglePositions(transformsLoaded, m_Player, true, m_Player.name);
            m_Player.GetComponent<NewPlayerController>().SetDefaultPositionCameraHolder();
            LoadSinglePositions(transformsLoaded, m_Enemy, true, m_Enemy.name);

            if(Unity.Mathematics.math.distancesq(m_Enemy.position, m_Player.position) < 75f) TeleportEnemyToOtherPoint();
            print("Loaded");
            m_TransformToSave.Clear();
            ClearAllTransforms();
        }
    }
    void TeleportEnemyToOtherPoint()
    {
        m_Enemy.position = m_SpawnEnemy[Random.Range(0, m_SpawnEnemy.Length - 1)].position;
        if (Unity.Mathematics.math.distancesq(m_Enemy.position, m_Player.position) < 5f)
        {
            TeleportEnemyToOtherPoint();
        }
    }
    void LoadSinglePositions(TransformObjects transformsLoaded, Transform transform, bool single, string name)
    {
        if (!single)
        {
            GetAllChilds(transform);

            for (int i = 0; i < m_TransformToSave.Count - 1; i++)
            {
                m_TransformToSave[i].position = transformsLoaded.positions[i];
                m_TransformToSave[i].rotation = transformsLoaded.rotations[i];
                m_TransformToSave[i].localScale = transformsLoaded.sizes[i];
            }
        }
        else
        {
            m_TransformToSave.Add(transform);
            int index = transformsLoaded.name.IndexOf(transformsLoaded.name.Where(n => n == name).FirstOrDefault());
            transform.position = transformsLoaded.positions[index];
            transform.rotation = transformsLoaded.rotations[index];
            transform.localScale = transformsLoaded.sizes[index];
        }
    }
    private void GetAllChilds(Transform transform)
    {
        foreach (Transform t in transform)
        {
            if (t.gameObject.name != "LoopRoom" && t.gameObject.name != "PalindromRoom")
            {
                m_TransformToSave.Add(t);
                GetAllChilds(t);
            }
        }
    }
    private void NewGame()
    {
        SaveLoadSystemData.DeleteFile(pathData, nameFileData);
    }
    private void ClearAllTransforms()
    {
        transforms.name.Clear();
        transforms.positions.Clear();
        transforms.rotations.Clear();
        transforms.sizes.Clear();
    }
}
[System.Serializable]
public class TransformObjects
{
    public List<string> name = new List<string>();
    public List<Vector3> positions = new List<Vector3>();
    public List<Quaternion> rotations = new List<Quaternion>();
    public List<Vector3> sizes = new List<Vector3>();
}