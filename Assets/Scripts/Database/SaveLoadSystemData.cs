using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting;
using UnityEngine;

public static class SaveLoadSystemData
{

    public static void SaveData<T>(T data, string path, string filename)
    {
        string fullPath = path + "/";
        bool checkFolderExit = Directory.Exists(fullPath);
        if (!checkFolderExit)
        {
            Directory.CreateDirectory(fullPath);
        }
        string json = JsonUtility.ToJson(data);
        DeleteFile(path, filename);
        File.WriteAllText(fullPath+filename+".json", json);
    }
    public static T LoadData<T>(string path, string fileName)
    {
        string fullPath = path+"/"+fileName+".json";
        if (File.Exists(fullPath))
        {
            string textJson = File.ReadAllText(fullPath);
            var obj = JsonUtility.FromJson<T>(textJson);
            return obj;
        }
        else
        {
            Debug.Log("file not found on load data");
            return default;
        }
    }
    public static void DeleteFile(string path, string fileName)
    {
        string fullPath = path + "/" + fileName + ".json";
        if (File.Exists(fullPath))
        {
            File.Delete(fullPath);
        }
    }
}
