using Mono.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;

public class SQLController : MonoBehaviour
{
    [SerializeField] private DatabaseConfig _databaseConfig;
    private string _path = "DataSavedGame/DatabaseGame";
    private string _connection;
    public static SQLController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        if (!Directory.Exists(_path))
        {
            try
            {
                Directory.CreateDirectory(_path);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }
        _connection = "URI=file:" + _path + "/demon_escape_config.db";
        GetAndCreateTables();
    }
    void Start()
    {
        MenuController.startNewGame += ResetAllTables;
    }
    private void CreateTable(string tableName, string[] columns)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = $"CREATE TABLE IF NOT EXISTS {tableName} ({string.Join(", ", columns)})";
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    private void GetAndCreateTables()
    {
        foreach (DatabaseTables table in _databaseConfig.tables)
        {
            if (table.tableName != "bug") CreateTable(table.tableName, table.columns);
        }
    }
    public void SaveStatusRoom(string roomName, int completed)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            if (GetStatusRoom(roomName)) return;
            if (GetIfRoomIsStarted(roomName)) return;
            connection.Open();
            var command = connection.CreateCommand();
            DeleteRoomStatus(roomName);
            print(roomName +" = no completada");
            command.CommandText = "INSERT OR REPLACE INTO StatusRoom (room_name, completed) VALUES (@roomName, @completed)";
            command.Parameters.AddWithValue("@roomName", roomName);
            command.Parameters.AddWithValue("@completed", completed);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void SaveStatusRoomWithoutCheckStart(string roomName, int completed)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            DeleteRoomStatus(roomName);
            command.CommandText = "INSERT OR REPLACE INTO StatusRoom (room_name, completed) VALUES (@roomName, @completed)";
            command.Parameters.AddWithValue("@roomName", roomName);
            command.Parameters.AddWithValue("@completed", completed);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public bool GetStatusRoom(string roomName)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            bool completed = false;
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM StatusRoom WHERE room_name = @room AND completed = @completed";
            command.Parameters.AddWithValue("@room", roomName);
            command.Parameters.AddWithValue("@completed", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                completed = true;
                break;
            }
            connection.Close();
            return completed;
        }
    }
    public bool GetIfRoomIsStarted(string roomName)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            bool completed = false;
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM StatusRoom WHERE room_name = @room AND started = @started";
            command.Parameters.AddWithValue("@room", roomName);
            command.Parameters.AddWithValue("@started", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                print("Room started " + roomName);
                completed = true;
                break;
            }
            connection.Close();
            return completed;
        }
    }
    private void DeleteRoomStatus(string roomName)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "DELETE FROM StatusRoom WHERE room_name = @room";
            command.Parameters.AddWithValue("@room", roomName);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public string GetRoomStarted()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            string room = "";
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT room_name FROM StatusRoom WHERE started = @started AND room_name != @room1 AND room_name != @room2 AND completed != @completed";
            command.Parameters.AddWithValue("@started", 1);
            command.Parameters.AddWithValue("@room1", "GameSaved");
            command.Parameters.AddWithValue("@room2", "InsideHouse");
            command.Parameters.AddWithValue("@completed", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                room = reader.GetString(0);
            }
            connection.Close();
            return room;
        }
    }
    public void SetRoomStarted(string room)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE StatusRoom SET started = @started WHERE room_name = @room";
            command.Parameters.AddWithValue("@room", room);
            command.Parameters.AddWithValue("@started", 1);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }    
    public void SetRoomFinished(string room)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE StatusRoom SET started = @started WHERE room_name = @room AND room_name != @room1 AND room_name != @room2";
            command.Parameters.AddWithValue("@room", room);
            command.Parameters.AddWithValue("@room1", "GameSaved");
            command.Parameters.AddWithValue("@room2", "InsideHouse");
            command.Parameters.AddWithValue("@started", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }    
    public void SetAllRoomFinished()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE StatusRoom SET started = @started WHERE completed != @completed";
            command.Parameters.AddWithValue("@started", 0);
            command.Parameters.AddWithValue("@completed", 1);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void SetRoomCompleted(string room)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE StatusRoom SET completed = @completed WHERE room_name = @room";
            command.Parameters.AddWithValue("@room", room);
            command.Parameters.AddWithValue("@completed", 1);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    private void UpdateStatusRoom()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE StatusRoom SET started = @started";
            command.Parameters.AddWithValue("@started", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    private void UpdateStatusRoomCompleted()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE StatusRoom SET completed = @completed";
            command.Parameters.AddWithValue("@completed", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void SaveSettings(string key, int value)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            if (GetSettings(key) == null) command.CommandText = "INSERT OR REPLACE INTO QualitySettings (key, value) VALUES (@key, @value)";
            else command.CommandText = "UPDATE QualitySettings SET value = @value WHERE key = @key";

            command.Parameters.AddWithValue("@key", key);
            command.Parameters.AddWithValue("@value", value);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void SaveVolume(string type, string value)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            if (GetVolume(type) == null) command.CommandText = "INSERT OR REPLACE INTO VolumeSettings (volume_type, value) VALUES (@volume_type, @value)";
            else command.CommandText = "UPDATE VolumeSettings SET value = @value WHERE volume_type = @volume_type";

            command.Parameters.AddWithValue("@volume_type", type);
            command.Parameters.AddWithValue("@value", value);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void SaveControls(string controls)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            DeleteControlRegister();
            command.CommandText = "INSERT OR REPLACE INTO Controls (controls, date) VALUES (@controls, @date)";
            command.Parameters.AddWithValue("@controls", controls);
            command.Parameters.AddWithValue("@date", DateTime.Now);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public string GetRoomCode(string room)
    {
        string code = "";
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM RoomCodes WHERE room_name = @room_name";
            command.Parameters.AddWithValue("@room_name", room);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                code = reader.GetString(1);
            }
            connection.Close();
            return code;
        }
    }
    public void SaveRoomCode(string room, string code)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            DeleteRoomCode(room);
            command.CommandText = "INSERT OR REPLACE INTO RoomCodes (room_name, code) VALUES (@room, @code)";
            command.Parameters.AddWithValue("@room", room);
            command.Parameters.AddWithValue("@code", code);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    private void DeleteRoomCode(string room)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "DELETE FROM RoomCodes WHERE room_name = @room";
            command.Parameters.AddWithValue("@room", room);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void SaveControls(string oldControl, string newControl, string controlName, bool isDefault)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            if (oldControl != "" && !ControlOld(newControl))
            {
                DeleteControl(controlName);
            }
            if (isDefault)
            {
                if (CheckIfControlExists(newControl, controlName))
                {
                    connection.Close();
                    return;
                }
            }
            command.CommandText = "INSERT OR REPLACE INTO Control_Stats (control_name, control, is_default) VALUES (@control_name , @control, @is_default)";
            command.Parameters.AddWithValue("@control_name", controlName);
            command.Parameters.AddWithValue("@control", newControl);
            command.Parameters.AddWithValue("@is_default", isDefault ? 1 : 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    private bool ControlOld(string control)
    {
        bool result = false;
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Control_Stats WHERE control = @control AND is_default = @default";
            command.Parameters.AddWithValue("@control", control);
            command.Parameters.AddWithValue("@default", 0);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result = reader.GetInt32(2) == 1 ? true : false;
            }
            connection.Close();
            return result;
        }
    }
    public bool CheckIfControlExists(string control, string controlname)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Control_Stats WHERE control = @control AND control_name = @name";
            command.Parameters.AddWithValue("@control", control);
            command.Parameters.AddWithValue("@name", controlname);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                connection.Close();
                return true;
            }
            connection.Close();
            return false;
        }
    }
    public QueryResult CheckIfControlIsDefault(string control, string controlname)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * " +
                                "FROM Control_Stats " +
                                "WHERE control = @control";
            command.Parameters.AddWithValue("@control", control);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader.GetString(0) == controlname)
                {
                    if (reader.GetInt32(2) == 1)
                    {
                        connection.Close();
                        return QueryResult.DEFAULT;
                    }
                }
                else
                {
                    connection.Close();
                    if (reader.GetInt32(2) == 1) return QueryResult.NOT_EXISTS;
                    return QueryResult.EXISTS;
                }

            }
            connection.Close();
            return QueryResult.NOT_EXISTS;
        }
    }
    public void DeleteControl(string control)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "DELETE FROM Control_Stats WHERE control_name = @control AND is_default = @default";
            command.Parameters.AddWithValue("@control", control);
            command.Parameters.AddWithValue("@default", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void DeleteControlRegister()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "DELETE FROM Controls";
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public string GetControls()
    {
        string result = "";
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Controls";
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string controls = reader.GetString(0);
                result = controls;
            }
            connection.Close();
            return result;
        }
    }
    public string GetControl(int id)
    {
        string result = "";
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Controls WHERE control_name = @id";
            command.Parameters.AddWithValue("@id", id);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result = reader.GetString(1);
            }
            connection.Close();
            return result;
        }
    }
    public QualitySettingsDB GetSettings(string key)
    {
        QualitySettingsDB quality = null;
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM QualitySettings WHERE key = @key";
            command.Parameters.AddWithValue("@key", key);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string key_r = reader.GetString(0);
                int value_r = reader.GetInt32(1);
                quality = new QualitySettingsDB(key_r, value_r);
            }
            connection.Close();
            return quality;
        }
    }
    public VolumeSettings GetVolume(string type)
    {
        VolumeSettings volume = null;
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM VolumeSettings WHERE volume_type = @type";
            command.Parameters.AddWithValue("@type", type);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string volume_type = reader.GetString(0);
                string value = reader.GetString(1);
                volume = new VolumeSettings(volume_type, value);
            }
            connection.Close();
            return volume;
        }
    }
    public void SaveInventoryStart(int id, string name)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            if (GetIfItemIsInInventory(id)) return;
            connection.Open();
            var command = connection.CreateCommand();
            DeleteItemById(id, "Inventory");
            command.CommandText = "INSERT OR REPLACE INTO Inventory (id, name, in_inventory) VALUES (@id, @name, @in_inventory)";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@name", name);
            command.Parameters.AddWithValue("@in_inventory", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void SaveItemInventoryStatus(int id, int status)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE Inventory SET in_inventory = @in_inventory WHERE id = @id";
            command.Parameters.AddWithValue("@in_inventory", status);
            command.Parameters.AddWithValue("@id", id);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void SetObjectReferenceIntoItemInventory(int idObject, int idObjectExtra)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE Inventory SET object_reference = @object_reference WHERE id = @id";
            command.Parameters.AddWithValue("@object_reference", idObjectExtra);
            command.Parameters.AddWithValue("@id", idObject);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void DeleteObjectReference(int idObject)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE Inventory SET object_reference = @object_reference WHERE id = @id";
            command.Parameters.AddWithValue("@object_reference", null);
            command.Parameters.AddWithValue("@id", idObject);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public bool GetIfItemIsInInventory(int id)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            bool result = false;
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Inventory WHERE id = @id AND in_inventory = @ininventory";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@ininventory", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result = true;
                break;
            }
            connection.Close();
            return result;
        }
    }
    public List<int> GetAllItemsInInventory()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            List<int> ids = new List<int>();
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Inventory WHERE in_inventory = @ininventory AND name != @nameExcluded";
            command.Parameters.AddWithValue("@ininventory", 1);
            command.Parameters.AddWithValue("@nameExcluded", "Flashlight");
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                ids.Add(reader.GetInt32(0));
            }
            connection.Close();
            return ids;
        }
    }
    public bool GetIfFlashlightIsActive()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            bool result = false;
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT in_inventory FROM Inventory WHERE name = @name AND in_inventory = @ininventory";
            command.Parameters.AddWithValue("@name", "Flashlight");
            command.Parameters.AddWithValue("@ininventory", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result = true;
                break;
            }
            connection.Close();
            return result;
        }
    }
    public int GetObjectReferenceInItemInventory(int id)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            int id_object_reference = 0;
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT object_reference FROM Inventory WHERE id = @id";
            command.Parameters.AddWithValue("@id", id);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                id_object_reference = reader.GetInt32(0);
            }
            connection.Close();
            return id_object_reference;
        }
    }
    public void SaveObjectsExtraStart(int id, string name)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            if (GetIfObjectExtraIsTaken(id)) return;
            connection.Open();
            var command = connection.CreateCommand();
            DeleteItemById(id, "ExtraObjects");
            command.CommandText = "INSERT OR REPLACE INTO ExtraObjects (id, name, taken) VALUES (@id, @name, @taken)";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@name", name);
            command.Parameters.AddWithValue("@taken", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public bool GetIfItemIsVisible(int id)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            bool result = false;
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM ExtraObjects WHERE id = @id AND visible = @visible";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@visible", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result = true;
                break;
            }
            connection.Close();
            return result;
        }
    }
    public bool GetIfObjectExtraIsTaken(int id)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            bool result = false;
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM ExtraObjects WHERE id = @id AND taken = @taken OR visible != @visible";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@taken", 1);
            command.Parameters.AddWithValue("@visible", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result = true;
                break;
            }
            connection.Close();
            return result;
        }
    }
    public void DeleteItemById(int id, string table)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = $"DELETE FROM {table} WHERE id = @id";
            command.Parameters.AddWithValue("@id", id);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void UpdateObjectExtra(int id, bool taken)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE ExtraObjects SET taken = @taken WHERE id = @id";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@taken", taken ? 1 : 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void ChangeVisibilityExtraObject(int id, bool visible)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE ExtraObjects SET visible = @visible WHERE id = @id";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@visible", visible ? 1 : 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void ChangeVisibilityAllExtraObject(bool visible)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE ExtraObjects SET visible = @visible";
            command.Parameters.AddWithValue("@visible", visible ? 1 : 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void UpdateAllObjectExtra()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE ExtraObjects SET taken = @taken";
            command.Parameters.AddWithValue("@taken", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void UpdateInInventory()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE Inventory SET in_inventory = @in_inventory";
            command.Parameters.AddWithValue("@in_inventory", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void UpdateObjectReferenceInInventory()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE Inventory SET object_reference = @object_reference";
            command.Parameters.AddWithValue("@object_reference", null);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void DeleteItemInInventory(int id)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE Inventory SET in_inventory = @in_inventory WHERE id = @id";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@in_inventory", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public bool GetIfPieceEnigmaticIsSetted(int id)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            bool result = false;
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM EnigmaticPieces WHERE id = @id AND setted = @setted";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@setted", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result = true;
                break;
            }
            connection.Close();
            return result;
        }
    }
    public List<int> GetAllEnigmaticPiecesSetted()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            List<int> result = new List<int>();
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM EnigmaticPieces WHERE setted = @setted";
            command.Parameters.AddWithValue("@setted", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result.Add(reader.GetInt32(0));
            }
            connection.Close();
            return result;
        }
    }
    public void SavePiecesEnigmaticStart(int id, int id_object)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            if (GetIfPieceEnigmaticIsSetted(id)) return;
            connection.Open();
            var command = connection.CreateCommand();
            DeleteItemById(id, "EnigmaticPieces");
            command.CommandText = "INSERT OR REPLACE INTO EnigmaticPieces (id, id_object, setted) VALUES (@id, @id_object, @setted)";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@id_object", id_object);
            command.Parameters.AddWithValue("@setted", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void UpdateEnigmaticPiece(int id, bool status)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE EnigmaticPieces SET setted = @setted WHERE id = @id";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@setted", status ? 1 : 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void UpdateEnigmaticPieceRestart()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE EnigmaticPieces SET setted = @setted";
            command.Parameters.AddWithValue("@setted", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void ResetAllTables()
    {
        print("Reset all");
        UpdateStatusRoom();
        UpdateInInventory();
        UpdateObjectReferenceInInventory();
        UpdateStatusRoomCompleted();
        UpdateAllObjectExtra();
        UpdateEnigmaticPieceRestart();
        ChangeVisibilityAllExtraObject(true);
        UpdateAllColliders();
        ResetColliders();
    }

    public void SaveCinematicOrVoiceCollidersStart(int id)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            if (GetIfColliderIsActive(id)) return;
            connection.Open();
            var command = connection.CreateCommand();
            DeleteItemById(id, "CinematicVoice");
            command.CommandText = "INSERT OR REPLACE INTO CinematicVoice (id, active) VALUES (@id, @active)";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@active", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public bool GetIfColliderIsActive(int id)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            bool result = false;
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM CinematicVoice WHERE id = @id AND active = @active";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@active", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result = true;
                break;
            }
            connection.Close();
            return result;
        }
    }
    public List<int> GetAllCollidersDisabled()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            List<int> result = new List<int>();
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM CinematicVoice WHERE active = @active";
            command.Parameters.AddWithValue("@active", 1);
            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result.Add(reader.GetInt32(0));
            }
            connection.Close();
            return result;
        }
    }
    public void UpdateColliders(int id)
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE CinematicVoice SET active = @active WHERE id = @id";
            command.Parameters.AddWithValue("@active", 1);
            command.Parameters.AddWithValue("@id", id);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }    
    public void UpdateAllColliders()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE CinematicVoice SET active = @active";
            command.Parameters.AddWithValue("@active", 1);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
    public void ResetColliders()
    {
        using (var connection = new SqliteConnection(_connection))
        {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "UPDATE CinematicVoice SET active = @active";
            command.Parameters.AddWithValue("@active", 0);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
}
public class QualitySettingsDB
{
    public string key;
    public int value;

    public QualitySettingsDB(string key, int value)
    {
        this.key = key;
        this.value = value;
    }
}
public class VolumeSettings
{
    public string volume_type;
    public string value;

    public VolumeSettings(string volume_type, string value)
    {
        this.volume_type = volume_type;
        this.value = value;
    }
}
public enum QueryResult
{
    EXISTS,
    NOT_EXISTS,
    DEFAULT
}