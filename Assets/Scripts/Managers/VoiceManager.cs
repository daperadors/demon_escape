using DG.Tweening;
using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;

public class VoiceManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _voiceText;
    public static bool _enableVoice;

    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private PlayerVoice[] _playerVoices;

    public delegate void PlayPlayerVoice(Voices audio);
    public static PlayPlayerVoice onPlayVoice;

    public delegate void StopPlayerVoice();
    public static StopPlayerVoice onStopPlayerVoice;

    private void Start()
    {
        _voiceText.enabled = false;
        onPlayVoice += InitializeVoice;
        onStopPlayerVoice += CancelVoice;
    }

    private void InitializeVoice(Voices audioId)
    {
        StopAllCoroutines();
        _enableVoice = true;
        StartCoroutine(PlayPlayerAudioVoice(audioId));
    }

    private IEnumerator PlayPlayerAudioVoice(Voices audioId)
    {
        PlayerVoice playerVoice = _playerVoices.Where(x => x._audioID == audioId).FirstOrDefault();

        if (playerVoice != null)
        {
            /*if (playerVoice._audioVoice != null)
            {
                _audioSource.clip = playerVoice._audioVoice;
                _audioSource.Play();
            }*/

            if (playerVoice._audioID != Voices.NOTHING)
            {
                _voiceText.enabled = true;
                _voiceText.transform.DOScale(1.1f, .5f);
                _voiceText.gameObject.GetComponent<AutoPutText>().ChengeID(playerVoice._audioText);
                //yield return new WaitWhile(() => _audioSource.isPlaying);
                yield return new WaitForSeconds(playerVoice._duration);
                _voiceText.transform.DOScale(1f, .5f);
                _voiceText.enabled = false;
                _enableVoice = false;

            }
        }
    }

    private void CancelVoice()
    {
        _voiceText.enabled = false;
    }



}

[System.Serializable]
public class PlayerVoice
{
    public Voices _audioID;
    public AudioClip _audioVoice;
    public string _audioText;
    public float _duration;
}

public enum Voices
{
    NOTHING, ENTRANCE, DOORENTRANCE, OPINION, ESCAPE, DOORBLOCKED, INITIAL_FINAL_CINEMATIC, FINISH_FINAL_CINEMATIC, SYRINGE_VOICE, ENEMYNEAR, FULL_INVENTORY, OUIJA_LESS_MAX_QUESTIONS, BREAK_OBSTACLE
}