using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static NewPlayerController;

public class CinematicManager : MonoBehaviour
{
    [Header("EntranceAnimation")]
    //[SerializeField] private GameObject _falseEnemy;
    [SerializeField] private GameObject[] _doorEntrance;

    public delegate void BlockDoor();
    public static BlockDoor onBlockDoor;

    public delegate IEnumerator CinematicAction();
    public static CinematicAction onCinematicAction;


    private void Start()
    {
        onCinematicAction += ExecuteCinematicEntranceAction;
        onBlockDoor += BlockDoors;
    }

    public IEnumerator ExecuteCinematicEntranceAction()
    {
        yield return StartCoroutine(Action());
        
    }

    private IEnumerator Action()
    {
        yield return new WaitForSeconds(1);
        foreach (var _door in _doorEntrance)
        {
            _door.GetComponent<DoorController>().CloseDoor();
        }
    }

    private void BlockDoors()
    {
        foreach (var _door in _doorEntrance)
        {
            _door.gameObject.tag = "DoorBlocked";
            //_door.gameObject.layer = LayerMask.NameToLayer("Default");
        }
    }

}
